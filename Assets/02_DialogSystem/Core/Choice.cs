using System.Linq;

namespace DialogSystem
{
    public struct ChoiceData
    {
        public string Jump;
        public string[] texts;
    }

    [System.Serializable]
    public class Choice : DialogNode
    {
        ChoiceData[] choices; // [ choice ] line data for that choice

        public Choice(
            string name, 
            Character character,
            string Label, 
            string jump, 
            ChoiceData[] data ) 
            : base("choice", name, character, Label, jump)
        {
            choices = data;
        }

        public override string[] Data(int locale)
            => choices.Select( x => x.texts[locale] ).ToArray();

        public override (string, bool) Input(object arg)
        {
            if( arg is not int )
                return base.Input( arg );

            int input = (int)arg;
            Jump = choices[input + 1].Jump;
            return (Jump, true);
        }
    }
}