using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

namespace DialogSystem
{
    public class Utils : MonoBehaviour
    {
        public static string ReplaceTags( string x, Regex regex, Func<string, string> replaceFunc )
        {
            int cursor = 0;
            MatchCollection matches = regex.Matches( x );            
            List<string> segements = new List<string>();

            for( int i = 0; i < matches.Count; ++i )
            {
                Match match = matches[i];  
                segements.Add( x.Substring( cursor, match.Index - cursor ) );
                
                string color = match.Groups[0].Value;
                color = color.Replace( match.Groups[1].Value, replaceFunc( match.Groups[1].Value ) );
                segements.Add( color );

                cursor = match.Index + match.Length;
            }

            segements.Add( x.Substring( cursor ) );

            return string.Join( "", segements );
        }
    }
}