using UnityEngine;

namespace DialogSystem
{
    [CreateAssetMenu(menuName ="Dialog/Character")]
    public class Character : ScriptableObject
    {
        public Sprite sprite;
        public Color color;

        [SerializeField]
        private RuntimeSetCharacter characters;

        void OnValidate()
        {
            characters?.Add( this );
        }

        void OnEnable()
        {
            characters?.Add( this );
        }
    }
}