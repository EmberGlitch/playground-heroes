using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface DialogInputReceiver
{
    public void ChooseOption( int option );
}
