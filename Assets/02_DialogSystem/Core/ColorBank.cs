using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct ColorInfo
{
    public string name;
    public Color color;
}

[CreateAssetMenu(menuName = "Dialog/Colorbank")]
public class ColorBank : ScriptableObject
{
    [SerializeField]
    private List<ColorInfo> colors;
    private Dictionary<string, int> lookup;

    void OnEnable()
    {
        RefreshLookup();
    }

    public ColorInfo? GetColor(string hex)
    {
        if( ColorUtility.TryParseHtmlString( hex, out Color color ) )
        {
            for( int i = 0; i < colors.Count; ++i )
            {
                if( colors[i].color == color )
                {
                    return colors[i];
                }
            }
        }

        return null;
    }

    public string AddColor(string hex)
    {
        if( ColorUtility.TryParseHtmlString( hex, out Color color ) )
        {
            colors.Add( new ColorInfo(){ name = $"color{colors.Count}", color = color } );
            RefreshLookup();

            return $"color{colors.Count}";
        }

        return "";
    }

    public void RefreshLookup()
    {
        lookup = new Dictionary<string, int>();
        for( int i = 0; i < colors.Count; ++i )
        {
            ColorInfo color = colors[i];
            lookup.Add( color.name, i );
        }
    }

    public string ReplaceColorWithHex( string colorString )
    {
        if( lookup.TryGetValue( colorString, out int index ) )
        {
            return $"#{ColorUtility.ToHtmlStringRGBA( colors[index].color )}";
        }

        return colorString;
    }
}
