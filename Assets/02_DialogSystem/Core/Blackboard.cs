using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

namespace DialogSystem
{
    [System.Serializable]
    public struct BlackboardData
    {
        public string name;
        public string value;
    }

    [CreateAssetMenu( menuName = "Dialog/Blackboard")]
    public class Blackboard : ScriptableObject
    {
        private static string replaceString = @"{\$|}";
        private static Regex regex = new Regex( replaceString );

        [SerializeField]
        private List<BlackboardData> data;
        private Dictionary<string, int> lookup;

        public BlackboardData this[int index] {
            get => data[index];
        }

        public BlackboardData this[string name] {
            get => data[ lookup[ regex.Replace( name, "" ) ] ];
        }

        public int Count => data.Count;

        public void OnEnable()
            => RefreshLookup();

        public void OnValidate()
            => RefreshLookup();

        public void Put( BlackboardData newData )
        {
            if( lookup.TryAdd(newData.name, data.Count ) )
            {
                data.Add( newData );
                return;
            }

            data[ lookup[newData.name] ] = newData;
        }

        public void Remove( string name )
        {
            data.RemoveAt( lookup[name] );
            RefreshLookup(); // invalidate lookup
        }

        private void RefreshLookup()
        {
            if( lookup == null )
                lookup = new Dictionary<string, int>();

            if( data == null )
                data = new List<BlackboardData>();

            lookup.Clear();

            for( int i = 0; i < data.Count; ++i )
            {
                lookup.Add( data[i].name, i );
            }
        }
    }
}