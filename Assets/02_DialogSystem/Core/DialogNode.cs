namespace DialogSystem
{
    [System.Serializable]
    public class DialogNode
    {
        public string Type;
        public string Name;
        public Character Character;
        public string Label;
        public string Jump;

        public DialogNode( string type, string name, Character character, string label, string jump )
        {
            Type = type;
            Name = name;            
            Character = character;
            Label = label;
            Jump = jump;        
        }

        public virtual string[] Data(int locale) => new string[] {string.Empty};
        public virtual (string, bool) Input(object arg) => (string.Empty, false); // label, isCompleted
    }
}