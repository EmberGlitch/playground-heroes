namespace DialogSystem
{
    public enum DialogNodeState
    {
        Pending = 0,
        Active = 1,
        Completed = 2,
    }
}