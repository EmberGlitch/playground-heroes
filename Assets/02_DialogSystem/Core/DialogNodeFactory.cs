using System;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;


namespace DialogSystem
{
    public struct DialogCreationContext
    {
        public string type;
        public string[] headings;
        public List<List<string>> data;
        public RuntimeSetCharacter characters;
        public ColorBank colorSettings;
    }

    public class DialogNodeFactory
    {
        public static int TypeIndex => lookup[ "Type" ];
        public static int TextIndexStart => mapping.Length - 1;

        public static Dictionary<string, int> lookup;
        public static string[] mapping = {
            "Effect","Character","Type","Label","Jump","Text(nl)"
        };

        public static bool Validate(string[] headings)
        {
            lookup = new Dictionary<string, int>();

            for( int i = 0; i < mapping.Length; ++i)
            {
                if( mapping[i] != headings[i] )
                    return false;

                lookup.Add( mapping[i], i );
            }

            return true;
        }

        //string type, List<List<string>> line, string[] headings
        public static DialogNode Create( DialogCreationContext context ) => context.type switch
        {
            "Text" => CreateTextNode( context ),
            "Choice" => CreateChoiceNode( context ),
            _ => throw new Exception("Unkown node type")
        };

        public static Text CreateTextNode( DialogCreationContext context )
        {   
            Dictionary<string, string> commonItems = GetCommonLineItems( context.data[0] );
            string name = commonItems["Text(nl)"];           

            return new Text( 
                name, 
                NameToCharacter( commonItems["Character"], context.characters ),
                commonItems["Label"], 
                commonItems["Jump"], 
                GetTexts( context.data[0] ) );
        }

        public static Choice CreateChoiceNode( DialogCreationContext context )
        {   
            Dictionary<string, string> commonItems = GetCommonLineItems( context.data[0] );
            string name = commonItems["Text(nl)"];

            ChoiceData[] choices = context.data.Select( x => new ChoiceData(){
                Jump = x[lookup["Jump"]],
                texts = GetTexts( x )
            } ).ToArray();

            return new Choice( 
                name, 
                NameToCharacter( commonItems["Character"], context.characters ),
                commonItems["Label"], 
                commonItems["Jump"],
                choices);
        }

        public static string[] GetTexts( IEnumerable<string> line )
            => line.Skip(TextIndexStart).ToArray();

        private static Character NameToCharacter(string name, RuntimeSetCharacter characterSet)
        {
            if (characterSet != null)
            {
                for( int i =0; i < characterSet.Count; ++i )
                {
                    if( characterSet[i].name == name )
                    {
                        return characterSet[i];
                    }     
                }
                return null;
            }
            return null;
        }

        private static Dictionary<string, string> GetCommonLineItems( List<string> line )
        {
            Dictionary<string,string> result = new Dictionary<string, string>();

            for( int i = 0; i < mapping.Length; ++i )
            {
                result.Add( mapping[i], line[ lookup[ mapping[i] ] ] );
            }

            return result;
        }
    }
}