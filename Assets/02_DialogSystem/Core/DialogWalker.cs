namespace DialogSystem
{
    [System.Serializable]
    public class DialogWalker
    {
        private Dialog dialog;
        [UnityEngine.SerializeField]
        private int index;        

        public Dialog Dialog
            => dialog;

        public DialogNode Current
            => dialog[index];

        public bool DialogDone 
            => Current == null;

        public DialogWalker( Dialog dialog )
        {
            this.dialog = dialog;
            index = 0;
        }

        public bool Next( object input )
        {
            (string jump, bool isCompleted) = Current.Input( input );

            if( isCompleted == false )
                return false;

            if( jump == "STOP" )
            {
                MoveCurrentTo(-1);
                return true;
            }

            DialogNode next = dialog.FindNodeWithLabel( jump );
            if( next == null )
            {
                MoveCurrentTo( ++index );
                return true;
            }            

            MoveCurrentTo( dialog.IndexOf( next ) );
            return true;
        }

        private void MoveCurrentTo( int newIndex )
        {
            index = newIndex;
        }
    }
}