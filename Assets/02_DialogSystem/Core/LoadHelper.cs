using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DialogSystem
{
    public class LoadHelper : MonoBehaviour
    {
        public static LoadHelper instance;
        public ScriptableObject[] Assets;

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        public static void LoadAll()
        {
            instance = Resources.Load<LoadHelper>("LoadHelper");
        }
    }
}