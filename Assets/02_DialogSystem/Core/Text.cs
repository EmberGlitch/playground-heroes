using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DialogSystem
{
    [System.Serializable]
    public class Text : DialogNode
    {
        string[] texts;

        public Text( 
            string name, 
            Character character, 
            string label,
            string jump,
            string[] texts
        ) 
        : base( "text", name, character, label, jump )
        {
            this.texts = texts;
        }

        public override string[] Data(int locale)
        {
            if( locale >= texts.Length )
                return base.Data(locale); // base returns empty response

            return new string[]{ texts[locale] };
        }

        public override (string, bool) Input(object arg)
            => (Jump, true);
    }

}
