using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Ideafixxxer.CsvParser;
using UnityEngine;
using UnityEngine.Networking;

namespace DialogSystem
{
#nullable enable
    [CreateAssetMenu( menuName = "Dialog/Dialog")]
    public class Dialog : ScriptableObject
    {
        private static string colorMatchPattern = @"<color=(?<!\#)([0-9a-zA-Z]+)>";
        private static Regex regex;

        [SerializeField]
        private RuntimeSetCharacter actors;
        [SerializeField]
        private ColorBank colors;
        [SerializeField]
        private List<DialogNode> dialogNodes;

        private UnityWebRequestAsyncOperation asyncOperation;
        private UnityWebRequest unityWebRequest;

        public DialogNode this[int index] {
            get {
                if( index >= 0 && index < dialogNodes.Count )
                    return dialogNodes[index];

#pragma warning disable CS8603 // Possible null reference return.
                return null;
#pragma warning restore CS8603 // Possible null reference return.
            }
        }

        void OnEnable()
        {
            dialogNodes = new List<DialogNode>();

            if( actors == null )
            {
                Debug.LogError($"Please provide an CharacterSet for Dialog \"{name}\"");
                return;
            }

            if( regex == null )
                regex = new Regex( colorMatchPattern );

            Load();
        }

        void OnDisable()
        {
            dialogNodes?.Clear();
        }

        [ContextMenu("Load Dialog")]
        public void Load()
        {
            dialogNodes.Clear();

            string path = Path.Combine( Application.streamingAssetsPath, $"{name}.csv" );
            if( path.StartsWith( "http" ) == false )
                path = $"file://{path}";

            unityWebRequest = UnityWebRequest.Get( path );
            asyncOperation = unityWebRequest.SendWebRequest(); 
            asyncOperation.completed += ProcessFile;
        }


        public DialogNode? FindNodeWithLabel(string label)
        {
            if( label == string.Empty )
                return null;

            return dialogNodes.LastOrDefault( x => x.Label == label );
        }

        public int IndexOf( DialogNode node )
        {
            return dialogNodes.IndexOf( node );
        }

        private void ProcessFile(AsyncOperation operation)
        {    
            if( unityWebRequest.error != null)
            {
                return;
            }

            // keep characters from the extended ascii character set
            string ascii_string = Regex.Replace( unityWebRequest.downloadHandler.text, @"[^\u0000-\u00FF]+", string.Empty );

            string[][]? content = ReadFileContent( ascii_string );
            if( content == null )
                throw new Exception("invalid file");

            DialogCreationContext context = new DialogCreationContext(){
                characters = actors,
                headings = content[0],
                colorSettings = colors
            };

            int typeIndex = DialogNodeFactory.TypeIndex;
            for( int line = 1; line < content.Length; )
            {
                string type = content[line][typeIndex];
                List<List<string>> node = new List<List<string>>();

                // get all options lines until the next dialogNode
                do {
                    string[] columns = content[line];
                    for( int i = 0; i < columns.Length; ++i )
                        columns[i] = ParseColors( columns[i] );

                    node.Add( columns.ToList() );
                    ++line;
                }while( line < content.Length && content[line][typeIndex] == "Option" );
                
                context.data = node;
                context.type = type;
                
                if( type.Length > 0 )
                    dialogNodes.Add( DialogNodeFactory.Create( context ) );
            }
        }   

        private string ParseColors(string x )
        {
            if( colors == null )
                return x;

            return Utils.ReplaceTags( x, regex, (i) => colors.ReplaceColorWithHex( i ) );
        }

        /// <summary>
        /// parse raw file content csv to string[][]
        /// </summary>
        /// <param name="fileContent">text content of file</param>
        /// <returns>parsed csv array [][]</returns>
        private string[][]? ReadFileContent(string fileContent)
        {
            CsvParser csvParser = new CsvParser();
            string[][]? content = csvParser.Parse( fileContent );  
            
            if( content.Length == 0 || DialogNodeFactory.Validate( content[0] ) == false )
                return null;

            return content;
        }
    }
#nullable disable
}   