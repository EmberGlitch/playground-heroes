using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using UnityEngine.Events;
using System.Text.RegularExpressions;

namespace DialogSystem
{
    [System.Serializable]
    public struct DialogEventContext
    {
        public DialogNode previousNode;
        public DialogNode newNode;
        public object input;
    }

    public class DialogBox : MonoBehaviour, IPointerClickHandler, DialogInputReceiver
    {
        [SerializeField]
        private TMPro.TMP_Text text;

        [SerializeField]
        private TMPro.TMP_Text nameLabel;

        [SerializeField]
        private GameObject nameField;

        [SerializeField]
        private Image characterImage;

        [SerializeField]
        private GameObject characterImageContainer;
        
        [SerializeField]
        private GameObject continueButton;

        [SerializeField]
        private ChoiceButton[] buttons;

        [SerializeField]
        private GameObject blocking;

        [Header("Settings")]

        [SerializeField]
        private Blackboard blackboard;

        [SerializeField]
        private Dialog dialog;
        [SerializeField]
        private DialogWalker walker;

        [SerializeField]
        private float TextSpeed = 100;

        [SerializeField]
        private new AudioSourceWrapper audio;

        public UnityEvent<DialogEventContext> OnDialogNodeChanged;
        public UnityEvent OnDialogEnded;

        private string desiredText = string.Empty;
        private int progress;
        private Coroutine textRoutine = null;

        public static Regex regex = new Regex( @"({\$\w+})" );
        
        void OnEnable()
        {
            if( audio == null )
            {
                audio = GetComponent<AudioSourceWrapper>();
            }

            LocaleSetting.SubToLocale( OnLocaleChange );

            if( dialog == null )
            {
                Debug.LogError("no dialog provided");
                enabled = false;
                return;
            }

            walker = new DialogWalker( dialog );
            SetupDialogGUI(false);            
            StartTextAnimation();
        }

        void OnDisable()
        {
            LocaleSetting.UnsubToLocale( OnLocaleChange );
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if( blocking != null && eventData.pointerCurrentRaycast.gameObject == blocking )
                return;

            if( progress < desiredText.Length )
            {
                progress = desiredText.Length;
                return;
            }

            ChooseOption(true);
        }

        public void SetDialog(Dialog dialog)
        {
            if( gameObject.activeInHierarchy )
                return;

            this.dialog = dialog;
        }

        public void ChoiceButton( bool option )
        {
            if( enabled == false )
                return;

            ChooseOption( option );
        }

        public void ChooseOption( int option )
            => ChooseOption( (object) option );

        public void ChooseOption( object optionIndex )
        {
            if( walker.Dialog == null )
                return;

            // cache current node for Event
            DialogNode node = walker.Current;

            // try to move to next
            if( walker.Next( optionIndex ) )
            {
                OnDialogNodeChanged.Invoke( new DialogEventContext(){
                    previousNode = node,
                    newNode = walker.Current,
                    input = optionIndex
                });

                SetupDialogGUI(false);
                StartTextAnimation();
                continueButton.SetActive( false );
            }
        }

        private void OnLocaleChange()
        {
            SetupDialogGUI(true);
        }

        private void SetupDialogGUI(bool force)
        {
            if( walker.DialogDone )
            {
                OnDialogEnded.Invoke();
                gameObject.SetActive( false );
                return;
            }

            ShowCharacter();
            ShowTextBox(force);
            ShowButtons();
        }

        private void ShowTextBox(bool force)
        {
            string[] texts = walker.Current.Data((int)LocaleSetting.localeInfo.locale);
            desiredText = Utils.ReplaceTags( texts[0], regex, (variable) => blackboard[variable].value );
            text.enabled = desiredText.Length > 0;

            if( force ) 
            {
                progress = int.MaxValue;
                text.text = desiredText;
            }
        }

        private void ShowButtons()
        {
            bool showButtons = walker.Current.Type == "choice";
            string[] data = walker.Current.Data( (int)LocaleSetting.localeInfo.locale );

            for( int i = 0; i < buttons.Length; ++i )
            {                    
                ChoiceButton button = buttons[ i ];
                button.gameObject.SetActive( showButtons );

                if( showButtons == false )
                    continue;

                int textIndex = i + 1;
                if( textIndex >= data.Length )
                {
                    button.gameObject.SetActive( false );
                    break;
                }

                button.Setup(this, data[textIndex], i );
            }
        }

        private void ShowCharacter()
        {
            bool showCharacterInfo = walker.Current.Character != null;

            characterImageContainer.SetActive(showCharacterInfo);
            nameField.SetActive(showCharacterInfo);

            if( showCharacterInfo == false )
                return;

            characterImage.sprite = walker.Current.Character.sprite;
            nameLabel.text = walker.Current.Character.name;
            nameLabel.color = walker.Current.Character.color;
        }

        private void StartTextAnimation()
        {
            if( walker.DialogDone || gameObject.activeSelf == false )
                return;

            if( textRoutine != null )
            {
                StopCoroutine(textRoutine);
            }

            textRoutine = StartCoroutine( AnimateText() );
        }

        private IEnumerator AnimateText()
        {
            progress = 0;
            bool foundRichTextTag = false;
            while( progress < desiredText.Length )
            {
                char current = desiredText[progress];
                
                if( current == '>' )
                {
                    foundRichTextTag = false;
                }

                if( current == '<' || foundRichTextTag )
                {
                    foundRichTextTag = true;
                }
                else
                {

                    audio.Play();
                    text.text = desiredText[..(progress+1)];
                    yield return new WaitForSeconds( 1.0f / TextSpeed );
                }
                
                ++progress;
            }

            continueButton.SetActive( walker.Current.Type == "text" );
            text.text = desiredText;
        }

    }
}