using System;
using UnityEngine;
using UnityEngine.UI;

namespace DialogSystem
{
    public class ChoiceButton : MonoBehaviour
    {
        [SerializeField]
        private TMPro.TMP_Text text;

        [SerializeField]
        private Button button;

        private DialogInputReceiver parentBox;
        private string data;
        private int optionIndex;

        void OnEnable()
        {
            button.onClick.AddListener( OnClick );
        }        

        void OnDisable()
        {
            button.onClick.RemoveListener( OnClick );
        }

        public void Setup( DialogInputReceiver parent, string data, int optionIndex )
        {
            parentBox = parent;
            this.data = data;
            this.optionIndex = optionIndex;
            UpdateText();
        }

        private void OnClick()
        {
            parentBox?.ChooseOption( optionIndex );
        }

        private void UpdateText()
        {
            text.text = data;
        }
    }
}