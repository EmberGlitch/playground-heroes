using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Ideafixxxer.CsvParser;
using System.Linq;
using System.IO;

using UnityEditor;
using System.Text.RegularExpressions;
using DialogSystem;

[CreateAssetMenu( menuName = "Dialog/FungusToDialog" )]
public class FungusToDialog : ScriptableObject
{
    public List<TextAsset> fungusDialogs;

    public ColorBank colors;

    [ContextMenu("convert")]
    void Convert()
    {
        CsvParser csvParser = new CsvParser();

        Regex regex = new Regex(@"(?<=\=)([\#A-Z0-9]+)");

        foreach( TextAsset file in fungusDialogs )
        {
            string[][] content = csvParser.Parse( file.text );  
            List<string[]> result = new List<string[]>
            {
                new string[] { "Effect", "Character", "Type", "Label", "Jump", "Text(nl)", "Text(en)" }
            };

            for( int i = 1; i < content.Length; ++i )
            {
                string rawKey = content[i][0].Trim();

                if( rawKey.StartsWith("CHARACTER") )
                    continue;

                string character = rawKey.Split('.').Last().Replace("Playground Heroes", "");
                if( character.StartsWith("\\n") || content[i][2].Length == 0 ) // empty line
                    continue;

                string text = content[i][2];

                text = Regex.Replace( text, @"({)(?!\$)", "<" );
                text = Regex.Replace( text, @"(?<!\$[a-zA-Z]+)(})", ">" );
                text = text.Replace( "\"", "\"\"" );
                
                text = Utils.ReplaceTags(text, regex, (x) => {                    
                    ColorInfo? color = colors.GetColor( x );
                    string colorName = string.Empty;
                    if( color == null )
                        colorName = colors.AddColor( x );
                    else
                        colorName = color?.name;

                    return colorName;
                } );

                string type = "Text";
                if( int.TryParse(character, out int _ ))
                {
                    character = "";
                    type = "Option";
                }
                else if( i + 1 < content.Length )
                {
                    string nextKey = content[i+1][0].Trim().Split('.').Last().Replace("Playground Heroes", "");
                    if( int.TryParse(nextKey, out int _ ))
                        type = "Choice";
                }

                result.Add( new string[] {
                    "",character,type,"","","",$"\"{text}\""
                } );

            }

            string path = $"{Application.streamingAssetsPath}/{file.name}.csv";
            
            using( StreamWriter writer = new StreamWriter(path) )
            {
                foreach( string[] line in result )
                {
                    writer.WriteLine( string.Join(",", line) );   
                }
            }

        }   


    }

}
