using UnityEngine;

[CreateAssetMenu( menuName = "Dialog/RuntimeSet")]
public class RuntimeSetGameObject : RuntimeSet<GameObject>
{   
}
