using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class RuntimeSet<T> : ScriptableObject
{
    [System.Serializable]
    public struct Change
    {
        public T Obj;
        public bool Added;
    }

    [SerializeField]
    private List<T> References;
    public UnityEvent<Change> OnListChange;
    
    public T this [ int index ] => References[index];

    public int Count => References.Count;

    void OnEnable()
    {
        Clear(); // ensure clean startup
    }

    public virtual void Add(T obj)
    {
        if( References.Contains( obj ) )
            return;

        References.Add( obj );
        OnListChange.Invoke( new Change() {Obj = obj, Added = true} );
    }

    public virtual void Remove(T obj)
    {
        References.Remove( obj );
        OnListChange.Invoke( new Change() {Obj = obj, Added = false} );
    }

    public virtual T Find(System.Predicate<T> match)
        => References.Find( match );

    public virtual void Clear()
        => References?.Clear();
}
