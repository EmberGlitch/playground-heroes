using UnityEngine;
using DialogSystem;

[CreateAssetMenu( menuName = "Dialog/CharacterSet" )]
public class RuntimeSetCharacter : RuntimeSet<Character>
{   
}
