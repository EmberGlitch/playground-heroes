using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatchMoveTestThingIDKIDontKnowHowSteerWorksHelp : MonoBehaviour
{
    public float StoppingDistance;
    public float max_Velocity;

    Vector2 position;
    Vector2 velocity;
    Vector2 targetPos;
    Vector2 desired_Velocity;
    Vector2 steering;
    private bool m_inputAllowed;
    void Start()
    {

    }
    void Update()
    {
        if (m_inputAllowed)
        {
            position = position + velocity;
            velocity = (targetPos.normalized - position.normalized) * max_Velocity;
            desired_Velocity = (targetPos.normalized - position.normalized) * max_Velocity;

            float distance = Vector2.Distance(targetPos, position);
            
            if (distance < StoppingDistance)
            {
                float multiplier = distance / StoppingDistance;
                desired_Velocity *= multiplier;
            }
            steering = desired_Velocity - velocity;
        }


        transform.position = position;

    }
}
