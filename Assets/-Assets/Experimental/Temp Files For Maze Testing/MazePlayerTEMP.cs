using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;
using TMPro;

public class MazePlayerTEMP : MonoBehaviour
{
    public bool hasFirstKey, hasSecondKey;
    public bool[] collectedItems;

    [SerializeField] private GameObject playerSpriteHolder;
    [SerializeField] private Sprite maleFace, femaleFace, nonBinaryFace;
    [SerializeField] private GameObject target;

    [SerializeField] private LayerMask hitMask;

    [SerializeField] private Transform p_transform;
    [SerializeField] private Vector3 move_point;
    [SerializeField] private float speed = 0.1f;
    [SerializeField] private Rigidbody2D rigidbody2;

    private SpriteRenderer playerSpriteRenderer;
    private menuUser_script genderChecker;

    private void Start()
    {
        rigidbody2 = GetComponent<Rigidbody2D>();
        genderChecker = FindObjectOfType<menuUser_script>();

        playerSpriteRenderer = GetComponentInChildren<SpriteRenderer>();

        if (!genderChecker)
            return;

        if (genderChecker.gender == "F")
            playerSpriteRenderer.sprite = femaleFace;
        else if (genderChecker.gender == "M")
            playerSpriteRenderer.sprite = maleFace;
        else
            playerSpriteRenderer.sprite = nonBinaryFace;

        
    } 

    private void Update()
    {
        HandleInput();
    }

    public void CheckAllCollectables()
    {
        int isTrue = 0;
        for (int i = 0; i < collectedItems.Length; i++)
        {
            if (collectedItems[i] == true)
            {
                isTrue++;
            }
        }
    }

    public void DestroyGate(GameObject lockedGate)
    {
        Destroy(lockedGate);
    }


    private void HandleInput()
    {

        if (Input.GetMouseButton(0))
        {
            move_point = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            //Debug.Log(Input.mousePosition);
            //Debug.Log(move_point);
            rigidbody2.MovePosition(Vector3.Lerp(rigidbody2.position, new Vector2(move_point.x, move_point.y), speed));

        }

    }
}