using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cone : MonoBehaviour
{
    public GameManager gameManager;

    public GameObject HammerObject;

    [SerializeField] private bool isObjectOne;
    private GameObject playerOb;
    private BuildableObject buildable;
    private bool enableClicked;

    private void Start()
    {
        playerOb = GameObject.Find("PF_Player");
        buildable = gameObject.GetComponentInParent<BuildableObject>();
    }

    private void Update()
    {
        if (enableClicked && Vector2.Distance(playerOb.transform.position, transform.position) < 3f && playerOb.GetComponent<Player>().sc_AIPatch.reachedEndOfPath)
        {
            StartCoroutine(Delay(playerOb.GetComponent<Player>().sc_AIPatch.endReachedDistance));
            enableClicked = false;
        }
    }

    private void Clicked()
    {
        if (isObjectOne)
        {
            gameManager.objectOneBuild = true;
            HammerObject.SetActive(true);

        }
        else if (!isObjectOne)
        {
            gameManager.objectTwoBuild = true;
            HammerObject.SetActive(true);
        }
    }

    private void OnMouseDown()
    {
        enableClicked = true;
    }

    private void OnMouseOver()
    {
        if (!buildable.objectBuild)
        {
            SpriteRenderer spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
            spriteRenderer.sprite = buildable.OutlinedConeFrame;
        }
    }

    private void OnMouseExit()
    {
        if (!buildable.objectBuild)
        {
            SpriteRenderer spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
            spriteRenderer.sprite = buildable.coneFrame;
        }
    }

    private IEnumerator Delay(float delay)
    {
        yield return new WaitForSeconds(delay);
        Clicked();
    }
}