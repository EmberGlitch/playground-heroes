using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueIndicator : MonoBehaviour
{
    public Sprite OutlineSprite;
    public Sprite NormalSprite;

    private SpriteRenderer m_spriteRenderer;

    private void Start()
    {
        m_spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        
    }

    public void OnMouseOver()
    {
        m_spriteRenderer.sprite = OutlineSprite;
    }

    private void OnMouseExit()
    {
        m_spriteRenderer.sprite = NormalSprite;
    }
}