using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HammerEvents : MonoBehaviour
{
    public BuildableObject Buildable;

    public void SpriteSwitchEvent()
    {
        Buildable.CheckForBuild();
    }

    public void EndEvent()
    {
        gameObject.SetActive(false);
    }
}
