using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Pathfinding;

public class GameManager : MonoBehaviour
{
    [Header("LevelSign Sprites")]
    public Sprite graySprite;

    public Sprite colorSprite;
    public Sprite outlineSprite;

    [Header("LevelSign Positions")]
    [SerializeField] private GameObject pos1;

    [SerializeField] private GameObject pos2;
    [SerializeField] private GameObject pos3;
    private GameObject levelSign;

    [Header("Automatically Set")]
    public GameObject player;

    public GameObject level1, level2, level3;

    public bool objectOneBuild = false;
    public bool objectTwoBuild = false;
    public bool canBuild = false;
    public bool confrontable = true;
    public bool chapterFinished = true;

    private Player playerScript;
    private SpriteRenderer signRenderer;
    private GameObject levelsHolder;
    private AIPath aiPath;
    private GameObject currentBullied;
    private Sprite currentSavedSprite;
    
    [SerializeField] private LevelOverzichtManager levelOverzicht;
   
    public static GameManager Singleton;
    
    private void Awake()
    {
        if (Singleton != null && Singleton != this)
        {
            Destroy(gameObject);
            return;
        }
        Singleton = this;

        if( Debug.isDebugBuild == false )
        {
            Debug.unityLogger.logEnabled = false;
        }
    }

    private void Start()
    {
        player = GameObject.Find("PF_Player");
        playerScript = GameObject.Find("PF_Player").GetComponent<Player>();
        aiPath = GameObject.Find("PF_Player").GetComponent<AIPath>();
        levelSign = GameObject.Find("LevelSign");
        signRenderer = GameObject.Find("LevelSign").GetComponent<SpriteRenderer>();
        levelsHolder = GameObject.Find("Levels Holder");
        level1 = GameObject.Find("Level 1 Holder");
        level2 = GameObject.Find("Level 2 Holder");
        level3 = GameObject.Find("Level 3 Holder");

        level2.SetActive(false);
        level3.SetActive(false);

        PlaygroundHeroes.EventSystem.Broadcast( "level", 1 );

        signRenderer.sprite = graySprite;
        levelSign.transform.position = pos1.transform.position;

        AstarPath.active.Scan();

        playerScript.enabled = true;
        chapterFinished = false;
        
    }

    private void Update()
    {
        if (!FindObjectOfType<LevelSign>().isHovering)
        {
            if (objectOneBuild && objectTwoBuild)
                signRenderer.sprite = colorSprite;
            else
                signRenderer.sprite = graySprite;
        }
    }

    public void ActivateNextSign()
    {
        objectOneBuild = false;
        objectTwoBuild = false;
        canBuild = false;
        confrontable = true;
        playerScript.sc_AIPatch.target = null;

        if (level1.activeSelf)
        {
            StartCoroutine(playerScript.ReloadStart());

            level1.SetActive(false);
            level2.SetActive(true);
            PlaygroundHeroes.EventSystem.Broadcast( "level", 2 );
            levelSign.transform.position = pos2.transform.position;
        }
        else if (level2.activeSelf)
        {
            StartCoroutine(playerScript.ReloadStart());

            level2.SetActive(false);
            level3.SetActive(true);
            PlaygroundHeroes.EventSystem.Broadcast( "level", 3 );
            levelSign.transform.position = pos3.transform.position;


            GameObject fade = GameObject.FindGameObjectWithTag( "FadeMe" );
            if( fade != null )
            {
                CanvasGroup group = fade.GetComponent<CanvasGroup>();
                var seq = LeanTween.sequence();
                seq.append( 0.5f );
                seq.append( LeanTween.alphaCanvas( group, 0, 1.0f ) );
            }
        }
        else if (level3.activeSelf)
        {
            levelOverzicht.StartAnimation();
            chapterFinished = true;
            StartCoroutine(Delay());
        }

        AstarPath.active.Scan();
    }

    public void TalkToBully(GameObject bullied, Sprite bulliedHappySprite, string senario)
    {
        currentBullied = bullied;
        currentSavedSprite = bulliedHappySprite;
        SceneManager.LoadScene(senario);
        confrontable = false;
        aiPath.enabled = false;
        //levelsHolder.SetActive(false);
    }

    public void SetBool()
    {
        canBuild = true;
    }

    public void ChangeBulliedSprite()
    {
        SpriteRenderer spriteRenderer = currentBullied.GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = currentSavedSprite;
    }

    public void StartMinigame(string minigameScene, GameObject thisGameObject)
    {
        GameObject aStar = GameObject.Find("A*");
        Destroy(aStar);

        string path;

        for (int i = 0; i < 9; i++)
        {
            path = SceneUtility.GetScenePathByBuildIndex(i);
        }

        SceneManager.LoadScene(minigameScene);
        BoxCollider2D coll = thisGameObject.GetComponent<BoxCollider2D>();
        Destroy(coll);
    }
    IEnumerator Delay()
    {
        yield return new WaitForSeconds(2.3f);
        StartCoroutine(playerScript.ReloadStart());
        levelSign.transform.position = pos2.transform.position;
    }
}
