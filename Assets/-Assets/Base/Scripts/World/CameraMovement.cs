using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    [SerializeField]
    private float cameraLimits;
    [SerializeField]
    private float cameraWidth;

    [SerializeField]
    private float MovementSpeed;

    [SerializeField]
    private Transform player;

    private Vector3 offset;
    private float movement;

    private Camera camera;

    void Start()
    {
        camera = Camera.main;
        cameraWidth = camera.orthographicSize * camera.aspect;
        movement = cameraWidth - cameraLimits;

        offset = transform.position;
        player = GameObject.FindGameObjectWithTag("Player").transform;

        transform.position = TargetPosition();
    }

    void Update()
    {
        transform.position = Vector3.MoveTowards( 
            transform.position, 
            TargetPosition(), 
            MovementSpeed * Time.deltaTime );
    }

    void LateUpdate()
    {
        cameraWidth = camera.orthographicSize * camera.aspect;
        movement = cameraWidth - cameraLimits;
    }

    private Vector3 TargetPosition()
    {
        float position = Mathf.Clamp( player.position.x / (cameraLimits * 0.25f), -1, 1 );
        return offset + new Vector3( position * -movement,0,0);
    }
}
