using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelReenabler : MonoBehaviour
{
    private DontDestroy dontDestroy;

    private void Start()
    {
        dontDestroy = GameObject.Find("Game Holder").GetComponent<DontDestroy>();
        dontDestroy.RestartScene();
    }
}