using System.Collections;
using System.Collections.Generic;
using DialogSystem;
using UnityEngine;

public class SetDialogResult : MonoBehaviour
{
    private List<string> dialogResult = new List<string>()
    {
        "WORST",
        "POOR",
        "GOOD"
    };

    public void OnDialogNodeChanged( DialogEventContext ctx )
    {
        if( ctx.input is bool )
            return;

        (string jump, bool complete) = ctx.previousNode.Input( ctx.input );

        if( complete == false )
            return;

        int index = dialogResult.FindIndex( x => x == jump );
        if( index == -1 )
            return;

        LevelManager.Singleton?.SaveRecentConsequence( index );
    }
}
