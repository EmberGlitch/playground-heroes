using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BullyConfronter : MonoBehaviour
{
    [SerializeField] private DialogueIndicator m_indicator;
    [SerializeField] Transform newTarget;
    [SerializeField] private string connectedSenario;
    [SerializeField] private GameObject bulliedCharacter;
    public static Transform Target;
    [SerializeField] private Sprite bulliedHappy, bulliedSad;

    private GameManager gameManager;

    private bool enableClicked;

    private Animator m_animator;

    public bool TriggerOnStart;
    public int level = -1;

    void Awake()
    {
        PlaygroundHeroes.EventSystem.Add( "level", AutoStart);
    }

    void OnDestroy()
    {
        PlaygroundHeroes.EventSystem.Remove( "level", AutoStart);
    }

    private void AutoStart(object arg0)
    {
        if( level != (int)arg0 || TriggerOnStart == false)
            return;
        
        StartCoroutine( StartConvo() );
    }

    private void Start()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();

        SpriteRenderer spriteRenderer = bulliedCharacter.GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = bulliedSad;

        m_animator = GetComponent<Animator>();
    }

    private void Update()
    {
        if (enableClicked && gameManager.confrontable && Vector2.Distance(gameManager.player.transform.position, newTarget.position) < 5 && gameManager.player.GetComponent<Player>().sc_AIPatch.reachedEndOfPath)
        {
            Clicked();
            enableClicked = false;
        }
    }

    public void Clicked()
    {
        gameManager.TalkToBully(bulliedCharacter, bulliedHappy, connectedSenario);
        gameManager.player.GetComponent<Player>().sc_AIPatch.target = null;
        m_indicator.gameObject.SetActive(false);
    }

    public void SetBool()
    {
        enableClicked = true;
        m_animator.SetBool("Clicked", false);
    }

    private void OnMouseDown()
    {
        gameManager.player.GetComponent<Player>().sc_AIPatch.target = newTarget;
        m_animator.SetBool("Clicked", true);
    }

    private IEnumerator StartConvo()
    {
        yield return new WaitForSeconds(0.05f);
        Clicked();
    }
}