using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum ObjectResult
{
    bad,
    decent,
    good
}

public class BuildableObject : MonoBehaviour
{
    [Header("Object Part Sprites")]
    public Sprite objectParts, shadowObjectParts;

    [Header("Object Bad Sprites")]
    public Sprite objectBad, shadowObjectBad;

    [Header("Object Ok Sprites")]
    public Sprite objectDecent, shadowObjectDecent;

    [Header("Object Good Sprites")]
    public Sprite objectGood, shadowObjectGood;

    [Header("Cone Sprites")]
    public Sprite coneFrame;

    public Sprite OutlinedConeFrame;

    [SerializeField] private SpriteRenderer spriteRenderer, shadowSpriteRenderer;
    [SerializeField] private int minigameScene;
    [SerializeField] private GameObject belowCone;

    public bool objectBuild;
    private GameManager gameManager;
    private GameObject gameHolder;
    private bool minigamePlayed, enableClicked = false;

    private Collider2D m_collider;

    private void Start()
    {
        m_collider = GetComponent<Collider2D>();

        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        gameHolder = GameObject.Find("Game Holder");
        spriteRenderer.sprite = objectParts;
    }

    private void Update()
    {
        Player player = gameManager.player.GetComponent<Player>(); 

        if (enableClicked && gameManager.canBuild && !minigamePlayed && Vector2.Distance(gameManager.player.transform.position, transform.position) < 3 && player.sc_AIPatch.reachedEndOfPath)
        {
            enableClicked = false;
            StartCoroutine(Delay(player.sc_AIPatch.endReachedDistance));
            //gameManager.StartMinigame(minigameScene, this.gameObject);
        }
    }

    private void Clicked()
    {
        LevelManager.Singleton.EnterMinigame(minigameScene, gameManager.player.transform.position, this);
        minigamePlayed = true;
    }

    private void OnMouseDown()
    {
        enableClicked = true;
    }

    public void ChangeObjectState()
    {
        gameHolder.SetActive(true);
        belowCone.SetActive(true);
        m_collider.enabled = false;
    }

    public void CheckForBuild()
    {
        if (minigamePlayed && !objectBuild && Vector2.Distance(gameManager.player.transform.position, transform.position) < 3)
        {
            switch(LevelManager.Singleton.GetLatestResult())
            {
                case MinigameResult.BAD: 
                    spriteRenderer.sprite = objectBad;
                    if (shadowObjectBad != null)
                    {
                        shadowSpriteRenderer.sprite = shadowObjectBad;
                    }
                    break;
                case MinigameResult.DECENT: 
                    spriteRenderer.sprite = objectDecent;
                    if (shadowObjectDecent != null)
                    {
                        shadowSpriteRenderer.sprite = shadowObjectDecent;
                    }
                    break;
                case MinigameResult.GOOD:
                    spriteRenderer.sprite = objectGood;
                    if (shadowObjectGood != null)
                    {
                        shadowSpriteRenderer.sprite = shadowObjectGood;
                    }
                    break;
            }

            objectBuild = true;
            belowCone.SetActive(false);
        }
    }

    private IEnumerator Delay(float delay)
    {
        yield return new WaitForSeconds(delay);
        Clicked();
    }
}