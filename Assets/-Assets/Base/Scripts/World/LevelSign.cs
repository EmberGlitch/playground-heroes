using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Pathfinding;

public class LevelSign : MonoBehaviour
{
    [SerializeField] private Transform newTarget;

    private SpriteRenderer spriteRenderer;
    private GameManager gameManager;
    private Sprite savedSprite;

    public bool isHovering, enableClicked = false;

    private void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
    }

    private void Update()
    {
        if (enableClicked && gameManager.objectOneBuild && gameManager.objectTwoBuild && Vector2.Distance(gameManager.player.transform.position, newTarget.position) < 3 && gameManager.player.GetComponent<Player>().sc_AIPatch.reachedEndOfPath)
        {
            gameObject.GetComponent<Collider2D>().enabled = false;
            StartCoroutine(Delay(gameManager.player.GetComponent<Player>().sc_AIPatch.endReachedDistance));
            enableClicked = false;
        }
    }

    public void Clicked()
    {
        gameObject.GetComponent<Collider2D>().enabled = true;
        gameManager.ActivateNextSign();
    }

    private void OnMouseDown()
    {
        if (isHovering)
        {
            enableClicked = true;
            gameManager.player.GetComponent<Player>().sc_AIPatch.target = newTarget;
        }
    }

    private void OnMouseOver()
    {
        savedSprite = spriteRenderer.sprite;

        if (spriteRenderer.sprite != gameManager.graySprite)
        {
            isHovering = true;
            spriteRenderer.sprite = gameManager.outlineSprite;
        }
    }

    private void OnMouseExit()
    {
        spriteRenderer.sprite = savedSprite;
        isHovering = false;
    }

    private IEnumerator Delay(float delay)
    {
        yield return new WaitForSeconds(delay);
        Clicked();
    }
}