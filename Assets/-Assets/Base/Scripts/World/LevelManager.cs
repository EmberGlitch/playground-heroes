using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum MinigameResult
{
    BAD = 0,
    DECENT = 1,
    GOOD = 2,
}

public class LevelManager : MonoBehaviour
{
    public IntReference Clicks;

    public static LevelManager Singleton;

    public GameObject GameHolder;

    public int GoodThreshold;
    public int OkeThreshold;

    public Image FadeImage;
    public Animator Animator;

    private BuildableObject m_recentBuildable;

    private int m_recentSubLevel;
    private int m_recentMinigameLevel;

    private int m_recentConsequence;
    private int m_latestResult;

    //Maybe make this a list (or maybe a dictionary?), don't know how any one person will go through each sub-level (needs testing)
    private float m_recentGameOutcome;

    private Vector3 m_recentOverworldPosition;

    private void Awake()
    {
        if (Singleton != null && Singleton != this)
        {
            Destroy(Singleton.gameObject);
        }

        Singleton = this;
        DontDestroyOnLoad(gameObject);
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Clicks.Value++;
        }
    }

    public void MoveCamera()
    {
        //Put each sub-level next to each other
        //Switch sub-level, similar to the original legend of zelda
        //For reference:
        //https://www.youtube.com/watch?v=wtXXPfjpCsI
    }

    public IEnumerator LevelFade(bool _minigame, int sceneOverride = -1)
    {
        Animator.SetBool("Fade", true);
        yield return new WaitUntil(() => FadeImage.color.a == 1);

        if (_minigame)
        {
            SceneManager.LoadScene(m_recentMinigameLevel);
            GameHolder.SetActive(false);
        }
        else
        {
            int scene = sceneOverride == -1? m_recentSubLevel : sceneOverride;

            SceneManager.LoadScene(scene);
            m_recentBuildable?.ChangeObjectState();
            if( GameManager.Singleton != null )
                GameManager.Singleton.player.transform.position = m_recentOverworldPosition;
        }

        Animator.SetBool("Fade", false);
        yield return new WaitUntil(() => FadeImage.color.a == 0);
    }

    public void ReturnToMainMenu()
    {
        StartCoroutine(LevelFade(false, 1));
    }

    public void ReturnToSubLevel()
    {
        StartCoroutine(LevelFade(false));
    }

    public void EnterMinigame(int _gamelevelindex, Vector3 _position, BuildableObject _buildable)
    {
        SaveRecentSubLevel();

        m_recentOverworldPosition = _position;
        m_recentBuildable = _buildable;
        m_recentMinigameLevel = _gamelevelindex;

        StartCoroutine(LevelFade(true));
    }

    public void SaveRecentSubLevel()
    {
        m_recentSubLevel = SceneManager.GetActiveScene().buildIndex;
    }

    public void SaveRecentConsequence(int _consequencetype)
    {
        m_recentConsequence = _consequencetype;
    }

    public void SaveMinigameOutcome(float _percentage, int _goodthreshold, int _okethreshold)
    {
        GoodThreshold = _goodthreshold;
        OkeThreshold = _okethreshold;

        m_recentGameOutcome = _percentage;
        m_recentMinigameLevel = SceneManager.GetActiveScene().buildIndex;
    }

    public void SaveMinigameOutcome( int result )
    {
        m_latestResult = result;
    }

    public MinigameResult GetLatestResult() => (MinigameResult)m_latestResult;

    public int GetConsequence()
    {
        return m_recentConsequence;
    }

    public float GetMinigameOutcome()
    {
        return m_recentGameOutcome;
    }

    public int GetMinigameLevel()
    {
        return m_recentMinigameLevel;
    }
}