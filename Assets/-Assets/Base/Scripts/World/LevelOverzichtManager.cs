using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;

public class LevelOverzichtManager : MonoBehaviour
{
    [SerializeField] private GameObject[] Characters;
    [SerializeField] private Transform[] LevelHolders;
    [SerializeField] private Transform Player, MoveCamera, MainCamera;
    [SerializeField] private Animator anim;
    [SerializeField] private Image Fade;
    [SerializeField] private Vector3 Lvl1, Lvl2, Lvl3;
    [SerializeField] private GameObject StaticLevelBorder, MovingLevelBorder;

    [SerializeField] private CameraMovement movement;

    private float index;
    private bool enableMTC;
    private float MoveThreshold;

    private void Awake()
    {
        GetCharacters();

        movement = FindObjectOfType<CameraMovement>();
        StaticLevelBorder.SetActive(true);
        MovingLevelBorder.SetActive(false);
    }

    private void Update()
    {
        if (enableMTC)
            MoveTheCamera();
    }

    private void GetCharacters()
    {
        Characters = GameObject.FindGameObjectsWithTag("GetPeople");
        Player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    private void MoveTheCamera()
    {

        MoveThreshold = Input.mousePosition.x / Screen.width;
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow) || MoveThreshold <= 0.1f)
            index -= 0.05f;
        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow) || MoveThreshold >= 0.9f)
            index += 0.05f;

        MoveCamera.gameObject.SetActive(true);
        MainCamera.gameObject.SetActive(false);

        index = Mathf.Clamp(index, 0.07f, 38f);

        MoveCamera.position = new Vector3(index, 0.45f, -10f);
    }

    private void LevelPos()
    {
        LevelHolders[0].position = Lvl1;
        LevelHolders[1].position = Lvl2;
        LevelHolders[2].position = Lvl3;

        for (int i = 0; i < LevelHolders.Length; i++)
        {
            LevelHolders[i].gameObject.SetActive(true);
        }
    }
    public void StartAnimation()
    {
        StaticLevelBorder.SetActive(false);
        MovingLevelBorder.SetActive(true);
        anim.enabled = true;
        anim.SetTrigger("Start");
        movement.enabled = false;


        StartCoroutine(Delay());
        StartCoroutine(DelayCam());
    }

    IEnumerator Delay()
    {
        yield return new WaitForSeconds(2.3f);
        index = 38f;
        LevelPos();
        for (int i = 0; i < Characters.Length; i++)
        {
            Characters[i].gameObject.SetActive(false);
            Player.gameObject.SetActive(false);
        }
    }
    IEnumerator DelayCam()
    {
        yield return new WaitForSeconds(26f);
        enableMTC = true;
    }
}
