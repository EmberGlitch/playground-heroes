using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinigameIndicator : MonoBehaviour
{
    public GameObject OutlineSprite;

    private bool m_isActive;
    private BuildableObject m_BuildableObject;

    private void Awake()
    {
        OutlineSprite.SetActive(false);
        m_isActive = false;

        m_BuildableObject = GetComponent<BuildableObject>();
    }

    private void Update()
    {
        if (GameManager.Singleton.canBuild && !m_isActive)
        {
            OutlineSprite.SetActive(true);
            m_isActive = true;
        }
        
        if (m_BuildableObject.objectBuild)
        {
            OutlineSprite.SetActive(false);
        }
    }

    private void OnMouseOver()
    {
        if (GameManager.Singleton.canBuild && !m_BuildableObject.objectBuild)
        {
            OutlineSprite.SetActive(false);
        }
    }

    private void OnMouseExit()
    {
        if (GameManager.Singleton.canBuild && !m_BuildableObject.objectBuild)
        {
            OutlineSprite.SetActive(true);
        }
    }
}
