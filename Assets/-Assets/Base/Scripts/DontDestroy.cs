using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class DontDestroy : MonoBehaviour
{
    private GameObject holding;
    private AIPath aiPath;
    private GameManager gameManager;

    void Start()
    {
        DontDestroyOnLoad(this.gameObject);
        holding = GameObject.Find("Levels Holder");
        gameManager = FindObjectOfType<GameManager>();
        aiPath = GameObject.Find("PF_Player").GetComponent<AIPath>();
    }

    public void RestartScene()
    {
        holding.SetActive(true);
        gameManager.ChangeBulliedSprite();
        aiPath.enabled = true;
        AstarPath aStar = GameObject.Find("A*").GetComponent<AstarPath>();
        aStar.Scan();
    }
}
