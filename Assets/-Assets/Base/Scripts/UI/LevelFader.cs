using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelFader : MonoBehaviour
{
    public bool isNeeded;
    public Image image;
    public Animator anim;

    // Start is called before the first frame update
    public IEnumerator FadeToScene()
    {
        //anim.SetBool("Fade", true);
        //yield return new WaitUntil(() => image.color.a == 1);
        yield return new WaitForSeconds(0.1f);
        //SceneManager.LoadScene(scene);
        LevelManager.Singleton.ReturnToSubLevel();

        Destroy(image);
    }

    public IEnumerator FadeToMinigame(int _index)
    {
        yield return new WaitUntil(() => image.color.a == 0);
    }

    public void DestroyImage()
    {
        if (!isNeeded)
        {
            Destroy(gameObject);
        }
    }
}