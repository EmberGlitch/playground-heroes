using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class RememberName : MonoBehaviour
{
    public string oldName;
    public string gender;

    [SerializeField] private new TMP_InputField name;
    [SerializeField] private Toggle toggleF, toggleM;

    private void Start()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    public void GetName()
    {
        oldName = name.text;

        if (toggleF.isOn)
            gender = "F";
        else if (toggleM.isOn)
            gender = "M";
        else
            gender = "N";
    }
}
