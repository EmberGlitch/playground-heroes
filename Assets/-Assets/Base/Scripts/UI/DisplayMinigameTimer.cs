using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayMinigameTimer : MonoBehaviour
{
    [SerializeField]
    private MinigameTimer timer;

    [SerializeField]
    private bool UseText;

    [SerializeField]
    private TMPro.TMP_Text text;
    
    [SerializeField]
    private Image timerImage;

    IEnumerator Start()
    {
        if( text == null )
            text = GetComponent<TMPro.TMP_Text>();
        
        if( timerImage == null )
            timerImage = GetComponent<Image>();

        yield return null;

        timer = FindObjectOfType<MinigameTimer>();
    }

    void Update()
    {
        if( timer == null )
            return;

        if( text != null && UseText == true)
            text.text = Mathf.CeilToInt( timer.CurrentTime ).ToString();

        if( timerImage != null && UseText == false )
            timerImage.fillAmount = Mathf.Ceil(timer.CurrentTime) / timer.Max;
    }

}
