using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeTextColor : MonoBehaviour
{
    private Text changingText;

    void Start()
    {
        changingText = GetComponentInChildren<Text>();
    }

    private void OnMouseEnter()
    {
        changingText.color = new Color(1, 1, 1);
    }

    private void OnMouseExit()
    {
        changingText.color = new Color(0.42f, 0.42f, 0.42f);
    }
}
