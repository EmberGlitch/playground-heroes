using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Buttons : MonoBehaviour
{
    [SerializeField] private GameObject confirmExit;
    [SerializeField] private bool isMainMenu;
    [SerializeField] private string connectedScene;

    [SerializeField] private GameManager gameManager;

    private Player player;
    private void Start()
    {
        player = GameObject.FindObjectOfType<Player>();
    }
    public void LoadScene()
    {
        SceneManager.LoadScene(connectedScene);
        GameObject sceneHolder = GameObject.Find("Game Holder");
        Destroy(sceneHolder);
    }
    public void ExitGame()
    {
        Application.Quit();
    }

    public void ConfirmExit()
    {
        if (isMainMenu)
        {
            confirmExit.SetActive(true);
        }
        else
        {
            if (player != null)
            {
                StartCoroutine(player.PrefentMovement());
            }

            if( gameManager != null && gameManager.chapterFinished )
            {
                LoadScene();
                return;
            }
                

            confirmExit.SetActive(true);
        }
    }

    public void CloseConfirmer()
    {
        confirmExit.SetActive(false);
    }

    public void DestroyAllDontDestroyOnLoadObjects()
    {

        GameObject gameObject = new GameObject("Sacrificial Object");
        DontDestroyOnLoad(gameObject);

        foreach (var root in gameObject.scene.GetRootGameObjects())
            if (root.name == "CharacterNameHolder")
            {
                root.name = "OldCharacterName";
            }
            else if (root.name != "FungusManager" || root.name == "OldCharacterName")
            {
                Destroy(root);
            }
    }
}
