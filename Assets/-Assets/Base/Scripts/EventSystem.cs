using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace PlaygroundHeroes
{
    public class EventSystem : MonoBehaviour
    {
        private static Dictionary<string, UnityAction<object>> channels = new Dictionary<string, UnityAction<object>>();
        
        public static void Broadcast( string name, object arg )
        {
            if( channels.ContainsKey( name ) )
            {
                channels[name].Invoke( arg );
            }
        }

        public static void Add(string name, UnityAction<object> callback)
        {
            if( channels.ContainsKey( name ) )
            {
                channels[name] += callback;
                return;
            }

            channels.Add( name, new UnityAction<object>( callback ) );
        }

        public static void Remove( string name, UnityAction<object> callback)
        {
            if( channels.ContainsKey( name ) )
            {
                channels[name] -= callback;
            }
        }
    }
}