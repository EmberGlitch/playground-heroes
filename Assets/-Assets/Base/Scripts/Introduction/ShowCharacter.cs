using System;
using System.Collections;
using DialogSystem;
using UnityEngine;

public class ShowCharacter : MonoBehaviour
{
    [SerializeField]
    private DialogBox dialogBox;
    [SerializeField]
    private Character characterName;
    [SerializeField]
    private new SpriteRenderer renderer;
    
    [Header("Fade Settings")]
    [SerializeField]
    private AnimationCurve curve;
    [SerializeField, Range(0,5)]
    private float fadeDuration = 2.0f;

    private float alpha = 0;

    void Start()
    {
        renderer = GetComponent<SpriteRenderer>();

        dialogBox.OnDialogNodeChanged.AddListener( OnDialogNodeChanged );
    }

    private void OnDialogNodeChanged(DialogEventContext arg0)
    {
        if( arg0.newNode.Character == null )
            return;

        if( arg0.newNode.Character.name == characterName.name )
        { 
            StartCoroutine( Show() );

            dialogBox.OnDialogNodeChanged.RemoveListener( OnDialogNodeChanged );
        }
    }

    private IEnumerator Show()
    {
        Color color = Color.white;
        color.a = 0;

        while( alpha < 1 )
        {
            alpha += Time.deltaTime / fadeDuration;
            color.a = curve.Evaluate( alpha );

            renderer.color = color;

            yield return  null;
        }

        color.a = 1;
        renderer.color = color;
        
    }
}
