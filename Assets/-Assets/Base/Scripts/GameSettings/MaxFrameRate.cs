using UnityEngine;

public class MaxFrameRate
{
    [RuntimeInitializeOnLoadMethod]
    static void OnRuntimeMethodLoad()
    {
        Application.targetFrameRate = 60;
    }
}