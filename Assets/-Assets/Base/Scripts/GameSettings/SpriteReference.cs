using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Data/SpriteVariableReference")]
public class SpriteReference : ScriptableObject
{
    public Sprite spriteRenderer;
}
