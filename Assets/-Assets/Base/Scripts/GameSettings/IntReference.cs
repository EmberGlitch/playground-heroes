using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Data/InVariableReference")]
public class IntReference : ScriptableObject
{
    public int Value;
}
