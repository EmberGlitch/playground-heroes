using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TotalPlayTimer : MonoBehaviour
{
    public float m_Time;
    private static TotalPlayTimer Singleton;

    private void Awake()
    {
        if (Singleton != null && Singleton != this)
        {
            Destroy(this);
        }

        Singleton = this;
    }

    void Update()
    {
        m_Time += Time.deltaTime;
    }
}
