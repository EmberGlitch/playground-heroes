using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class DataCollection : MonoBehaviour
{
    public static DataCollection Singleton;

    private void Awake()
    {
        if (Singleton != null && Singleton != this)
        {
            Destroy(Singleton.gameObject);
        }

        Singleton = this;
        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {

        
    }

}
