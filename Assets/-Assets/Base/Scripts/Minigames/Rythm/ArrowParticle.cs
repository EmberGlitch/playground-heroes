using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rythm
{
    public class ArrowParticle : MonoBehaviour
    {
        [SerializeField]
        private new ParticleSystem particleSystem;

        private void OnValidate()
        {
            particleSystem = GetComponent<ParticleSystem>();
        }

        public void Emit(PresResultData result)
        {
            if( result.result == PresResult.Missed )
                return;

            particleSystem.Emit(1);
        }
    }
}