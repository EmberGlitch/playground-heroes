using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rythm
{
    public class ArrowTimeOut : MonoBehaviour
    {
        [SerializeField]
        private Arrow arrow;

        void OnValidate()
        {
            arrow = GetComponentInParent<Arrow>();
        }

        void Start()
        {
            arrow.ListenToOnPress( OnPressed );
        }

        public void Enable()
        {
            enabled = true;
        }

        public void OnPressed( PresResultData data )
        {
            if( data.result == PresResult.Missed )
                return;

            enabled = false;
        }

        public void OnArrowTimeOut()
        {
            if( enabled == true )
            {
                arrow.Miss();
                enabled = false;
            }
        }
    }
}