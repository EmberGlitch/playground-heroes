using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Rythm
{
    public class HealthGraphic : MonoBehaviour
    {
        [SerializeField]
        private GameObject[] notes;

        public void OnHealthChange( float health )
        {
            float step = 1.0f / notes.Length;

            for (int i = 0; i < notes.Length; i++)
            {

                notes[i].SetActive( step * i <= health );
            }
        }
    }
}