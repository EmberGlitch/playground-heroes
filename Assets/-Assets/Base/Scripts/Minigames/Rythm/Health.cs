using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Rythm
{
    public class Health : MonoBehaviour
    {
        [SerializeField]
        private float value;
        [SerializeField]
        private UnityEvent<float> OnHealthChange;

        public void ListenToOnChange(UnityAction<float> func)
        {
            OnHealthChange.AddListener(func);
        }

        public void Change(float value)
        {
            this.value += value;
            OnHealthChange.Invoke( this.value );
        }

    }
}