using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Timeline;
using UnityEngine.Playables;
using System;

namespace Rythm
{
    public class RythmManager : MonoBehaviour
    {
        [SerializeField]
        private PlayableDirector director;

        [SerializeField]
        private Animator gameArea;

        [SerializeField]
        private Health health;

        [Header("Settigns")]
        [SerializeField]
        public ConclusionManager Conclusion;
        public FloatReference Score;
        public int MissedNotes;

        [SerializeField]
        private Animator conductor;

        [SerializeField]
        private float MissPenalty;

        [SerializeField]
        private AudioMixerSnapshot gameOver;

        [SerializeField]
        private float gameOverTransitionDuration;

        private void OnValidate()
        {
            if (!health)
                health = FindObjectOfType<Health>(true);
        }

        private IEnumerator Start()
        {
            BackgroundMusic.Instance.Mute();

            yield return new WaitForSeconds(1.0f);

            conductor.SetBool( "started", true );
            director.stopped += OnDirectorStopped;
            director.Play();
        }

        public void OnHealthChange(float health)
        {
            MissedNotes++;

            if (Score.Value < 0)
                return;

            Score.Value = Mathf.Max(0, health) * 100;
            if (Score.Value == 0)
            {
                GameOver();
            }
        }

        public void OnArrowPressed(PresResultData result)
        {
            if (result.result == PresResult.Missed)
            {
                health.Change(MissPenalty);
                conductor.SetTrigger( "Fail" );
             }
        }

        public void ExitLevel()
        {
            BackgroundMusic.Instance.UnMute();
            LevelManager.Singleton.ReturnToSubLevel();
        }

        private void GameOver()
        {
            if (LevelManager.Singleton != null)
            {
                LevelManager.Singleton.SaveMinigameOutcome(Score.Value, 75, 25);
            }

            conductor.SetBool( "started", false );
            // fade out gameplay
            if (!gameArea)
                return;

            gameOver.TransitionTo(gameOverTransitionDuration);
            gameArea.Play("fadeOut");

            // show end screen
            Conclusion.StartEndingDialogue();
        }

        private void OnDirectorStopped(PlayableDirector obj)
        {
            GameOver();
        }
    }
}