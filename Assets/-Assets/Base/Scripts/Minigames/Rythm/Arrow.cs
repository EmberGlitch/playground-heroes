using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Rythm
{
    public enum PresResult
    {
        Missed,
        Good,
        Perfect
    }

    [System.Serializable]
    public struct PresResultData
    {
        public PresResult result;
        public Arrow arrow;
    }

    public class Arrow : MonoBehaviour, IPointerDownHandler
    {
        [Header("Object References")]
        [SerializeField]
        private RectTransform[] arrows;

        [SerializeField]
        private Graphic arrowImage;
        

        [Header("Settings")]
        [SerializeField]
        private float threshold = 0.1f;
        [SerializeField]
        private float threshold_perfect = 0.02f;

        [SerializeField]
        private AnimationCurve highlightCurve;
        [SerializeField]
        private Color highlightColor;
        [SerializeField]
        private Color defaultColor;

        [SerializeField]
        private KeyCode[] keys;
        
        [SerializeField]
        private UnityEvent<PresResultData> OnPressed;

        private new RectTransform transform;

        void OnValidate()
        {
            if( arrowImage )
                arrowImage.color = defaultColor;
        }

        void Start()
        {
            transform = base.transform as RectTransform;
        }

        void Update()
        {
            CheckForInput();

            UpdateArrowImageHighLight();
        }

        public void ListenToOnPress(UnityAction<PresResultData> action)
        {
            OnPressed.AddListener( action );
        }

        public void Check()
        {
            PresResultData result = new PresResultData();
            result.arrow = this;
            result.result = PresResult.Missed;
            
            // check wether an arrows is within threshold distance
            for( int i = 0; i < arrows.Length; ++i )
            {
                float diff = Mathf.Abs( arrows[i].anchorMax.y );
                // hit check
                if( diff < threshold )
                {
                    // pres result
                    result.result = PresResult.Good;
                    if( diff < threshold_perfect )
                        result.result = PresResult.Perfect;

                    break;
                }
            }

            OnPressed.Invoke( result );
        }

        public void Miss()
        {
            PresResultData data = new PresResultData();
            data.arrow = this;
            data.result = PresResult.Missed;
            OnPressed.Invoke(data);
        }

        public void OnPointerDown( PointerEventData eventData )
        {
            Check();
        }
        
        private void CheckForInput()
        {
            for( int i = 0; i < keys.Length; ++i )
            {
                if ( Input.GetKeyDown( keys[i] ) )
                {
                    Check();
                    break;
                }
            }
        }

        private void UpdateArrowImageHighLight()
        {
            // get closest arrow
            float closest = 1000;
            for( int i = 0; i < arrows.Length; ++i )
            {
                float diff = Mathf.Abs( arrows[i].anchorMax.y );
                if( diff < closest )
                    closest = diff;
            }

            // update color
            arrowImage.color = Color.Lerp(defaultColor, highlightColor, highlightCurve.Evaluate(closest));
        }
    }
}