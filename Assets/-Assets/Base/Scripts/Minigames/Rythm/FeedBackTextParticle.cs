using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rythm
{
    public class FeedBackTextParticle : MonoBehaviour
    {
        [SerializeField]
        private PresResult EmitOnResult;

        [SerializeField]
        private new ParticleSystem particleSystem;

        [SerializeField]
        private float heightOffset = 100;

        void OnValidate()
        {
            particleSystem = GetComponent<ParticleSystem>();
        }

        public void OnPressed(PresResultData data)
        {
            if( data.result != EmitOnResult )
                return;

            ParticleSystem.EmitParams emitParams = new ParticleSystem.EmitParams();
            emitParams.position = data.arrow.transform.position + Vector3.up * heightOffset;

            particleSystem.Emit(emitParams, 1);
        }

    }
}