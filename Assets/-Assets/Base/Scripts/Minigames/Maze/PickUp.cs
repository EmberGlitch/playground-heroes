using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PickUp : MonoBehaviour
{
    public FloatReference Score;
    public IntReference PickUpData;

    [Header("Collectable Item UI")]
    [SerializeField] private Image connectedSprite;

    private MazePlayerTEMP player;

    private void Start()
    {
        player = GameObject.FindObjectOfType<MazePlayerTEMP>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            if (gameObject.tag == "Key")
            {
                player.hasFirstKey = true;
                Destroy(gameObject);
            }
            else if (gameObject.tag == "Key2")
            {
                player.hasSecondKey = true;
                Destroy(gameObject);
            } 
            else
            {
                for (int i = 0; i < player.collectedItems.Length; i++)
                {
                    if (player.collectedItems[i] == false)
                    {
                        PickUpData.Value++;

                        connectedSprite.color = Color.white;

                        Score.Value += 33;

                        player.collectedItems[i] = true;
                        player.CheckAllCollectables();
                        Destroy(gameObject);
                        break;
                    }
                }
            }
        }
    }
}