using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MazePreview : MonoBehaviour
{
    [SerializeField] private GameObject inGameCanvas;
    [SerializeField] private GameObject fogOfWarCanvas;
    [SerializeField] private float previewLength;

    private GameObject mainCam;
    private MazePlayerTEMP player;

    void Start()
    {
        player = FindObjectOfType<MazePlayerTEMP>();
        mainCam = GameObject.Find("Main Camera");

        mainCam.SetActive(false);
        player.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (previewLength >= 0)
        {
            previewLength -= Time.deltaTime;
        }
        if (previewLength < 0)
        {
            gameObject.SetActive(false);
            player.enabled = true;
            inGameCanvas.SetActive(true);
            fogOfWarCanvas.SetActive(true);
            mainCam.SetActive(true);
        }
    }
}
