using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScale : MonoBehaviour
{
    [SerializeField]
    private float defaultAspect = 1.778947f;
    [SerializeField]
    private float lowestAspect = 1.333837f;
    [SerializeField]
    private float defaultCameraSize = 12;
    [SerializeField]
    private float maxCameraSize = 15.5f;

    [SerializeField]
    private new Camera camera;

    void Start()
    {
        camera = GetComponent<Camera>();
        float diff = lowestAspect - defaultAspect;
        float current = camera.aspect - defaultAspect;
        
        float sizeDiff = maxCameraSize - defaultCameraSize;
        camera.orthographicSize = defaultCameraSize + ( (current / diff) * sizeDiff );
    }
}
