using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MazeCamera : MonoBehaviour
{
    private Transform player;

    void Start()
    {
        player = GameObject.Find("PR_MazePlayer").transform;
    }

    void LateUpdate()
    {
        transform.position = new Vector3(player.position.x, player.position.y, -10);
    }
}
