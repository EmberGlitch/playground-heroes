using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;
using TMPro;

public class MazePlayer : MonoBehaviour
{
    public bool hasFirstKey, hasSecondKey;
    public bool[] collectedItems;

    [SerializeField] private GameObject playerSpriteHolder;
    [SerializeField] private Sprite maleFace, femaleFace, nonBinaryFace;
    //[SerializeField] private AIPath sc_AIPatch;
    [SerializeField] private GameObject target;

    [SerializeField] private LayerMask hitMask;

    //[SerializeField] private float maximumWalkDistance;
    [SerializeField] private Transform p_transform;
   // [SerializeField] private Vector3 moveToo;
    [SerializeField] private Vector3 move_point;
    [SerializeField] private float speed = 0.1f;

    private SpriteRenderer playerSpriteRenderer;
    private menuUser_script genderChecker;
    //private GameObject lastLoc;
    //private Vector2 lastClick;

    private void Start()
    {
        //playerSpriteRenderer = playerSpriteHolder.GetComponent<SpriteRenderer>();
        //lastLoc = GameObject.Find("LastLoc");

        

        //PathToTarget();

        genderChecker = FindObjectOfType<menuUser_script>();

        if (!genderChecker)
            return;

        if (genderChecker.gender == "F")
            playerSpriteRenderer.sprite = femaleFace;
        else if (genderChecker.gender == "M")
            playerSpriteRenderer.sprite = maleFace;
        else
            playerSpriteRenderer.sprite = nonBinaryFace;
    }

    //private void PathToTarget()
    //{
    //    sc_AIPatch.destination = target.transform.position;
    //    sc_AIPatch.SearchPath();
    //    sc_AIPatch.canMove = sc_AIPatch.remainingDistance < maximumWalkDistance;
    //}

    private void Update()
    {
        HandleInput();
    }

    public void CheckAllCollectables()
    {
        int isTrue = 0;
        for (int i = 0; i < collectedItems.Length; i++)
        {
            if (collectedItems[i] == true)
            {
                isTrue++;
            }
        }

        for (int i = 0; i < collectedItems.Length; i++)
        {
            if (collectedItems[i] == false)
            {
                break;
            }
            else
            {
            }
        }
    }

    public void DestroyGate(GameObject lockedGate)
    {
        Destroy(lockedGate);
        RescanMap();
    }

    private void RescanMap()
    {
        AstarPath.active.Scan();
    }

    private void HandleInput()
    {
        
        if (Input.GetMouseButton(0))
        {
            //////Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            //////RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction, 100, hitMask);
            //////if (hit.collider != null)
            //////{
            //////    lastClick = hit.point;
            //////    lastLoc.transform.position = new Vector3(lastClick.x, lastClick.y, 0);
            //////    if (Vector2.Distance(lastLoc.transform.position, transform.position) <= 5)
            //////    {
            //////        target.transform.position = lastLoc.transform.position;
            //////        PathToTarget();
            //////    }
            //////}

            //Vector2 p_direction = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            //float angle = Mathf.Atan2(p_direction.y, p_direction.x) * Mathf.Rad2Deg;
            //Quaternion rotation = Quaternion.AngleAxis(angle - 90, Vector3.forward);


            move_point = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            transform.position = Vector3.Lerp(transform.position, move_point, speed);
            

        }

    }
}