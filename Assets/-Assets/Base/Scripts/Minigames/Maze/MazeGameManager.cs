using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class MazeGameManager : MonoBehaviour
{
    public ConclusionManager Conclusion;
    public FloatReference Score;
    public float PlayTime;

    [SerializeField] private float secondsToPlay;
    [SerializeField] private GameObject inGameCanvas;
    [SerializeField] private TextMeshProUGUI timeLeft;
    [SerializeField] private MazePlayer mazePlayer;

    private GameObject mazeManager;

    private void Start()
    {
        mazeManager = GameObject.Find("PF_Minigame_Maze");
        Score.Value = 0;
    }

    private void Update()
    {
        if ( inGameCanvas.activeInHierarchy )
        {
            secondsToPlay -= Time.deltaTime;
            timeLeft.text = secondsToPlay.ToString("F0") + " Seconds";
            if (secondsToPlay < 1)
            {
                ExitLevel(Score.Value, false);
            }
        }
    }

    public void ExitLevel(float _score, bool _end)
    {
        PlayTime = secondsToPlay;

        _score -= _end ? 0 : 33;
        _score = Mathf.Max(0, _score + 1);

        LevelManager.Singleton.SaveMinigameOutcome(_score, 100, 33);
        Conclusion.StartEndingDialogue();
    }
}