using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GatesScript : MonoBehaviour
{
    private MazePlayerTEMP player;
    private GameObject thisGate;
    public FloatReference Score;

    private MazeGameManager mazeManager;
    private void Start()
    {
        thisGate = this.gameObject;
        player = GameObject.FindObjectOfType<MazePlayerTEMP>();
        mazeManager = GameObject.FindObjectOfType<MazeGameManager>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            if (player.hasFirstKey)
            {
                player.DestroyGate(thisGate);
                player.hasFirstKey = false;
            }
            else if (player.hasSecondKey)
            {
                player.DestroyGate(thisGate);
                player.hasSecondKey = false;
            }
            if(gameObject.tag == "Final_Gate")
            {      
                if ((player.hasFirstKey && player.hasSecondKey) == false)
                {
                    mazeManager.ExitLevel(Score.Value, true);
                }
            }
        }
    }
}
