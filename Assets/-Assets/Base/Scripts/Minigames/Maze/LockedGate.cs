using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockedGate : MonoBehaviour
{
    private MazePlayer player;
    private GameObject thisGate;

    private void Start()
    {
        thisGate = this.gameObject;
        player = GameObject.FindObjectOfType<MazePlayer>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            if (player.hasFirstKey)
            {
                player.DestroyGate(thisGate);
                player.hasFirstKey = false;
            }
            else if (player.hasSecondKey)
            {
                player.DestroyGate(thisGate);
                player.hasSecondKey = false;
            }
        }
    }
}
