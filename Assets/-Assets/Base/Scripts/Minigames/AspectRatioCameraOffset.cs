using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AspectRatioCameraOffset : MonoBehaviour
{
    [SerializeField]
    private float cameraLimits;
    [SerializeField]
    private float cameraWidth;

    [SerializeField]
    private float side;

    private float movement;
    private Vector3 offset;

    void Start() 
    {
        Camera camera = Camera.main;
        cameraWidth = camera.orthographicSize * camera.aspect;
        movement = cameraWidth - cameraLimits;

        offset = transform.position;
        transform.position = TargetPosition();
    }

    private Vector3 TargetPosition()
    {
        return offset + new Vector3( side * -movement,0,0);
    }

    void Update()
    {
        Camera camera = Camera.main;
        cameraWidth = camera.orthographicSize * camera.aspect;
        movement = cameraWidth - cameraLimits;        
        transform.position = TargetPosition();
    }
}
