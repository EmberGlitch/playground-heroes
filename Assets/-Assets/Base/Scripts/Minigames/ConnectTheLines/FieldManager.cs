using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldManager : MonoBehaviour
{
    public List<GameObject> Pipes = new List<GameObject>();
    public List<int> Mask = new List<int>();
    public List<Tube> Tubes = new List<Tube>();

    public GameObject Tile;
    public int FieldSizeX;
    public int FieldSizeY;

    public float Offset;

    // Start is called before the first frame update
    void Start()
    {
        SetPipePositions();
    }

    private void SetPipePositions()
    {
        for (int y = 0; y < FieldSizeX; y++)
        {
            for (int x = 0; x < FieldSizeY; x++)
            {
                GameObject gameObject = Instantiate(Pipes[0], new Vector2(x * Offset, y * -Offset), Quaternion.identity);

                Tube _temp = gameObject.GetComponent<Tube>();
                Tubes.Add(_temp);

                for (int i = 0; i < _temp.PipeMask.Length; i++)
                {
                    Mask.Add(_temp.PipeMask[i]);
                }
            }
        }
    }

    public void UpdateFieldMask()
    {
        Mask.Clear();

        for (int i = 0; i < Tubes.Count; i++)
        {
            for (int j = 0; j < Tubes[i].PipeMask.Length; j++)
            {
                Mask.Add(Tubes[i].PipeMask[j]);
            } 
        }
    }
}
