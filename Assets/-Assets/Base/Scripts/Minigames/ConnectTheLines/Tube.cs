using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum TubeKind
{
    IPiece,
    LPiece,
    TPiece,
    XPiece
}

public class Tube : MonoBehaviour
{
    public FieldManager fieldManager;
    public TubeKind Type;

    public int[] PipeMask = new int[]
    {
        0,0,0,
        1,1,1,
        0,0,0
    };

    private int[] Field = new int[3 * 3];

    private void Start()
    {
        fieldManager = FindObjectOfType<FieldManager>();

        //fieldManager.UpdateFieldMask(PipeMask, transform.position);
    }

    private void OnMouseDown()
    {
        gameObject.transform.rotation *= Quaternion.Euler(new Vector3(0,0,90));
        Rotate();
    }

    private void Rotate()
    {
        for (int x = 0; x < 3; x++)
        {
            for (int y = 0; y < 3; y++)
            {
                if (x == 1 && y == 1)
                {
                    Field[x + (y * 3)] = PipeMask[x + (y * 3)];
                    continue;
                }

                //indices -> direction from center
                Vector3 _direction = new Vector3(x - 1, y - 1, 0);

                //rotate direction
                Vector3 _destination = Quaternion.Euler(0, 0, -90) * _direction;

                //rotate direction -> indices (destination)
                int newX = Mathf.RoundToInt(_destination.x + 1);
                int newY = Mathf.RoundToInt(_destination.y + 1);

                //place data from old position into new
                Field[newX + (newY * 3)] = PipeMask[x + (y * 3)];
            }
        }

        UpdateMask();
    }

    private void UpdateMask()
    {
        for (int i = 0; i < 8; i++)
        {
            PipeMask[i] = Field[i];
        }

        fieldManager.UpdateFieldMask();
    }
}
