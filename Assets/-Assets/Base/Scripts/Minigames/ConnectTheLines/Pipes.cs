using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TypeOfPipe
{
    Straight,
    Corner,
    tSplit,
    nonPlayable
}

public class Pipes : MonoBehaviour
{
    public Vector3 rotation;
    public TypeOfPipe pipeType;
    public bool isConnected;
    public bool[,] pipeHolder { get; private set; }

    [SerializeField] private Sprite pipe, pipeGray, pipeOutline, pipeFilled;
    [SerializeField] private bool isStartPipe;
    [SerializeField] private AudioSource clickSound, rotateSound;

    private SpriteRenderer spriteRenderer;
    private Sprite currentSprite;
    private ConnectLinesManager gameManager;
    private int gridPosX;
    private int gridPosY;
    private bool oldIsConnected;

    private bool[,] straight = new bool[,]
    {
            { false, true, false },
            { false, true, false },
            { false, true, false }
    };

    private bool[,] corner = new bool[,]
    {
            { false, true, false },
            {false, true, true},
            {false, false, false}
    };

    private bool[,] tSplit = new bool[,]
    {
            {false, true, false},
            {true, true, true},
            {false, false, false}
    };

    private bool[,] start = new bool[,]
    {
            {false, true, false},
            {false, true, false},
            {false, true, false}
    };

    private bool[,] end = new bool[,]
    {
            {false, false, false},
            {true, true, true},
            {false, false, false}
    };

    private void Start()
    {
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        gameManager = FindObjectOfType<ConnectLinesManager>();

        GivePipesStats();

        oldIsConnected = isConnected;
    }

    private void OnMouseDown()
    {
        if (pipeType != TypeOfPipe.nonPlayable)
        {
            transform.rotation = Quaternion.Euler(new Vector3(0, 0, transform.rotation.eulerAngles.z - 90));

            rotation = transform.rotation.eulerAngles;
        }
        //send event to start a check to the manager.
    }

    private void OnMouseEnter()
    {
        if (pipeType != TypeOfPipe.nonPlayable)
        {
            if (!gameManager.hasWon)
            {
                currentSprite = spriteRenderer.sprite;
                spriteRenderer.sprite = pipeOutline;
            }
        }
    }

    private void OnMouseExit()
    {
        if (pipeType != TypeOfPipe.nonPlayable)
        {
            if (!gameManager.hasWon)
            {
                spriteRenderer.sprite = currentSprite;
            }
        }
    }

    public bool CheckStateChange()
    {
        if (!oldIsConnected && isConnected)
        {
            oldIsConnected = isConnected;
            return true;
        }
        else if (oldIsConnected && !isConnected)
        {
            oldIsConnected = isConnected;
            return true;
        }
        else
        {
            return false;
        }
    }

    public void PlayWaterAnimation()
    {
        spriteRenderer.sprite = pipeFilled;

        //Play animation to do this
    }

    public void PlayPipeSound()
    {
        if (CheckStateChange())
        {
            if (isConnected)
            {
                clickSound.Play();
            }
            else
            {
                rotateSound.Play();
            }
        }
    }

    private void GivePipesStats()
    {
        if (pipeType != TypeOfPipe.tSplit)
            pipeHolder = tSplit;
        else if (pipeType != TypeOfPipe.Corner)
            pipeHolder = corner;
        else if (pipeType != TypeOfPipe.Straight)
            pipeHolder = straight;
        else if (pipeType != TypeOfPipe.nonPlayable)
            if (isStartPipe)
                pipeHolder = start;
            else
                pipeHolder = end;

        if (pipeType != TypeOfPipe.nonPlayable)
        {
            int random = Random.Range(0, 3);
            transform.rotation = Quaternion.Euler(new Vector3(0, 0, 90 * random));

            rotation = transform.rotation.eulerAngles;

            for (int x = 0; x < 3; x++)
            {
                for (int y = 0; y < 3; y++)
                {
                    Vector2 tempVector = new Vector2(x, y) - Vector2.one;
                    tempVector *= rotation;
                    tempVector += Vector2.one;
                }
            }

            gameManager.CheckConnectedSides(gridPosY, gridPosX, pipeHolder);
            CheckStateChange();
            if (isConnected)

                PlayPipeSound();
        }
    }

    private void GetPosOnGrid()
    {
        int y = 0;
        int x = 0;

        for (int i = 0; i < gameManager.pipeOnGrid.Length; i++)
        {
            if (gameManager.pipeOnGrid[i] == gameObject)
            {
                gridPosY = y;
                gridPosX = x;
                break;
            }

            if (x == 3)
            {
                x = 0;
                y++;
            }
            else
            {
                x++;
            }
        }
    }
}