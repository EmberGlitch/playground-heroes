using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConnectLinesManager : MonoBehaviour
{
    public bool[,] grid;

    private int gridX = 4, gridY = 4;

    public bool hasWon;
    public GameObject[] pipeOnGrid;

    [SerializeField] private GameObject gridHolder;

    private Pipes pipe;
    private int amountConnectedPipes;

    private void Start()
    {
        pipeOnGrid = GameObject.FindGameObjectsWithTag("Pipe");

        grid = new bool[gridX * 9, gridY * 9];
    }

    public void UpdateGrid(int gridPosY, int gridPosX, bool[,] pipe)
    {
    }

    //Index van buizen grid(alle buizen), buis rotation
    public int CheckConnectedSides(int gridPosY, int gridPosX, bool[,] givenPipe)
    {
        int answer = 0;

        for (int x = 0; x < 3; x++)
        {
            for (int y = 0; y < 3; y++)
            {
                Vector2 tempVector = new Vector2(x, y) - Vector2.one;
                bool amount = grid[gridPosX + x + (int)tempVector.x, gridPosY + y + (int)tempVector.y];
                bool sides = givenPipe[x, y];
                grid[gridPosX + x, gridPosY + y] = givenPipe[x, y];

                if (sides)
                {
                    answer += (amount == true ? 1 : -1);
                }
            }
        }

        return answer;
    }

    //Index alle buizen, referentie naar alle buizen, hasWon, amountConnectedPipes
    private void CheckForWin()
    {
        //kijkt naar alle verchillende wegen dat ie kan checken (boven is buis index -4, links is buis index  -1, rechts is buis index  +1, onder is buis index  +4)

        //Als de pipe connected is: amountConnectedPipes++;

        if (hasWon == true)
        {
            //voor alle buizen die correct waren
            pipe.PlayWaterAnimation();
        }
    }

    //amountConnectedPipes, yourScore
    private void CheckScore()
    {
    }
}