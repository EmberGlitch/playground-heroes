using System.Linq;
using UnityEngine;

public class MinigameManager : MonoBehaviour
{
    public FloatReference Score;
    public ConclusionManager Conclusion;

    public int HandlePulls;

    [SerializeField] private ConnectManager manager;
    [SerializeField] private LevelFader levelFade;

    private Animator animator;

    private void Start()
    {
        animator = gameObject.GetComponent<Animator>();
        Score.Value = 0;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.U) && Debug.isDebugBuild)
        {
            LevelManager.Singleton.ReturnToSubLevel();
        }
    }

    private void OnMouseDown()
    {
        if ( manager.endConnected )
        {
            HandlePulls++;
            animator.SetBool("isCorrect", true);
        }
        else
        {
            HandlePulls++;
            animator.SetBool("isWrong", true);
        }
    }

    public void WinTheGame()
    {
        Score.Value = manager.GetPipes().Sum( e => e.Connected ? 1 : 0 );

        Score.Value = Score.Value / 16;
        Score.Value = Score.Value * 100;

        // incase of timeout
        Score.Value = Score.Value * (manager.endConnected? 1 : 0.5f);

        float goodThreshold = Conclusion.scoreTableElements.Last().minimum;
        float poorThreshold = Conclusion.scoreTableElements[1].minimum;

        LevelManager.Singleton?.SaveMinigameOutcome(Score.Value, (int)goodThreshold, (int)poorThreshold);
        Conclusion.StartEndingDialogue();
    }

    private void ResetAnimation()
    {
        animator.SetBool("isWrong", false);
    }
}