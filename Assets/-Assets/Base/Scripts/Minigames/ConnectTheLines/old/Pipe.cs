using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum PipeType
{
    straight,
    bended,
    tsplit,
    nonPlayable
}

public class Pipe : MonoBehaviour
{
    public IntReference Clicks;

    public BoolReference isPlaying;

    public SpriteRenderer spriteRenderer;

    //Header
    public bool isConnected;

    public bool connected;
    public bool isFinalPipe;
    public PipeType pipeType;
    public int thisPipeIndex { get; private set; }
    public int[,,] thisPipe { get; private set; }

    private List<int[,,]> differntPipes;

    [SerializeField] private Sprite currectSprite;
    [SerializeField] private Sprite baseSprite;
    [SerializeField] private Sprite connectedSprite;
    [SerializeField] private Sprite outlineSprite;

    public Collider2D pipeThis, pipeUp, pipeLeft, pipeRight, pipeDown;

    private Grid grid;
    private float[] rotations = { 0, 90, 180, 270 };

    //Masks of the different type of pipes that are used in the minigame
    private int[,,] straightPipe = new int[,,]
    {
        {
            {0, 0, 0},
            {1, 1, 1},
            {0, 0, 0}
        },
        {
            {0, 1, 0},
            {0, 1, 0},
            {0, 1, 0}
        },
        {
            {0, 0, 0},
            {1, 1, 1},
            {0, 0, 0}
        },
        {
            {0, 1, 0},
            {0, 1, 0},
            {0, 1, 0}
        }
    };

    private int[,,] bendedPipe = new int[,,]
   {
        {
            {0, 0, 0},
            {0, 1, 1},
            {0, 1, 0}
        },
        {
            {0, 1, 0},
            {0, 1, 1},
            {0, 0, 0}
        },
        {
            {0, 1, 0},
            {1, 1, 0},
            {0, 0, 0}
        },
        {
            {0, 0, 0},
            {1, 1, 0},
            {0, 1, 0}
        }
   };

    private int[,,] tSplitPipe = new int[,,]
   {
        {
            {0, 1, 0},
            {1, 1, 1},
            {0, 0, 0}
        },
        {
            {0, 1, 0},
            {1, 1, 0},
            {0, 1, 0}
        },
        {
            {0, 0, 0},
            {1, 1, 1},
            {0, 1, 0}
        },
        {
            {0, 1, 0},
            {0, 1, 1},
            {0, 1, 0}
        }
   };

    private void Awake()
    {
        //Makes a list of the different types of pipes there are and their index
        differntPipes = new List<int[,,]>
        {
            straightPipe,
            bendedPipe,
            tSplitPipe
        };
    }

    private void Start()
    {
        if (pipeType == PipeType.nonPlayable)
        {
        }
        else
        {
            grid = FindObjectOfType<Grid>();
            //spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
            int rand = Random.Range(0, rotations.Length);
            transform.eulerAngles = new Vector3(0, 0, rotations[rand]);
            thisPipe = RandomPipeRotation();
            pipeThis = gameObject.GetComponent<BoxCollider2D>();

            //Sets the current index of the pipe to their corresponding rotation
            if (transform.rotation.eulerAngles.z == 0)
            {
                thisPipeIndex = 0;
            }
            else if (transform.rotation.eulerAngles.z == 90)
            {
                thisPipeIndex = 1;
            }
            else if (transform.rotation.eulerAngles.z == 180)
            {
                thisPipeIndex = 2;
            }
            else if (transform.rotation.eulerAngles.z == 270)
            {
                thisPipeIndex = 3;
            }
            //int doubleNumb = (int)transform.rotation.eulerAngles.z / 90;
            //thisPipeIndex = Mathf.Round()

            CheckTexture();
        }
    }

    public void CheckTexture()
    {
        if (pipeType == PipeType.nonPlayable)
        {
        }
        else
        {
            connected = false;

            if (pipeType == PipeType.bended)
            {
                if (thisPipeIndex == 0)
                {
                    CheckSelectedLocation("down");
                    CheckSelectedLocation("right");
                }
                else if (thisPipeIndex == 1)
                {
                    CheckSelectedLocation("up");
                    CheckSelectedLocation("right");
                }
                else if (thisPipeIndex == 2)
                {
                    CheckSelectedLocation("up");
                    CheckSelectedLocation("left");
                }
                else if (thisPipeIndex == 3)
                {
                    CheckSelectedLocation("down");
                    CheckSelectedLocation("left");
                }
            }
            else if (pipeType == PipeType.straight)
            {
                if (thisPipeIndex == 0)
                {
                    CheckSelectedLocation("left");
                    CheckSelectedLocation("right");
                }
                else if (thisPipeIndex == 1)
                {
                    CheckSelectedLocation("up");
                    CheckSelectedLocation("down");
                }
                else if (thisPipeIndex == 2)
                {
                    CheckSelectedLocation("left");
                    CheckSelectedLocation("right");
                }
                else if (thisPipeIndex == 3)
                {
                    CheckSelectedLocation("up");
                    CheckSelectedLocation("down");
                }
            }
            if (pipeType == PipeType.tsplit)
            {
                if (thisPipeIndex == 0)
                {
                    CheckSelectedLocation("up");
                    CheckSelectedLocation("left");
                    CheckSelectedLocation("right");
                }
                else if (thisPipeIndex == 1)
                {
                    CheckSelectedLocation("up");
                    CheckSelectedLocation("left");
                    CheckSelectedLocation("down");
                }
                else if (thisPipeIndex == 2)
                {
                    CheckSelectedLocation("left");
                    CheckSelectedLocation("down");
                    CheckSelectedLocation("right");
                }
                else if (thisPipeIndex == 3)
                {
                    CheckSelectedLocation("up");
                    CheckSelectedLocation("right");
                    CheckSelectedLocation("down");
                }
            }

            if (!connected)
            {
                CheckSelectedLocation("none");
            }

            currectSprite = spriteRenderer.sprite;
        }
    }

    private void OnMouseDown()
    {
        if( isPlaying.Value == false )
            return;

        if (pipeType == PipeType.nonPlayable)
        {
        }
        else
        {
            Clicks.Value++;

            //Rotates the pipe 90 degree
            if (transform.rotation.eulerAngles.z == 0)
            {
                transform.rotation = Quaternion.Euler(new Vector3(0, 0, 90));
                thisPipeIndex = 1;
            }
            else if (transform.rotation.eulerAngles.z == 90)
            {
                transform.rotation = Quaternion.Euler(new Vector3(0, 0, 180));
                thisPipeIndex = 2;
            }
            else if (transform.rotation.eulerAngles.z == 180)
            {
                transform.rotation = Quaternion.Euler(new Vector3(0, 0, 270));
                thisPipeIndex = 3;
            }
            else if (transform.rotation.eulerAngles.z == 270)
            {
                transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
                thisPipeIndex = 0;
            }

            CheckTexture();
        }
    }

    //Updates the current index of the pipe
    private int ChangeBlockIndex(int pipeIndex)
    {
        return pipeIndex % thisPipe.GetLength(0);
    }

    //Gives a random rotation to the pipe
    private int[,,] RandomPipeRotation()
    {
        return differntPipes[(int)pipeType];
    }

    private void OnMouseOver()
    {
        if( isPlaying.Value == false )
            return;

        if (pipeType == PipeType.nonPlayable)
        {
        }
        else
        {
            SpriteRenderer spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
            spriteRenderer.sprite = outlineSprite;
        }
    }

    private void OnMouseExit()
    {
        if( isPlaying.Value == false )
            return;
            
        if (pipeType == PipeType.nonPlayable)
        {
        }
        else
        {
            SpriteRenderer spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
            spriteRenderer.sprite = currectSprite;

            for (int i = 0; i < grid.pipes.Length; i++)
            {
                Pipe tempPipe = grid.pipes[i].GetComponent<Pipe>();

                if (tempPipe.pipeType == PipeType.nonPlayable)
                {
                }
                else
                {
                    tempPipe.CheckTexture();
                }
            }
        }
    }

    public void CheckSelectedLocation(string location)
    {
        if (location == "up")
        {
            if (!connected && pipeUp)
            {
                Pipe temp = pipeUp.GetComponentInParent<Pipe>();
                if (temp.pipeType == PipeType.bended)
                {
                    if (temp.thisPipeIndex == 0 || temp.thisPipeIndex == 3)
                    {
                        if (temp.isConnected)
                        {
                            spriteRenderer.sprite = connectedSprite;
                            isConnected = true;
                            connected = true;
                        }
                    }
                }
                else if (temp.pipeType == PipeType.straight)
                {
                    if (temp.thisPipeIndex == 1 || temp.thisPipeIndex == 3)
                    {
                        if (temp.isConnected)
                        {
                            spriteRenderer.sprite = connectedSprite;
                            isConnected = true;
                            connected = true;
                        }
                    }
                }
                else if (temp.pipeType == PipeType.tsplit)
                {
                    if (temp.thisPipeIndex == 1 || temp.thisPipeIndex == 2 || temp.thisPipeIndex == 3)
                    {
                        if (temp.isConnected)
                        {
                            spriteRenderer.sprite = connectedSprite;
                            isConnected = true;
                            connected = true;
                        }
                    }
                }
                else if (temp.pipeType == PipeType.nonPlayable)
                {
                    if (temp.isConnected)
                    {
                        spriteRenderer.sprite = connectedSprite;
                        isConnected = true;
                        connected = true;
                    }
                }
                else
                {
                    spriteRenderer.sprite = baseSprite;
                    isConnected = false;
                }
            }
        }

        if (location == "left")
        {
            if (!connected && pipeLeft)
            {
                Pipe temp = pipeLeft.GetComponentInParent<Pipe>();
                if (temp.pipeType == PipeType.bended)
                {
                    if (temp.thisPipeIndex == 0 || temp.thisPipeIndex == 1)
                    {
                        if (temp.isConnected)
                        {
                            spriteRenderer.sprite = connectedSprite;
                            isConnected = true;
                            connected = true;
                        }
                    }
                }
                else if (temp.pipeType == PipeType.straight)
                {
                    if (temp.thisPipeIndex == 0 || temp.thisPipeIndex == 2)
                    {
                        if (temp.isConnected)
                        {
                            spriteRenderer.sprite = connectedSprite;
                            isConnected = true;
                            connected = true;
                        }
                    }
                }
                else if (temp.pipeType == PipeType.tsplit)
                {
                    if (temp.thisPipeIndex == 0 || temp.thisPipeIndex == 2 || temp.thisPipeIndex == 3)
                    {
                        if (temp.isConnected)
                        {
                            spriteRenderer.sprite = connectedSprite;
                            isConnected = true;
                            connected = true;
                        }
                    }
                }
            }
        }

        if (location == "right")
        {
            if (!connected && pipeRight)
            {
                Pipe temp = pipeRight.GetComponentInParent<Pipe>();
                if (temp.pipeType == PipeType.bended)
                {
                    if (temp.thisPipeIndex == 2 || temp.thisPipeIndex == 3)
                    {
                        if (temp.isConnected)
                        {
                            spriteRenderer.sprite = connectedSprite;
                            isConnected = true;
                            connected = true;
                        }
                    }
                }
                else if (temp.pipeType == PipeType.straight)
                {
                    if (temp.thisPipeIndex == 0 || temp.thisPipeIndex == 2)
                    {
                        if (temp.isConnected)
                        {
                            spriteRenderer.sprite = connectedSprite;
                            isConnected = true;
                            connected = true;
                        }
                    }
                }
                else if (temp.pipeType == PipeType.tsplit)
                {
                    if (temp.thisPipeIndex == 0 || temp.thisPipeIndex == 1 || temp.thisPipeIndex == 2)
                    {
                        if (temp.isConnected)
                        {
                            spriteRenderer.sprite = connectedSprite;
                            isConnected = true;
                            connected = true;
                        }
                    }
                }
                else if (temp.pipeType == PipeType.nonPlayable)
                {
                    if (temp.isConnected)
                    {
                        spriteRenderer.sprite = connectedSprite;
                        isConnected = true;
                        connected = true;
                    }
                }
            }
        }

        if (location == "down")
        {
            if (!connected && pipeDown)
            {
                Pipe temp = pipeDown.GetComponentInParent<Pipe>();
                if (temp.pipeType == PipeType.bended)
                {
                    if (temp.thisPipeIndex == 1 || temp.thisPipeIndex == 2)
                    {
                        if (temp.isConnected)
                        {
                            spriteRenderer.sprite = connectedSprite;
                            isConnected = true;
                            connected = true;
                        }
                    }
                }
                else if (temp.pipeType == PipeType.straight)
                {
                    if (temp.thisPipeIndex == 1 || temp.thisPipeIndex == 3)
                    {
                        if (temp.isConnected)
                        {
                            spriteRenderer.sprite = connectedSprite;
                            isConnected = true;
                            connected = true;
                        }
                    }
                }
                else if (temp.pipeType == PipeType.tsplit)
                {
                    if (temp.thisPipeIndex == 0 || temp.thisPipeIndex == 1 || temp.thisPipeIndex == 3)
                    {
                        if (temp.isConnected)
                        {
                            spriteRenderer.sprite = connectedSprite;
                            isConnected = true;
                            connected = true;
                        }
                    }
                }
            }
        }

        if (location == "none")
        {
            spriteRenderer.sprite = baseSprite;
            isConnected = false;

            if (pipeUp)
            {
                Pipe temp = pipeUp.GetComponentInParent<Pipe>();
                CheckAll(temp);
            }

            if (pipeLeft)
            {
                Pipe temp = pipeLeft.GetComponentInParent<Pipe>();
                CheckAll(temp);
            }

            if (pipeDown)
            {
                Pipe temp = pipeDown.GetComponentInParent<Pipe>();
                CheckAll(temp);
            }

            if (pipeRight)
            {
                Pipe temp = pipeRight.GetComponentInParent<Pipe>();
                CheckAll(temp);
            }
        }
    }

    public void CheckAll(Pipe temp)
    {
        temp.connected = false;

        if (temp.pipeType == PipeType.bended)
        {
            if (temp.thisPipeIndex == 0)
            {
                temp.CheckSelectedLocation("down");
                temp.CheckSelectedLocation("right");
            }
            else if (temp.thisPipeIndex == 1)
            {
                temp.CheckSelectedLocation("up");
                temp.CheckSelectedLocation("right");
            }
            else if (temp.thisPipeIndex == 2)
            {
                temp.CheckSelectedLocation("up");
                temp.CheckSelectedLocation("left");
            }
            else if (temp.thisPipeIndex == 3)
            {
                temp.CheckSelectedLocation("down");
                temp.CheckSelectedLocation("left");
            }
        }
        else if (temp.pipeType == PipeType.straight)
        {
            if (temp.thisPipeIndex == 0)
            {
                temp.CheckSelectedLocation("left");
                temp.CheckSelectedLocation("right");
            }
            else if (temp.thisPipeIndex == 1)
            {
                temp.CheckSelectedLocation("up");
                temp.CheckSelectedLocation("down");
            }
            else if (temp.thisPipeIndex == 2)
            {
                temp.CheckSelectedLocation("left");
                temp.CheckSelectedLocation("right");
            }
            else if (temp.thisPipeIndex == 3)
            {
                temp.CheckSelectedLocation("up");
                temp.CheckSelectedLocation("down");
            }
        }
        if (temp.pipeType == PipeType.tsplit)
        {
            if (temp.thisPipeIndex == 0)
            {
                temp.CheckSelectedLocation("up");
                temp.CheckSelectedLocation("left");
                temp.CheckSelectedLocation("right");
            }
            else if (temp.thisPipeIndex == 1)
            {
                temp.CheckSelectedLocation("up");
                temp.CheckSelectedLocation("left");
                temp.CheckSelectedLocation("down");
            }
            else if (temp.thisPipeIndex == 2)
            {
                temp.CheckSelectedLocation("left");
                temp.CheckSelectedLocation("down");
                temp.CheckSelectedLocation("right");
            }
            else if (temp.thisPipeIndex == 3)
            {
                temp.CheckSelectedLocation("up");
                temp.CheckSelectedLocation("right");
                temp.CheckSelectedLocation("down");
            }
        }
    }
}