using System;
using System.Collections;
using System.Collections.Generic;
using DialogSystem;
using UnityEngine;
using UnityEngine.UI;

public class DialogElement : MonoBehaviour
{
    [SerializeField]
    private Image person;
    [SerializeField]
    private TMPro.TMP_Text text;

    public void Setup(DialogNode current, Blackboard blackboard)
    {
        if( current.Character )
            person.sprite = current.Character.sprite;

        person.gameObject.SetActive(current.Character != null);

        string[] texts = current.Data((int)LocaleSetting.localeInfo.locale);
        text.text = Utils.ReplaceTags( 
            texts[0], 
            DialogBox.regex, 
            (variable) => blackboard[variable].value );
    }
}
