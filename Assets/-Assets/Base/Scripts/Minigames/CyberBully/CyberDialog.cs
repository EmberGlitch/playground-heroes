using System.Collections;
using DialogSystem;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Minigame.Cyber
{
    public class CyberDialog : MonoBehaviour, DialogInputReceiver
    {
        [SerializeField]
        private Dialog dialog;
        [SerializeField]
        private Blackboard blackboard;

        [SerializeField]
        private DialogWalker walker;
        
        [SerializeField, Range(0, 10)]
        private float dialogInterval;

        [SerializeField]
        private ChoiceButton[] buttons;

        [SerializeField]
        private TMPro.TMP_Text prompt;

        [SerializeField]
        private GameObject container;
        [SerializeField]
        private DialogElement element;

        
        public UnityEvent OnDialogEnded;
        public UnityEvent<DialogEventContext> OnDialogNodeChanged;

        private int input;

        private ScrollRect scrollRect;

        void Start()
        {
            scrollRect = GetComponentInChildren<ScrollRect>();

            walker = new DialogWalker( dialog );

            prompt.enabled = false;
            for (int i = 0; i < buttons.Length; i++)
            {
                buttons[i].gameObject.SetActive(false);
            }

            StartDialog();
        }

        [ContextMenu("Start dialog")]
        public void StartDialog()
        {
            StartCoroutine( Walk() );
        }

        private IEnumerator Walk()
        {
            while( walker.Current != null )
            {
                yield return ToNext(walker.Current);
            }

            OnDialogEnded.Invoke();
        }

        private IEnumerator ToNext(DialogNode node) => node.Type switch
        {
            "text" => Text(),
            "choice" => Choice(),
            _ => null
        };

        private IEnumerator Choice()
        {
            input = -1;            
            
            string[] lines = walker.Current.Data( (int)LocaleSetting.localeInfo.locale );
            
            prompt.enabled = lines[0].Length > 0;
            prompt.text = Utils.ReplaceTags( 
                    lines[0], 
                    DialogBox.regex, 
                    (variable) => blackboard[variable].value );

            for (int i = 0; i < buttons.Length; i++)
            {
                int option = new int( );
                option = i;

                buttons[i].gameObject.SetActive( i + 1 < lines.Length );
                string line = Utils.ReplaceTags( 
                    lines[i + 1], 
                    DialogBox.regex, 
                    (variable) => blackboard[variable].value );
                
                if( i + 1 < lines.Length )
                    buttons[i].Setup( this, lines[i + 1], option );
            }

            // yield for input          
            yield return new WaitWhile( () => input == -1 );

            DialogNode node = walker.Current;
            // proceed to next
            if( walker.Next( input ) )
            {
                OnDialogNodeChanged.Invoke( new DialogEventContext(){
                    previousNode = node,
                    newNode = walker.Current,
                    input = input
                });
            }

            prompt.enabled = false;
            for (int i = 0; i < buttons.Length; i++)
            {
                buttons[i].gameObject.SetActive(false);
            }
        }

        private IEnumerator Text()
        {
            DialogElement newElement = Instantiate( element, container.transform );
            newElement.Setup( walker.Current, blackboard );
            yield return null;
            scrollRect.CalculateLayoutInputVertical();
            scrollRect.verticalNormalizedPosition = 0;    

            float duration = walker.Current.Data((int)LocaleSetting.localeInfo.locale)[0].Length / 110.0f;
            yield return new WaitForSeconds( duration * dialogInterval );
            walker.Next( true );            
        }

        public void ChooseOption(int option)
        {
            input = option;
        }
    }    
}
