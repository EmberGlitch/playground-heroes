using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class IntroductionManager : MonoBehaviour
{
    public TutorialData Data;

    public UnityEvent DialogueEvent;
    public UnityEvent StartEvent;

    public GameObject OptionalSkip;
    public GameObject IntroObject;

    private void Awake()
    {
        StartIntroDialogue();
    }

    private void StartIntroDialogue()
    {
        OptionalSkip.SetActive(Data.ShownIntro);
        IntroObject.SetActive(true);

        DialogueEvent.Invoke();
    }

    public void StartMinigame()
    {
        Data.ShownIntro = true;
        OptionalSkip.SetActive(false);
        IntroObject.SetActive(false);
        StartEvent.Invoke();
    }
}