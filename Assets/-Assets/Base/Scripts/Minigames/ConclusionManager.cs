using UnityEngine;
using UnityEngine.Events;
using Fungus;
using DialogSystem;
using System.Collections.Generic;

[System.Serializable]
public struct ScoreTableElement
{
    public float minimum;
    public Dialog dialog;
}

public class ConclusionManager : MonoBehaviour
{
    private Dictionary<int,int> offsets = new Dictionary<int, int>{
        {0, -1},
        {1, 0},
        {2, 1},
    };

    public ScoreTableElement[] scoreTableElements;
    public Blackboard blackboard;

    public FloatReference Score;

    public UnityEvent StopGameLoop;

    public GameObject ConclusionDialogue;

    public DialogBox dialogBox;

    public void StartEndingDialogue()
    {         
        int outcome = 0;
        for (int i = 0; i < scoreTableElements.Length; i++)
        {   
            if( Score.Value > scoreTableElements[i].minimum )
                outcome = i;
        }
        outcome += offsets[LevelManager.Singleton.GetConsequence()];
        outcome = Mathf.Clamp(outcome, 0, 2 );
        LevelManager.Singleton.SaveMinigameOutcome( outcome );
        blackboard.Put( new BlackboardData() { name = "latestTotalScore", value = outcome.ToString() } );
        blackboard.Put( new BlackboardData() { name = "CandyCrushScore", value = Score.Value.ToString() } );

        StopGameLoop.Invoke();

        dialogBox.SetDialog( scoreTableElements[outcome].dialog );
        ConclusionDialogue.SetActive(true);
    }

    public void EndGame()
    {
        Score.Value = 0;
        LevelManager.Singleton.ReturnToSubLevel();
    }
}