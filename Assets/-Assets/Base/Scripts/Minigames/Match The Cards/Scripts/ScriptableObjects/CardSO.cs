using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Card", menuName = "MemoryAssets/CardSO")]
public class CardSO : ScriptableObject
{
    public Sprite[] m_CardsSO;
}
