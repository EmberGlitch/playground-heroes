using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Card : MonoBehaviour
{
    private MemoryManager mm;
    private float index = 10;
    public string CardName;
    public bool CardBool;

    public void SetMemoryManager(MemoryManager manager)
    {
        mm = manager;
    }

    public void ClickedOnCard()
    {
        mm.PickCard(this);
        StartCoroutine(TurnCard());
    }

    public void StartTurnBack()
    {
        StartCoroutine(TurnBack());
    }

    public void StartShake()
    {
        StartCoroutine(ShakeCard());
    }

    IEnumerator ShakeCard()
    {
        for (float i = 0; i <= 180; i += index)
        {
            transform.rotation = Quaternion.Euler(0, 0, i);
            if (i == 160)
            {
                index = -10f;
            }
            else if (i == -360)
            {
                i = 0;
                index = 0;
            }
            yield return new WaitForSeconds(0.025f);
        }
    }

    IEnumerator TurnCard()
    {
        GetComponent<Button>().enabled = false;
        for (float i = 0; i <= 90; i += 10f)
        {
            transform.rotation = Quaternion.Euler(0, i, 0);
            if (i == 90)
            {
                GetComponent<Image>().sprite = mm.m_CardSprite[int.Parse(gameObject.name)];
                CardName = GetComponent<Image>().sprite.name;

                for (int j = 90; j >= 0; j -= 10)
                {
                    transform.rotation = Quaternion.Euler(0, j, 0);
                    yield return new WaitForSeconds(0.025f);
                }
            }
            yield return new WaitForSeconds(0.025f);
        }
    }

    IEnumerator TurnBack()
    {
        for (float i = 0; i <= 90; i += 10f)
        {
            transform.rotation = Quaternion.Euler(0, i, 0);
            if (i == 90)
            {
                GetComponent<Button>().enabled = true;
                gameObject.GetComponent<Image>().sprite = mm.m_CardBack;

                for (int j = 90; j >= 0; j -= 10)
                {
                    transform.rotation = Quaternion.Euler(0, j, 0);
                    yield return new WaitForSeconds(0.025f);
                }
            }
            yield return new WaitForSeconds(0.025f);
        }
    }
}

