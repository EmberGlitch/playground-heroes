using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Animations;

public class MemoryManager : MonoBehaviour
{
    //Make sure the arrays are in the correct order
    public SpriteReference[] CardSprites;
    public FloatReference[] CardIndex;

    public ConclusionManager Conclusion;
    public FloatReference Score;
    public bool StartTimer;

    [Header("Match The Cards Field")]
    public GameObject m_ButtonPref;

    public Transform MemmoryField;

    public Sprite m_CardBack;

    public List<Sprite> m_CardSprite = new List<Sprite>();
    public List<Button> m_Cards = new List<Button>();

    [Header("Player Stats")]
    public float m_iTime;
    public float DataTime;

    public int m_TotalCards;
    private int m_iZetten, m_iSet;

    private bool First, Second, PickBool, EndCalled;

    private Card[] m_CardChosen = new Card[2];

    [SerializeField] private CardSO m_CSO;
    [SerializeField] private GameObject m_Outro;
    [SerializeField] private GameObject[] m_Bin;

    [Header("TXT")]
    [SerializeField] private TMP_Text m_TimeTXT;

    [SerializeField] private TMP_Text m_SetsTXT;
    [SerializeField] private TMP_Text m_ZettenTXT;
    [SerializeField] private TMP_Text m_OutroTXT;

    private void Start()
    {
        //Instatiating the "m_ButtonPref".
        for (int i = 0; i < m_TotalCards; i++)
        {
            GameObject button = Instantiate(m_ButtonPref);
            button.name = "" + i;   //This makes the name of the instantiated buttons a numbered name.
            button.transform.SetParent(MemmoryField, false);

            Card card = button.GetComponent<Card>();
            card.SetMemoryManager(this);

            m_Cards.Add(button.GetComponent<Button>());
            m_Cards[i].image.sprite = m_CardBack;
        }

        AddCards();
        RandomizeCards(m_CardSprite);

        Score.Value = 0;
    }

    private void Update()
    {
        DataTime += Time.deltaTime;

        //A timer for the game, everything thats within 60 seconds is functioning.
        if (m_iTime > 0 && StartTimer)
        {
            m_iTime -= Time.deltaTime;
            if (m_iTime <= 0)
            {
                m_TimeTXT.text = "0";
                for (int i = 0; i < m_Cards.Count; i++)
                {
                    m_Cards[i].enabled = false;
                    Outro();
                }
            }
            else
            {
                int index;
                index = Mathf.RoundToInt(m_iTime);
                m_TimeTXT.text = index.ToString();
                m_SetsTXT.text = m_iSet.ToString();
                m_ZettenTXT.text = m_iZetten.ToString();
            }
            if (m_iSet == m_TotalCards/2)
            {
                if (!EndCalled)
                {
                    End(); //If the 60 seconds have gone by the game stops, and you're redirected to the outro.
                    EndCalled = true;
                }
            }
        }
    }

    public void StartTime()
    {
        StartTimer = true;
    }

    private void AddCards()
    {
        int iSprite = 0;

        for (int i = 0; i < m_Cards.Count; i++)
        {
            if (iSprite == m_Cards.Count / 2)
            {
                iSprite = 0;
            }
            m_CardSprite.Add(m_CSO.m_CardsSO[iSprite]);
            iSprite++;
        }
    }

    private void Outro()
    {
        //Disabled Objects:
        m_TimeTXT.gameObject.SetActive(false);
        m_SetsTXT.gameObject.SetActive(false);
        m_ZettenTXT.gameObject.SetActive(false);

        for (int i = 0; i < m_Bin.Length; i++)
        {
            m_Bin[i].SetActive(false);
        }

        if (m_iSet >= 7 && m_iZetten <= 15)
        { m_OutroTXT.text = string.Format("Goed gespeeld! Je hebt {0} setjes kunnen maken in  {1} zetten. Dat betekent dat je een goede uitkomst hebt!", m_iSet, m_iZetten); }
        else if (m_iSet >= 4 && m_iSet <= 6 && m_iZetten >= 16)
        { m_OutroTXT.text = string.Format("Goed gespeeld! Je hebt {0} setjes kunnen maken in  {1} zetten. Dat betekent dat je een ok� uitkomst hebt!", m_iSet, m_iZetten); }
        else { m_OutroTXT.text = string.Format("Jammer! Je hebt {0} setjes kunnen maken in  {1} zetten. Dat betekent dat je een slecht uitkomst hebt!", m_iSet, m_iZetten); }
    }

    private void RandomizeCards(List<Sprite> sList)
    {
        for (int i = 0; i < sList.Count; i++)
        {
            Sprite holder = sList[i];
            int index = Random.Range(i, sList.Count);
            sList[i] = sList[index];
            sList[index] = holder;
        }
    }

    public void PickCard(Card card) //This function inhabits the card clicked on.
    {
        if (PickBool)
            return;

        PickBool = true;
        if (!First)
        {
            First = true;
            m_CardChosen[0] = card;
            PickBool = false;
            return;
        }
        else if (!Second)
        {
            Second = true;
            m_CardChosen[1] = card;

            StartCoroutine(Check());
            CardsEnable(false);
            return;
        }
    }

    public void CardsEnable(bool enable)
    {
        for (int i = 0; i < m_Cards.Count; i++)
        {
            m_Cards[i].enabled = enable;
        }
    }

    public void End()
    {
        if (LevelManager.Singleton != null)
        {
            LevelManager.Singleton.SaveMinigameOutcome(m_iSet, 6, 3);
        }
        Score.Value = m_iSet;
        Conclusion.StartEndingDialogue();
    }

    private IEnumerator Check()
    {
        yield return new WaitForSeconds(1);

        if (m_CardChosen[0].CardName == m_CardChosen[1].CardName)
        {
            m_CardChosen[0].GetComponent<Button>().interactable = false;
            m_CardChosen[1].GetComponent<Button>().interactable = false;

            //for (int i = 0; i < m_CardSprite.Count; i++)
            //{
            //    if (m_CardChosen[0].GetComponent<SpriteRenderer>().sprite.name == CardSprites[i].name)
            //    {
            //        //CardIndex[i].Value++;
            //    }
            //}

            m_iSet++;
            m_iZetten++;
            m_CardChosen[0].StartShake();
            m_CardChosen[1].StartShake();
            CardsEnable(true);
            PickBool = false;
            First = Second = false;
        }
        else
        {
            m_iZetten++;
            CardsEnable(true);
            for (int i = 0; i < m_CardChosen.Length; i++)
            {
                m_CardChosen[i].StartTurnBack();
            }
            PickBool = false;
            First = Second = false;
        }
    }
}