using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataTimer : MonoBehaviour
{
    public float Timer;

    void Update()
    {
        Timer += Time.deltaTime;
    }
}
