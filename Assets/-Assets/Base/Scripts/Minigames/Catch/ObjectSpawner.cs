using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawner : MonoBehaviour
{
    [Header("Analytics Data")]
    public bool MeasureReactionTime;
    public float ReactionTimer;
    public List<float> m_reactionTimes = new List<float>();
    public Vector3 ObjectPosition;

    [Header("Game Variables")]
    public List<GameObject> GameObjects;
    public Camera Camera;

    public PlayerMovement Player;

    public float DelayBeforeSpawningObjects;

    private float m_halfWidth;

    private void Start()
    {
        MeasureReactionTime = false;
        m_halfWidth = Camera.aspect * Camera.orthographicSize;

        StartCoroutine(Delay());
    }

    private void Update()
    {
        if (!Player.GetInput())
        {
            CancelInvoke("SpawnObject");
        }

        if (MeasureReactionTime)
        {
            ReactionTimer += Time.deltaTime;
        }
        else if (!MeasureReactionTime && ReactionTimer > 0)
        {
            m_reactionTimes.Add(ReactionTimer);
            ReactionTimer = 0;
        }
    }

    private void SpawnObject()
    {
        float _horizontalMin = -m_halfWidth + 2;
        float _horizontalMax = m_halfWidth - 2;

        int _randomizer = Random.Range(0, 6);
        float _positionX = Random.Range(_horizontalMin, _horizontalMax);

        Instantiate(GameObjects[_randomizer], new Vector3(_positionX, 7.5f, 0), Quaternion.identity);

        if (GameObjects[_randomizer].CompareTag("Catch_GoodObject"))
        {
            ObjectPosition.x = _positionX;
            MeasureReactionTime = true;
        }
    }

    private IEnumerator Delay()
    {
        yield return new WaitForSeconds(DelayBeforeSpawningObjects);
        //TODO: CHANGE THIS, FOR THE LOVE OF GOD
        InvokeRepeating("SpawnObject", 0.5f, 1.0f);
    }

    public void StopInvokes()
    {
        CancelInvoke();
    }
}