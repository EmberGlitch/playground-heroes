using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Timer : MonoBehaviour
{
    public PlayerMovement Player;

    public ConclusionManager conclusion;

    public TMP_Text TimerText;

    public FloatReference TimerRef;

    private bool m_timesUp, hasEnded;

    private void Start()
    {
        TimerRef.Value = TimerRef.MaxValue;

        m_timesUp = false;
    }

    private void Update()
    {
        if (!m_timesUp)
        {
            TimerRef.Value -= Time.deltaTime;
            float _minutes = Mathf.FloorToInt(TimerRef.Value / 60);
            float _seconds = Mathf.FloorToInt(TimerRef.Value % 60);

            TimerText.text = string.Format("{0:00}:{1:00}", _minutes, _seconds);

            if (TimerRef.Value <= 0 && !hasEnded)
            {
                TimerText.text = string.Format("{0:00}:{1:00}", 0, 0);
                Player.CancelInput();

                m_timesUp = true;

                ScoreSystem.Instance.CalculateScorePercentage(CatchGameManager.Singleton.CatchID);
                conclusion.StartEndingDialogue();
                hasEnded = true;
            }
        }
    }

    public void TimePenalty()
    {
        StartCoroutine(TextColorChange());
        TimerRef.Value -= 5;
    }

    private IEnumerator TextColorChange()
    {
        TimerText.color = Color.red;
        yield return new WaitForSeconds(0.5f);
        TimerText.color = new Color(40f / 255f, 40f / 255f, 40f / 255f);
    }
}