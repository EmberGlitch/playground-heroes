using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIEvents : MonoBehaviour
{
    public CatchGameManager CatchGameManager;

    public void CatchContinueEvent()
    {
        CatchGameManager.ExitCatchGame();
    }

    public void RetryEvent()
    {
        Scene _scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(_scene.name);
    }
}