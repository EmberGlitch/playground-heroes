using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Basket : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        ObjectType _type = collision.gameObject.GetComponentInParent<ObjectType>();
        ScoreSystem.Instance.UpdateScore(_type.Types);

        Destroy(collision.gameObject.transform.parent.gameObject);
    }
}