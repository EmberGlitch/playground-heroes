using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScoreSystem : MonoBehaviour
{
    [Header("Analytics Data")]
    public IntReference BallData;
    public IntReference ShoeData;
    public IntReference PoleData;
    public IntReference NetData;
    public IntReference BowlingBallData;
    public IntReference ScissorsData;

    public int TotalCaught;

    [Header("Game Variables")]
    public FloatReference Score;

    public TMP_Text SchoenText;
    public TMP_Text BalText;
    public TMP_Text PaalText;
    public TMP_Text NetstukText;

    public Timer Timer;
    public static ScoreSystem Instance;
    
    public Dictionary<ObjectType.Type, int> LevelOneObjects = new Dictionary<ObjectType.Type, int>();

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
            return;
        }
        Instance = this;
    }

    private void Start()
    {
        LevelOneObjects.Add(ObjectType.Type.Bowlingbal, 0);
        LevelOneObjects.Add(ObjectType.Type.Schaar, 0);
        LevelOneObjects.Add(ObjectType.Type.Voetbal, 0);
        LevelOneObjects.Add(ObjectType.Type.VoetbalSchoen, 0);
        LevelOneObjects.Add(ObjectType.Type.Netstuk, 0);
        LevelOneObjects.Add(ObjectType.Type.Paal, 0);
    }

    public void UpdateScore(ObjectType.Type _type)
    {
        AddScore(_type);
        Displaytext();
    }

    private void AddScore(ObjectType.Type _type)
    {
        TotalCaught++;

        if (_type == ObjectType.Type.Schaar || _type == ObjectType.Type.Bowlingbal)
        {
            if (_type == ObjectType.Type.Schaar)
            {
                ScissorsData.Value++;
            }
            else
            {
                BowlingBallData.Value++;
            }

            Timer.TimePenalty();
            return;
        }

        if (LevelOneObjects.ContainsKey(_type))
        {
            LevelOneObjects[_type]++;
            return;
        }

        LevelOneObjects.Add(_type, 1);
    }

    private void Displaytext()
    {
        foreach (KeyValuePair<ObjectType.Type, int> item in LevelOneObjects)
        {
            ObjectType.Type _type = item.Key;

            if (_type == ObjectType.Type.VoetbalSchoen)
            {
                ShoeData.Value++;
                SchoenText.text = item.Value.ToString();
            }
            else if (_type == ObjectType.Type.Voetbal)
            {
                BallData.Value++;
                BalText.text = item.Value.ToString();
            }
            else if (_type == ObjectType.Type.Paal)
            {
                PoleData.Value++;
                PaalText.text = item.Value.ToString();
            }
            else if (_type == ObjectType.Type.Netstuk)
            {
                NetData.Value++;
                NetstukText.text = item.Value.ToString();
            }
        }
    }

    public void CalculateScorePercentage(int id)
    {
        
        Score.Value = LevelOneObjects[ObjectType.Type.VoetbalSchoen] + LevelOneObjects[ObjectType.Type.Voetbal] + LevelOneObjects[ObjectType.Type.Paal] + LevelOneObjects[ObjectType.Type.Netstuk];

        Score.Value = (Score.Value / 4f) / 5f;
        Score.Value = Mathf.Round(Score.Value * 100);

        // Score.Value = Mathf.Round(Score.Value * 10f) * 0.1f;

        if (LevelManager.Singleton != null)
        {
            LevelManager.Singleton.SaveMinigameOutcome(Score.Value, 100, 50);
        }
    }
}