using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float ReactionThreshhold;
    public ObjectSpawner Spawner;

    [SerializeField] private Sprite m_maleSprite, m_femaleSprite, m_nonbinarySprite;

    private Vector2 _currentvelocity;
    private menuUser_script m_genderScript;
    private SpriteRenderer m_renderer;
    private bool m_inputAllowed;

    Vector2 position;
    Vector2 velocity;
    Vector2 targetPos;
    Vector2 desired_Velocity;
    Vector2 steering;
    [SerializeField]
    private float mass;
    [SerializeField]
    private float max_Velocity;

    [SerializeField]
    private float stoppingDistance;

    private Vector3 m_LastFramePos;
    [SerializeField] float speed;
    private void Awake()
    {
        m_genderScript = FindObjectOfType<menuUser_script>();
        m_renderer = gameObject.GetComponent<SpriteRenderer>();

        if (!m_genderScript) 
        { 
            return;
        }

        if (m_genderScript.gender == "F")
        {
            m_renderer.sprite = m_femaleSprite;
        }
        else if (m_genderScript.gender == "M")
        {
            m_renderer.sprite = m_maleSprite;
        }
        else
        {
            m_renderer.sprite = m_nonbinarySprite;
        }
    }

    private void Start()
    {
        m_LastFramePos = transform.position;

        m_inputAllowed = true;
    }

    private void Update()
    {
        if (m_inputAllowed)
        {
            Ray _ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(_ray, out RaycastHit _raycasthit))
            {
                targetPos.x = _raycasthit.point.x;
            }
            position = position + velocity * Time.deltaTime;
            desired_Velocity = (targetPos - position) * max_Velocity;
            
            float distance = Vector2.Distance(targetPos, position);
            if (distance < stoppingDistance)
            {
                float multiplier = distance / stoppingDistance;
                desired_Velocity *= multiplier;
            }

            steering = (desired_Velocity - velocity) / mass;
            velocity = velocity + steering * Time.deltaTime;
            velocity = Vector2.ClampMagnitude(velocity, max_Velocity);
            position.x = Mathf.Clamp(position.x, -10, 10);

            if (Vector3.Distance(m_LastFramePos, Spawner.ObjectPosition) >= Vector3.Distance(transform.position, Spawner.ObjectPosition))
            {
                if (_currentvelocity.x >= ReactionThreshhold || _currentvelocity.x <= -ReactionThreshhold)
                {
                    Spawner.MeasureReactionTime = false;
                }
            }
            position.y = transform.position.y;
            transform.position = position;
        }

        m_LastFramePos = transform.position;
    }

    public void CancelInput()
    {
        m_inputAllowed = false;
    }

    public bool GetInput()
    {
        return m_inputAllowed;
    }
}