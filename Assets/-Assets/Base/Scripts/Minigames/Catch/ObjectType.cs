using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectType : MonoBehaviour
{
    public enum Type
    {
        VoetbalSchoen,
        Voetbal,
        Paal,
        Netstuk,
        Gloeilamp,
        GlasVanLantaarnpaal,
        HegZaden,
        ZitzakVulling,
        Tablets,
        Knuffels,
        Kussens,
        Bowlingbal,
        Schaar,
        Baksteen,
        Afval,
        Bananenschil,
    }

    public Type Types;
    private Transform Item;
    private GameObject Glow;

    private void Start()
    {
        Item = gameObject.transform.GetChild(0);
        Glow = gameObject.transform.GetChild(1).gameObject;
    }

    private void Update()
    {
        if (Item.localPosition.y <= -1f)
        {
            Glow.SetActive(false);
        } 

        if (Item.position.y <= -10)
        {
            Destroy(gameObject);
        }
    }
}