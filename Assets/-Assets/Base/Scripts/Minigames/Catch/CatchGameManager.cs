using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CatchGameManager : MonoBehaviour
{
    public int CatchID;
    public string LevelID;

    public static CatchGameManager Singleton;

    private void Awake()
    {
        if (Singleton != null && Singleton != this)
        {
            Destroy(gameObject);
            return;
        }

        Singleton = this;
    }

    public void ExitCatchGame()
    {
        gameObject.SetActive(false);
        LevelManager.Singleton.ReturnToSubLevel();
    }
}