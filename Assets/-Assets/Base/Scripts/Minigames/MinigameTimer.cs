using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MinigameTimer : MonoBehaviour
{
    [SerializeField, Range(0,100)]
    private float time = 60;
    [SerializeField]
    private ConclusionManager conclusionManager;

    [SerializeField]
    private UnityEvent OnTimerElapsed;

    private float timeStarted = -1;
    
    public float CurrentTime { get {
        if( timeStarted == -1 )
            return time;

        return Mathf.Max( 0, time - (Time.unscaledTime - timeStarted) );
    }}

    public float Max => time;

    void OnDisable()
    {
        StopAllCoroutines();
    }

    [ContextMenu("Start Timer")]
    public void StartTimer()
    {
        StartCoroutine(Timer());
    }

    public void AddToTimer( float delta )
    {
        timeStarted += delta;
    }

    private IEnumerator Timer()
    {
        timeStarted = Time.unscaledTime;
        yield return new WaitUntil( () => Time.unscaledTime - timeStarted > time );

        if( OnTimerElapsed.GetPersistentEventCount() > 0 )
        {
            OnTimerElapsed.Invoke();
            yield break;
        }

        conclusionManager.StartEndingDialogue();
    }
}
