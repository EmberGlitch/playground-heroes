using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Data/TutorialData")]
public class TutorialData : ScriptableObject
{
    public bool ShownIntro;
}