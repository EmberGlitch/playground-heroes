using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class Grid_ : MonoBehaviour
{
    class Location
    {
        public int X, Y;
        public int F,G,H;
        public Location Parent;
    }

    private int[,] grid;

    [SerializeField]
    private int width;
    [SerializeField]
    private int height;

    public UnityEvent OnGridChange;

    void Awake()
    {
        grid = new int[height, width];
    }

    public bool CanNavigateTo( Vector2Int startPosition, Vector2Int targetPosition )
    {
        Location start = new() {X = startPosition.x, Y = startPosition.y};
        Location target = new() {X = targetPosition.x, Y = targetPosition.y};
        int g = 0;
        List<Location> open = new List<Location>();
        List<Location> closed = new List<Location>();

        open.Add( start );


        while( open.Count > 0 )
        {
            var lowest = open.Min( e => e.F );
            var current = open.First( e => e.F == lowest );

            closed.Add( current );
            open.Remove( current );

            // if we added the destination to the closed list, we've found a path
            if (closed.FirstOrDefault(e => e.X == target.X && e.Y == target.Y) != null)
                break;

            List<Location> adjacents = GetWalkableAdjecents( current.X, current.Y, grid );
            ++g;

            foreach(var adjacentSquare in adjacents)
            {
                // if this adjacent square is already in the closed list, ignore it
                if (closed.FirstOrDefault(l => l.X == adjacentSquare.X
                        && l.Y == adjacentSquare.Y) != null)
                    continue;
            
                // if it's not in the open list...
                if (open.FirstOrDefault(l => l.X == adjacentSquare.X
                        && l.Y == adjacentSquare.Y) == null)
                {
                    // compute its score, set the parent
                    adjacentSquare.G = g;
                    adjacentSquare.H = ComputeHScore(adjacentSquare.X,
                        adjacentSquare.Y, target.X, target.Y);
                    adjacentSquare.F = adjacentSquare.G + adjacentSquare.H;
                    adjacentSquare.Parent = current;
            
                    // and add it to the open list
                    open.Insert(0, adjacentSquare);
                }
                else
                {
                    // test if using the current G score makes the adjacent square's F score
                    // lower, if yes update the parent because it means it's a better path
                    if (g + adjacentSquare.H < adjacentSquare.F)
                    {
                        adjacentSquare.G = g;
                        adjacentSquare.F = adjacentSquare.G + adjacentSquare.H;
                        adjacentSquare.Parent = current;
                    }
                }
            }
        }


        return closed.FirstOrDefault(e => e.X == target.X && e.Y == target.Y) != null;
    }

    public void UpdateGrid( int[,] mask, Vector2Int position )
    {
        int height = mask.GetLength(0);
        int width = mask.GetLength(1);

        int startX = position.x - (int)(width * 0.5f);
        int startY = position.y - (int)(height * 0.5f);

        for (int x = 0; x < width; x++)
        {
            int gridX = startX + x;
            for (int y = 0; y < height; y++)
            {
                int gridY = startY + y;

                grid[gridY, gridX] = mask[y,x];
            }   
        }

        Print2DArray<int>( grid );
        
        OnGridChange?.Invoke();
    }

    private int ComputeHScore(int x, int y, int targetX, int targetY)
    {
        return Math.Abs(targetX - x) + Math.Abs(targetY - y);
    }

    private List<Location> GetWalkableAdjecents(int x, int y, int[,] grid)
    {
        var proposedLocations = new List<Location>()
        {
            new Location { X = x, Y = y - 1 },
            new Location { X = x, Y = y + 1 },
            new Location { X = x - 1, Y = y },
            new Location { X = x + 1, Y = y },
        };
    
        return proposedLocations.Where( l => 
            l.X >= 0 && l.X < grid.GetLength(1) 
            && l.Y >= 0 && l.Y < grid.GetLength(0) 
            && grid[l.Y,l.X] == 1 ).ToList();
    }

    public static void Print2DArray<T>(T[,] matrix)
    {
        if( Debug.isDebugBuild == false && Application.isEditor == false )
            return;

        string text = string.Empty;

        for (int i = 0; i < matrix.GetLength(0); i++)
        {
            for (int j = 0; j < matrix.GetLength(1); j++)
            {
                text += matrix[i,j] + "\t";
            }
            text += "\n";
        }

        Debug.Log( text );
    }

}
