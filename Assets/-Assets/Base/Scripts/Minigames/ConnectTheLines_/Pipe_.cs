using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

public class Pipe_ : MonoBehaviour
{
    private static Regex regex = new Regex( @"\[(.*?)\]" );

    private Grid_ grid;

    [SerializeField]
    private ConnectManager manager;

    public Vector2Int Position => position;
    private Vector2Int position;

    [field: SerializeField] public bool Connected {get; private set;}
    [field: SerializeField] public bool Highlight { get; internal set; }

    private new SpriteRenderer renderer;
    [SerializeField] private Sprite normal;
    [SerializeField] private Sprite highLight;
    [SerializeField] private Sprite connected;

    [SerializeField, TextArea]
    private string mask = @"000
000
000";

    private int[,] current;

    void Start()
    {
        renderer = GetComponent<SpriteRenderer>();
        grid = GetComponentInParent<Grid_>();

        if( regex.IsMatch( name ) == false )
        {
            return;
        }

        string match = regex.Match( name ).Groups[1].Value;
        string[] indices = match.Split(',');
        position = new Vector2Int( 
            int.Parse(indices[0].Trim()),
            int.Parse(indices[1].Trim())
        );

        grid.OnGridChange.AddListener( OnGridChanged );
        
        CreateMask();
    }

    void LateUpdate()
    {
        if( Highlight == true )
        {
            renderer.sprite = highLight;
            return;
        }

        renderer.sprite = Connected? connected : normal;

    }

    private void OnGridChanged()
    {
        Connected = grid.CanNavigateTo( position, manager.start.Position );        
    }

    private void CreateMask()
    {
        current = new int[3, 3];
        int progress = 0;
        for( int i = 0; i < mask.Length; ++i )
        {   
            char b = mask[i];
            if( b == '0' || b == '1' )
            {
                current[ progress / 3, progress % 3 ] = int.Parse( string.Empty + b );
                ++progress;
            }
        }   

        Grid_.Print2DArray<int>( current );
        grid?.UpdateGrid( current, position );
    }

    public void Rotate( int rotationDirection )
    {
        Vector3 center = new( 1, 1);
        int[,] newMask = new int[ current.GetLength(0), current.GetLength(1) ];
        Quaternion rotationAmount = Quaternion.Euler( 0, 0, rotationDirection * -90 );
        

        for( int y = 0; y < current.GetLength(0); ++y )
        {
            for( int x = 0; x < current.GetLength(1); ++x )
            {
                Vector3 direction = new Vector3( x, y ) - center;
                Vector3 newPosition = (Quaternion.Euler( 0, 0, rotationDirection * 90 ) * direction) + center;
                newMask[ Mathf.RoundToInt(newPosition.y), Mathf.RoundToInt(newPosition.x) ] = current[y,x];
            }
        }

        transform.rotation *= rotationAmount;

        current = newMask;
        Grid_.Print2DArray<int>( current );
        grid?.UpdateGrid( current, position );
    }
}
