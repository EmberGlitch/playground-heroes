using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PipeInput : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    private Pipe_ pipe;

    [SerializeField] private IntReference clicks;
    [SerializeField] private BoolReference playState;

    void Start()
    {
        pipe = GetComponent<Pipe_>();    
    }


    public void OnPointerClick(PointerEventData eventData)
    {
        if( playState.Value == false )
            return;

        int direction = 1;
        if( eventData.button == PointerEventData.InputButton.Right )
            direction = -1;

        pipe.Rotate( direction );

        clicks.Value++;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        pipe.Highlight = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        pipe.Highlight = false;
    }
}
