using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConnectManager : MonoBehaviour
{
    public bool endConnected;

    public Pipe_ start;
    public Pipe_ end;
    public Pipe_[] pipes;

    private Grid_ grid;

    IEnumerator Start()
    {
        grid = GetComponent<Grid_>();
        grid.OnGridChange.AddListener( OnGridChanged );
        pipes = GetComponentsInChildren<Pipe_>();

        yield return null;

        for( int i = 0; i < 100; ++i)
        {
            pipes[Random.Range(2,pipes.Length)].Rotate(1);
        }
    }

    public Pipe_[] GetPipes()
        => pipes;

    private void OnGridChanged()
    {
        endConnected = grid.CanNavigateTo( start.Position, end.Position );
    }
}
