using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Data/FloatVariableReference")]
public class FloatReference : ScriptableObject
{
    public float MaxValue;
    public float Value;
}