using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Data/BoolVariableReference")]
public class BoolReference : ScriptableObject
{
    [field: SerializeField] public bool Value {get; set;}
}