using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CookingSpace
{
    public class CookingPoints : MonoBehaviour
    {
        [SerializeField] private Animator anim;

        public ClickPoints clickPoint;

        public float AnimationSpeed;

        private void Start()
        {
            anim.speed = AnimationSpeed;
            StartAnimation();
        }

        private void Update()
        {
            if (GetComponent<RectTransform>().sizeDelta == new Vector2(0, 0))
            {
                clickPoint.MissedPoint();
                gameObject.SetActive(false);
            }
        }

        public void DeactivatePoint()
        {
            clickPoint.ClickedPoint();
            gameObject.SetActive(false);
        }

        private void StartAnimation()
        {
            anim.PlayInFixedTime("PointsAnimation");
        }
    }
}
