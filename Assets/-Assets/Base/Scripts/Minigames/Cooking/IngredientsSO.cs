using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "New Ingrediënt", menuName = "Cooking/IngrediëntSO")]
public class IngredientsSO : ScriptableObject
{
    public string   naam;

    public Sprite IngredientSprite;
    public Sprite CutSprite;
    public Sprite FinishedSprite;
    public Sprite[] TypeIngredientsSprite;
    public GameObject ing;

    public Sprite[] IngredientStages;
}

