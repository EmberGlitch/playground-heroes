using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

namespace CookingSpace
{
    public class SwitchManager : MonoBehaviour
    {
        [SerializeField] private Animator CookAnim;
        [SerializeField] private ClickPoints clickPoints;

        public static SwitchManager switchManager;

        public GameObject[] Fase;
        public GameObject[] DataCollectors;
        public Sprite[] CounterSprites;

        public Transform Board, Moving;

        public Image IngSprite, IngCutSprite;
        public Image[] IngFinishedSprites;
        public Image BackroundImage;

        public Vector3 StartingPos;

        public float StopRangeCuttingBoard, EndDistanceCuttingBoard;

        public int FaseCount, BoardCount, DirectionAndSpeed;
        public bool EnableBoardMoving, EnablePoints, StateBool;

        public Animator BoardAnim;

        private bool FaseDone = false;

        private void Awake()
        {
            switchManager = this;
            IngSprite.sprite = Board.GetChild(1).GetComponent<Image>().sprite;
        }

        private void Update()
        {
            //Automatically switches fases whenever reached end ingrediënt
            if (BoardCount == CookingManager.cookingManager.Ingredients.Length && !FaseDone)
            {
                SwitchFase();
                FaseDone = true;
            } 

        }

        public void SwitchFase()
        {
            if (Fase[FaseCount].activeSelf && FaseCount != 3)
            {
                Fase[FaseCount].SetActive(false);
                FaseCount++;
            }

            BackroundImage.sprite = CounterSprites[FaseCount];
            Fase[FaseCount].SetActive(true);

            if (Fase[FaseCount].activeSelf && FaseCount == 2)
            {
                CookingManager.cookingManager.CookIngredients();
            }
            else if (FaseCount == 3)
            {
                Fase[FaseCount].SetActive(true);
                CookAnim.Play("Koken_2");
            }
        }

        public void SwitchBoard()
        {
            if( Fase[1].activeSelf == false )
                return;

            DataCollectors[BoardCount].gameObject.SetActive(false); // disable previous ingredient

            BoardCount = Mathf.Clamp(BoardCount + 1, 0, CookingManager.cookingManager.Ingredients.Length);

            if (BoardCount >= CookingManager.cookingManager.Ingredients.Length)
                return;            

            clickPoints.IngredientOnBoard.points = clickPoints.IngPoly[BoardCount].points;
            clickPoints.IngredientOnBoard.offset = clickPoints.IngPoly[BoardCount].offset;

            clickPoints.SecondsBetweenSpawningPoints -= clickPoints.NumberToSubstract;

            if (BoardCount != CookingManager.cookingManager.Ingredients.Length)
            {
                IngSprite.sprite = CookingManager.cookingManager.Ingredients[BoardCount].GetComponent<Image>().sprite;
            }
        }

        public void EndBoardEvent()
        {
            BoardAnim.SetBool("EndBoard", false);

            clickPoints.ResetRound();

            IngSprite.sprite = CookingManager.cookingManager.INGSO[BoardCount].IngredientSprite;

            EnableBoardMoving = false;
            clickPoints.Check = true;
            clickPoints.CuttingboardCuts.enabled = false;
            for (int i = 0; i < IngFinishedSprites.Length; i++)
            {
                IngFinishedSprites[i].gameObject.SetActive(false);
            }

            clickPoints.RepositionPoints();
            SwitchBoard();
        }

        public void MoveBoard(bool enable)
        {
            if (Moving.position.x <= StopRangeCuttingBoard)
            {
                Moving.Translate(Vector3.right * Time.deltaTime * DirectionAndSpeed); 
            }
            if (enable)
            {
                Moving.Translate(Vector3.right * Time.deltaTime * DirectionAndSpeed);
                //CookingManager.cookingManager.IngredientsForCooking[BoardCount].GetComponent<Image>().sprite = IngSprite.sprite;

                clickPoints.ResetRound();

                if (Moving.position.x >= EndDistanceCuttingBoard)
                {
                    IngSprite.sprite = CookingManager.cookingManager.INGSO[BoardCount].IngredientSprite;
                    Moving.position = StartingPos;

                    EnableBoardMoving = false;
                    clickPoints.Check = true;
                    clickPoints.CuttingboardCuts.enabled = false;
                    for (int i = 0; i < IngFinishedSprites.Length; i++)
                    {
                        IngFinishedSprites[i].gameObject.SetActive(false);
                    }

                    clickPoints.RepositionPoints();
                    
                    SwitchBoard();
                }
            }
        }

        public void ChangeSprite(bool isGood)
        {
            int sprite = System.Convert.ToInt32(isGood);

            if (sprite < CookingManager.cookingManager.INGSO.Length && Fase[1].activeSelf)
                IngSprite.sprite = CookingManager.cookingManager.INGSO[BoardCount].TypeIngredientsSprite[sprite];
        }
    }
}