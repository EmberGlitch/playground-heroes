using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

namespace CookingSpace
{
    public class ClickPoints : MonoBehaviour
    {
        [SerializeField] private GameObject ButtonPref, ButtonsParent;
        [SerializeField] private GameObject[] Points;

        [SerializeField] private Vector3 StartScalePoints;
        [SerializeField] private Vector2 MinRange, MaxRange;

        [SerializeField] private SwitchManager switchManager;
        [SerializeField] private CookingManager cookingManager;

        public RectTransform KnifePos;

        public PolygonCollider2D[] IngPoly;
        public PolygonCollider2D IngredientOnBoard;

        public Image CuttingboardCuts;
        public Sprite[] BoardCuts;

        private Vector2 SpawnLoc;

        private int ChildIndex;

        private float _MDB;

        public float SecondsBetweenSpawningPoints, NumberToSubstract, RemainingPoints, MinimumDistanceBetweenPoints;

        public int TotalPoints, Count, NotClickedPoints;

        public bool Check;

        private int IngSpriteIndex;

        private void Awake()
        {
            Points = new GameObject[TotalPoints];
            RemainingPoints = TotalPoints;

            _MDB = MinimumDistanceBetweenPoints;
        }

        private void Update()
        {
            CheckForNextRound();
        }

        public void DefineCollidersOnIngredients()
        {
            IngPoly = new PolygonCollider2D[cookingManager.Ingredients.Length];
            for (int i = 0; i < IngPoly.Length; i++)
            {
                IngPoly[i] = cookingManager.Ingredients[i].GetComponent<PolygonCollider2D>();
            }
        }

        private void CheckForNextRound()
        {
            if (RemainingPoints == 0 && Check || NotClickedPoints >= RemainingPoints && Check)
            {
                Check = false;
                CheckIngredientState();
            }
        }

        private void CheckIngredientState()
        {
            StartCoroutine(WaitForCut());
        }

        private void UpdateScore()
        {
            for (int i = 0; i < Points.Length; i++)
            {
                Points[i].SetActive(false);
            }
        }

        public void GoCreatePoints()
        {
            RemainingPoints = TotalPoints;
            CreatePoints();
        }

        public void RepositionPoints()
        {
            RemainingPoints = TotalPoints;
            IngredientOnBoard.points = IngPoly[switchManager.BoardCount].points;
            IngredientOnBoard.offset = IngPoly[switchManager.BoardCount].offset;
            for (int i = 0; i < Points.Length; i++)
            {
                Vector3 XY = GetRandomPosition();
                while (CheckOverlapping(XY))
                {
                    XY = GetRandomPosition();
                    if (Count >= 10)
                    {
                        _MDB -= 5;
                        Count = 0;
                    }
                    Count++;
                }
                Points[i].transform.position = XY;
                _MDB = MinimumDistanceBetweenPoints;
            }
            GoPointsActive();
        }

        private Vector3 GetRandomPosition()
        {
            SpawnLoc.x = Random.Range(IngredientOnBoard.bounds.min.x, IngredientOnBoard.bounds.max.x);
            SpawnLoc.y = Random.Range(IngredientOnBoard.bounds.min.y, IngredientOnBoard.bounds.max.y);

            return SpawnLoc;
        }

        private bool CheckOverlapping(Vector3 Pos)
        {
            for (int i = 0; i < Points.Length; i++)
            {
                if (Vector3.Distance(Pos, Points[i].transform.position) < _MDB)
                {
                    return true;
                }
            }
            return false;
        }

        private void GoPointsActive()
        {
            SwitchManager.switchManager.EnablePoints = true;
            StartCoroutine(SetPointActive());
        }

        public void ResetRound()
        {
            IngSpriteIndex = 0;
            RemainingPoints = NotClickedPoints = 0;
        }

        public void ClickedPoint()
        {
            --RemainingPoints;

            CookingManager.cookingManager.AddScore();
            
            StartCoroutine(CutAnim());
        }

        public void ChangeSprite()
        {
            switchManager.IngSprite.sprite = cookingManager.INGSO[switchManager.BoardCount].IngredientStages[IngSpriteIndex];

            switchManager.IngCutSprite.sprite = cookingManager.INGSO[switchManager.BoardCount].CutSprite;
            switchManager.IngFinishedSprites[IngSpriteIndex].sprite = cookingManager.INGSO[switchManager.BoardCount].FinishedSprite;

            switchManager.IngFinishedSprites[IngSpriteIndex].gameObject.SetActive(true);
            switchManager.IngFinishedSprites[IngSpriteIndex].SetNativeSize();
            switchManager.IngCutSprite.SetNativeSize();

            IngSpriteIndex++;
        }

        private void SetCutSprite(bool _active)
        {
            switchManager.IngCutSprite.gameObject.SetActive(_active);
        }

        private IEnumerator SetPointActive()
        {
            ChildIndex = 0;
            while (ChildIndex < TotalPoints)
            {
                yield return new WaitForSeconds(SecondsBetweenSpawningPoints);
                Points[ChildIndex].gameObject.SetActive(true);
                ChildIndex++;
            }
        }

        private IEnumerator WaitForCut()
        {
            yield return new WaitForSeconds(1f);
            switchManager.EnableBoardMoving = true;
            switchManager.BoardAnim.SetBool("EndBoard", true);
            UpdateScore();
        }

        private void CreatePoints()
        {
            for (int i = 0; i < Points.Length; ++i)
            {
                Points[i] = Instantiate(ButtonPref, ButtonsParent.transform);
                CookingPoints point = Points[i].GetComponent<CookingPoints>();
                point.clickPoint = this;

                Points[i].SetActive(false);
            }
            RepositionPoints();
        }

        public void MissedPoint()
        {
            NotClickedPoints++;
            if (NotClickedPoints == 1)
            {
                CuttingboardCuts.sprite = BoardCuts[NotClickedPoints - 1];
                CuttingboardCuts.enabled = true;
            }

            CuttingboardCuts.sprite = BoardCuts[NotClickedPoints - 1];
        }

        public IEnumerator CutAnim()
        {
            KnifePos.localPosition = new Vector3(-60, 56, 0);
            KnifePos.localRotation = new Quaternion(0, 0, 0, 180);

            ChangeSprite();
            SetCutSprite(true);
            yield return new WaitForSeconds(0.1f);

            SetCutSprite(false);
            KnifePos.localPosition = new Vector3(-93.4f, 85.6f, 0);
            KnifePos.localRotation = new Quaternion(0, 0, 26.159f, 180);
        }
    }
}
//private IEnumerator DefinePoints()
//{
//    for (int i = 0; Count < TotalPoints; i++)
//    {
//        radius = new Vector3(Random.Range(MinRange.x, MaxRange.x), Random.Range(MinRange.y, MaxRange.y), 0);

//        Instantiate(ButtonPref, radius, Quaternion.identity, ButtonsParent.transform);
//        Count++; RemainingPoints++;

//        yield return new WaitForSeconds(0.1f);

//        if (Count >= TotalPoints)
//        {
//            SwitchManager.switchManager.EnablePoints = true;
//            ChildIndex = 0;
//            StartCoroutine(SetPointActive());
//        }
//        //radius = new Vector3(Random.Range(MinRange.x, MaxRange.x), Random.Range(MinRange.y, MaxRange.y), 0);

//        //if (Points.Length != 0)
//        //{
//        //    PointsPos = Points[CountHolder].transform.position;
//        //    if (radius.x < PointsPos.x + -100 ||
//        //        radius.x > PointsPos.x + 100 &&
//        //        radius.y < PointsPos.y + -100 ||
//        //        radius.y > PointsPos.y + 100) { CountHolder++; }
//        //}
//        //CreatePoints();

//        //radius = new Vector3(Random.Range(switchManager.IngSprite.rectTransform.position.x, switchManager.IngSprite.rectTransform.position.y), Random.Range(switchManager.IngSprite.rectTransform.position.y, switchManager.IngSprite.rectTransform.position.x), 0);
//    }
//}
//if (Points.Length <= 0)
//{ radius = new Vector3(Random.Range(MinRange.x, MaxRange.x), Random.Range(MinRange.y ,MaxRange.y), 0); }
//else
//{
//    //Range.x = Points[CountHolder].transform.position.x + AddRange.x;
//    //Range.y = Points[CountHolder].transform.position.y + AddRange.y;

//    radius = new Vector3(Random.Range(Points[CountHolder].transform.position.x, Range.x), Random.Range(Points[CountHolder].transform.position.y, Range.y), 0);

//    CountHolder++;

//    //Mathf.Clamp(radius.x, SPx, SPx + AddRange.x);
//    //Mathf.Clamp(radius.y, SPy, SPy + AddRange.y);
//}
//CreatePoints();

//private void CreatePoints()
//{
//    for (int i = 0; i < TotalPoints; i++)
//    {
//        Instantiate(ButtonPref, radius, Quaternion.identity, ButtonsParent.transform);
//        TotalPoints++; RemainingPoints++;
//    }
//}

//private void RepositionPoints()
//{
//    radius = new Vector3(Random.Range(MinRange.x, MaxRange.x), Random.Range(MinRange.y, MaxRange.y), 0);
//    foreach (var buttonPref in Points)
//    {
//        buttonPref.transform.position = radius;
//    }
//    GoPointsActive();

//    //for (int i = 0; i < Points.Length; i++)
//    //{
//    //    //if (Points[i].transform.position != new Vector3(0, 0, 0))
//    //    //{  }
//    //}
//}
//private void GoPointsActive()
//{
//    SwitchManager.switchManager.EnablePoints = true;
//    ChildIndex = 0;
//    StartCoroutine(SetPointActive());
//}