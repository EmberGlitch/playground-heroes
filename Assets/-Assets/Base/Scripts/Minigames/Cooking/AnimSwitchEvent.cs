using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CookingSpace
{
    public class AnimSwitchEvent : MonoBehaviour
    {
        public SwitchManager SwitchManager;

        public void SwitchEvent()
        {
            SwitchManager.SwitchFase();
        }
    }
}
