using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CookingSpace
{
    public class CuttingBoardEvent : MonoBehaviour
    {
        public SwitchManager switchManager;

        public void BoardEvent()
        {
            switchManager.EndBoardEvent();
        }
    }
}
