using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CookingSpace
{
    public class WashingTools : MonoBehaviour
    {
        [SerializeField] SwitchManager switchManager;
        [SerializeField] ClickPoints clickPoints;
        public void StartWashingAnimation()
        {
            switchManager.SwitchFase();
            clickPoints.DefineCollidersOnIngredients();
            clickPoints.GoCreatePoints();
        }

        public void ChangeCounterSprite()
        {
            switchManager.BackroundImage.sprite = switchManager.CounterSprites[1];
        }
    }
}
