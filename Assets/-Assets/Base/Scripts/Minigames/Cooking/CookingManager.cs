using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;
using TMPro;
using System;

namespace CookingSpace
{
    public class CookingManager : MonoBehaviour
    {
        public Sprite[] FinalDishSprites; 

        [SerializeField] private GameObject FinalDish;
        [SerializeField] private SwitchManager switchManager;
        [SerializeField] private ClickPoints clickPoints;
        [SerializeField] private Animator[] anim;

        private bool m_calculatedScore = false;

        public ConclusionManager Conclusion;

        [Space(10)]
        [SerializeField] private TMP_Text NormalGameTimer_TXT;

        [SerializeField] private TMP_Text EightSecondTimer_TXT;
        //private float Timer;

        [Space(20)]
        public static CookingManager cookingManager;

        //public int Score;
        public FloatReference Score;

        [Header("Ingredi�nt Order")]
        public GameObject[] IngredientsForCooking;

        public IngredientsSO[] INGSO;
        public GameObject[] Ingredients;

        public GameObject[] Gerecht;

        private void Start()
        {
            Score.Value = 50;

            GetIngredients();
            CookIngredients();
            StartCoroutine(WaitForWashingAnimation());
        }

        private void Awake()
        {
            cookingManager = this;
        }

        private void Update()
        {
            if (Input.GetKeyUp(KeyCode.E))
            {
                for (int i = 0; i < Ingredients.Length; i++)
                {
                    Ingredients[i].GetComponent<Ingredient>().enabled = true;
                }
                GetIngredients();
            }

            if (FinalDish.activeSelf)
            {
                StartCoroutine("ShowEndUIWithDelay");
            }
        }

        public void GetIngredients()
        {
            Ingredients = new GameObject[INGSO.Length];

            for (int i = 0; i < INGSO.Length; ++i)
            {
                Ingredients[i] = INGSO[i].ing;

                SwitchManager.switchManager.Board.GetChild(0).GetComponent<Image>().sprite = Ingredients[SwitchManager.switchManager.BoardCount].GetComponent<Image>().sprite;
            }
        }

        public void SubstractScore(int score)
        {
            Score.Value -= score;
        }

        public void AddScore()
        {
            Score.Value += 10;
        }

        public void CookIngredients()
        {
            LevelManager lvlm = LevelManager.Singleton;

            if (switchManager.Fase[2].activeSelf)
            {
                if (lvlm != null && !m_calculatedScore)
                {
                    m_calculatedScore = true;
                    LevelManager.Singleton.SaveMinigameOutcome(Score.Value, 80, 50);

                    if (Score.Value >= 80)
                    {
                        FinalDish.GetComponent<Image>().sprite = FinalDishSprites[0];
                    }
                    else if (Score.Value >= 50)
                    {
                        FinalDish.GetComponent<Image>().sprite = FinalDishSprites[1];
                    }
                    else
                    {
                        FinalDish.GetComponent<Image>().sprite = FinalDishSprites[2];
                    }
                }

                anim[1].PlayInFixedTime("Koken");
            }
        }

        public void ExitGame()
        {
            LevelManager.Singleton.ReturnToSubLevel();
        }

        private IEnumerator ShowEndUIWithDelay()
        {
            yield return new WaitForSeconds(1);
            Conclusion.StartEndingDialogue();
        }

        private IEnumerator WaitForWashingAnimation()
        {
            if (switchManager.Fase[0].activeSelf)
            {
                anim[0].PlayInFixedTime("Washing");
                yield return new WaitForSeconds(1);             
            }
        }
    }
}