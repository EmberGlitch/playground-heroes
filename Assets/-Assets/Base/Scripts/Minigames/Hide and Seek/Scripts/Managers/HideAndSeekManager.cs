using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TMPro;
using System.Linq;

public class HideAndSeekManager : MonoBehaviour
{
    [SerializeField] private GameObject m_Outro;
    [SerializeField] private GameObject Game;

    [SerializeField] private TMP_Text TXT_Timer;

    public int collectCount = 5;
    public float CollectPenalty = -3;
    
    [SerializeField] private Sprite[] itemsToCollect;
    [SerializeField] private Dictionary<Sprite, int> counts;

    public ConclusionManager Conclusion;
    public FloatReference Score;
    [SerializeField] private MinigameTimer timer;

    public static HideAndSeekManager HAS;
    public float Percentage, PForTXT;

    private void Start()
    {
        PlaygroundHeroes.EventSystem.Add( "Pickup", CheckItems );
    }

    private void Awake()
    {
        HAS = this;

        counts = new Dictionary<Sprite, int>();
    }

    private void Update()
    {
        if (Percentage == 1)
        {
            Outro();
        }
    }

    public void CheckItems(object arg)
    {
        Sprite sprite = (Sprite) arg;
        if( counts.ContainsKey( sprite ) == false )
            counts.Add( sprite, 0 );

        counts[sprite] = Mathf.Clamp( counts[sprite] + 1, 0, collectCount );

        Percentage = 0;
        for (int i = 0; i < itemsToCollect.Length; i++)
        {
            if( counts.TryGetValue( itemsToCollect[i], out int count ) )
                Percentage += count;
        }

        Percentage /= itemsToCollect.Length * collectCount;


        if( itemsToCollect.Contains( sprite ) )
            return;

        timer.AddToTimer(CollectPenalty);
    }

    public int GetCount(Sprite sprite)
    {
        if( counts.ContainsKey( sprite ) )
            return counts[sprite];

        return 0;
    }

    public void Outro()
    {
        PForTXT = Percentage * 100;

        Score.Value = PForTXT;

        LevelManager.Singleton?.SaveMinigameOutcome(PForTXT, 75, 25);

        Conclusion.StartEndingDialogue();
    }   

    public void ExitGame()
    {
        LevelManager.Singleton.ReturnToSubLevel();
    }
}