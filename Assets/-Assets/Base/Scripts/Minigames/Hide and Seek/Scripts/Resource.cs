using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Minigame.HideAndSeek
{    
    public class Resource : MonoBehaviour, IPointerClickHandler
    {
        private new SpriteRenderer renderer;
        
        void Awake()
        {
            renderer = GetComponent<SpriteRenderer>();

            float scale = Maths.Map( transform.position.y, -1.75f, -9.81f, 0.45f, 1.0f);
            transform.localScale = Vector3.one * scale;
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            PlaygroundHeroes.EventSystem.Broadcast( "Pickup", renderer.sprite );

            gameObject.SetActive( false );
        }
    }    
}

