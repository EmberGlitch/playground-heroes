using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Line : MonoBehaviour
{
    public Vector3 start;
    public Vector3 end;

    private Vector3 direction;

    void Awake()
        => direction = (start - end).normalized;

    public Vector3 GetPointOnLine( Vector3 point )
    {
        Vector3 relative = point - start;
        float dot = Vector3.Dot( relative, direction );
        return start + direction * dot;
    }

    public static Vector2 GetRangeBetweenLines( Vector3 position, Line a, Line b)
    {
        Vector3 pointOnA = a.GetPointOnLine( position );
        Vector3 pointOnB = b.GetPointOnLine( position );
        return new Vector2( pointOnA.x, pointOnB.x );
    }
}
