using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResourceUI : MonoBehaviour
{
    [SerializeField]
    private new Image renderer;
    [SerializeField]
    private TMPro.TMP_Text text;    

    void Awake()
    {
        renderer = GetComponent<Image>();
        text = GetComponentInChildren<TMPro.TMP_Text>();
    }

    void Update()
    {
        if( HideAndSeekManager.HAS == null )
            return;

        int count = HideAndSeekManager.HAS.GetCount( renderer.sprite );
        int target = HideAndSeekManager.HAS.collectCount;
        text.text = $"{count}/{target}";
    }
}
