using System.Collections;
using System.Collections.Generic;
using DialogSystem;
using UnityEngine;
using UnityEngine.EventSystems;

public class Box : MonoBehaviour, IDragHandler, IPointerClickHandler
{
    [SerializeField]
    private Sprite containingResource;

    [SerializeField]
    private Sprite BoxOpen;

    [SerializeField]
    private bool canDrag = true;

    [SerializeField]
    private Line left;
    [SerializeField]
    private Line right;

    [SerializeField,Range(0,1)]
    private float minScale;

    [SerializeField,Range(0,1)]
    private float maxScale;

    private float depth;

    [SerializeField]
    private GameObject popup;
    private SpriteRenderer popupRenderer;

    private new Collider2D collider;

    void Awake()
    {
        collider = GetComponent<Collider2D>();

        if( popup != null )
            popupRenderer = popup.GetComponent<SpriteRenderer>();
    }

    void Start()
    {
        float y = depth = transform.position.y;
        var renderer = GetComponentInChildren<SpriteRenderer>();
        renderer.sortingOrder = -(int)(y * 100);

        if( popupRenderer )
            popupRenderer.sortingOrder = renderer.sortingOrder + 1;

        float scale = Maths.Map( y, -1.75f, -9.81f, 0.45f, 1.0f);
        transform.localScale = Vector3.one * scale;
    }

    public void OnDrag(PointerEventData eventData)
    {
        if( canDrag == false )
            return;

        Vector3 delta = eventData.delta;
        delta.y = 0;

        Vector3 desiredPosition = transform.position + (delta * Time.deltaTime);
        Vector2 limits = Line.GetRangeBetweenLines( desiredPosition, left, right );
        
        float min = (collider.bounds.min.x - collider.bounds.center.x) * minScale;
        float max = (collider.bounds.max.x - collider.bounds.center.x) * maxScale;

        desiredPosition.x = Mathf.Clamp( desiredPosition.x, limits.x - min, limits.y - max ); 
        transform.position = desiredPosition;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if( containingResource == null )
            return;

        if( eventData.clickCount >= 2 )
        {
            PlaygroundHeroes.EventSystem.Broadcast( "Pickup", containingResource );
            

            GetComponentInChildren<SpriteRenderer>().sprite = BoxOpen;

            if( popup == null )
                return;

            popupRenderer.sprite = containingResource;
            containingResource = null;
            
            var seq = LeanTween.sequence();
            seq.append( LeanTween.value(popup, 0f, 1f, 0.2f).setOnUpdate( (float v) => {
                Color color = popupRenderer.color;
                color.a = v;
                popupRenderer.color = color;
            } ) );
            seq.append( () => {
                LeanTween.moveLocalY( popup, 6.66f, 3.0f );                
                LeanTween.value(popup, 1f, 0f, 0.5f).setOnUpdate( (float v) => {
                    Color color = popupRenderer.color;
                    color.a = v;
                    popupRenderer.color = color;
                } );
            } );
        }
    }
}
