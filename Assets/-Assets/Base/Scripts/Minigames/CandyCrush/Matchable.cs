using System;
using System.Collections;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.PlayerLoop;


//TODO: target variable send data only when target and p_Position are ==
public class Matchable : MonoBehaviour
{
    //refence to the coroutine used for movement
    private Coroutine routine;
    //Used to determine if the backend should be updated
    public Vector2 p_target;
    //defined when instantiated and changed during the game
    public Vector2 p_Position;

    [SerializeField, Range(1,5)]
    private float speed = 0;

    public Sprite powerupSprite;

    public Vector2 Position {
        set {
            p_Position = value;
            p_target = value;
            if (routine != null) {
                StopCoroutine(routine);
            }
            routine = StartCoroutine( Move() );
        }
        get => p_Position;
    }

    //defined by serialisedfield in the prefab
    public int p_type;
    //used to mark matchables ast a potentional match so they can be swapped back if needed
    public bool p_destruction = false;
    //changes later in the game so should be false by default
    public bool p_IsPowerUp = false;
    
    private GameObject matchAnimation;

    private void Awake() {
        matchAnimation = Resources.Load<GameObject>("ParticleSystem");
    }
    
    /// <summary>
    /// Call this function to destroy item with a fancy animation
    /// </summary>
    public void DestroyItem() {
        StartCoroutine(DestroyMatch());
    }

    public void BecomePowerup() {
        p_IsPowerUp = true;
        gameObject.GetComponent<SpriteRenderer>().sprite = powerupSprite; 
    }
    
    private IEnumerator Move() {
        float t = 0;
        while(t < 1) {
            transform.position = Vector3.Lerp( transform.position, p_Position, t);
            t += Time.deltaTime * speed;
            yield return null;
        }
        
        transform.position = p_Position;

        yield return new WaitForSeconds(0.75f);
    }

    private IEnumerator DestroyMatch() {
        while (new Vector2(transform.position.x, transform.position.y) != p_target) {
            yield return new WaitForEndOfFrame();
        }
        GameObject animation = Instantiate(matchAnimation, transform.position, quaternion.identity);
        yield return new WaitForSeconds(0.5f);
        Destroy(animation);
        p_destruction = true;
        yield return new WaitForSeconds(0.1f);
        Destroy(this.gameObject);
    }
}
