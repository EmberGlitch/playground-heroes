using System;
using System.Collections;
using System.Collections.Generic;
using Fungus;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

public enum GameState {
    go,
    wait
}

public class Board : MonoBehaviour {
    public int gridX;
    public int gridY;
    [SerializeField] private GameObject[] prefabs;

    private const int goalCount = 20;
    [SerializeField] private int goal1CountDown = goalCount;
    [SerializeField] private int goal2CountDown = goalCount;

    [SerializeField] private TMP_Text goal1Text;
    [SerializeField] private TMP_Text goal2Text;

    [SerializeField] private SpriteRenderer goal1Sprite;
    [SerializeField] private SpriteRenderer goal2Sprite;

    [SerializeField] private Sprite[] sprites;

    [SerializeField] private GameState currentState = GameState.go;

    [SerializeField] private Shuffle shuffle;

    [SerializeField] private FloatReference score;
    
    private int goal1Comptype;
    private int goal2Comptype;
    
    public GameObject[,] matchables;

    private float swipeAngle = 0;
    private float swipeResister = 0.9f;
    
    private GameObject firstTouch;
    private GameObject finalTouch;
        
    private GameObject otherMatchable;

    private bool playerStarted = false;

    [SerializeField]
    private UnityEvent OnGameCompleted;

    //Functions after start and update are ordered by when they are used
    void Start() {
        //sets the goals
        goal1Comptype = setGoal();
        goal1Sprite.sprite = sprites[goal1Comptype];
        goal1Text.text = "" + goal1CountDown;
        goal2Comptype = setGoal();
        while (goal2Comptype == goal1Comptype) {
            goal2Comptype = setGoal();
        }
        goal2Sprite.sprite = sprites[goal2Comptype];
        goal2Text.text = "" + goal2CountDown;
        
        // sets the array with objects so that it can actually be used
        matchables = new GameObject[gridX, gridY];
        GridBuilder(true);
    }

    void Update() {
        //Input
        if (currentState == GameState.go) {
            //begin swipe
            if (Input.GetKeyDown(KeyCode.Mouse0)) {
                playerStarted = true;
                InitSwipe();
            }
            //end swipe
            else if (Input.GetKeyUp(KeyCode.Mouse0)) {
                InputManagement();
            }
        }
    }

    private int setGoal() {
        int type = Random.Range(1, 6);
        return type;
    }
    
    /// <summary>
    /// Generates a grid with randomised items and calls sort array if matches are expected
    /// </summary>
    private void GridBuilder(bool matches) {
        //generates the grid based on a double for loop
        for (int x = 0; x < gridX; x++) {
            for (int y = 0; y < gridY; y++) {
                // Removes the "Clear" matchable
                if (matchables[x, y]) {
                    if (matchables[x, y].GetComponent<Matchable>().p_type == 0) {
                        matchables[x, y].GetComponent<Matchable>().DestroyItem();
                        matchables[x, y].GetComponent<SpriteRenderer>().enabled = false;
                        matchables[x, y] = null;
                    }
                    else if (y != 0 && !matchables[x, y - 1]) {
                        matchables[x, y].GetComponent<Matchable>().Position -= Vector2.up;
                        matchables[x, y - 1] = matchables[x, y];
                        matchables[x, y] = null;
                    }
                }
            }

            for (int y = gridY - 1; y >= 0; y--) {
                if (!matchables[x, y]) {
                    SpawnTile(x, y);
                }
            }
        }
        
        if (matches) {
            SortArray();
            CheckForMatch(false);
        } else {
            shuffle.CheckForShuffle();
        }
        
    }

    /// <summary>
    /// Spawns a tile at the giver location inside the grid
    /// </summary>
    private void SpawnTile(int x, int y) {
        Vector2 spawnLocation = new Vector2(x, y);
        GameObject newMatchable = Instantiate(prefabs[Random.Range(0, prefabs.Length)],
            new Vector2(spawnLocation.x, spawnLocation.y + 1), Quaternion.identity);
        Matchable script = newMatchable.GetComponent<Matchable>();
        script.Position = spawnLocation;
        newMatchable.name = "Matchable ( " + x + ", " + y + " ) type: " + script.p_type;
        matchables[x, y] = newMatchable;
    }

    private void InitSwipe() {
        Vector2 ray = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        RaycastHit2D hit = Physics2D.Raycast(ray, Vector2.zero);
        if (hit.transform == null)
            return;

        firstTouch = hit.transform.gameObject;
    }

    /// <summary>
    /// sends a raycast and sets the first and second touch positions
    /// based on what it hits
    /// </summary>
    /// <param name="Requires true or false based on if it is the first or second input"></param>
    private void InputManagement() {
        Vector2 ray = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        RaycastHit2D hit = Physics2D.Raycast(ray, Vector2.zero);
        if (hit.transform == null)
            return;
        
        finalTouch = hit.transform.gameObject;
        GameObject secondMatchable = CalculateSwipe();
        
        if (secondMatchable == null) {
            currentState = GameState.go;
            return;
        }

        StartCoroutine(FunctionOrder(secondMatchable));
    }

    /// <summary>
    /// Checks if the swipe presented is valid and if so attempts to swap the items
    /// </summary>
    private GameObject CalculateSwipe() {
        //checks if the swipe is valid and if so takes control from the player and attempts to swap the items
        Vector2 first = firstTouch.transform.position;
        Vector2 final = finalTouch.transform.position;
        if (Mathf.Abs(final.y - first.y) > swipeResister || Mathf.Abs(final.x - first.x) > swipeResister) {
            swipeAngle = Mathf.Atan2(final.y - first.y, final.x - first.x) * 180 / Mathf.PI;
            currentState = GameState.wait;
            int row = (int)firstTouch.GetComponent<Matchable>().Position.x;
            int column = (int)firstTouch.GetComponent<Matchable>().Position.y;

            float angle = Mathf.Round(swipeAngle / 90) * 90;
            Vector2 moveDirection = new Vector2(Mathf.Cos(angle * Mathf.Deg2Rad), Mathf.Sin(angle * Mathf.Deg2Rad));

            return matchables[row + (int)moveDirection.x, column + (int)moveDirection.y];
        }
    
        return null;
    }
    
    /// <summary>
    /// Checks if the two items can swap and if so calls upon SortArray to do so
    /// </summary>
    private void SwapItems(GameObject second) {
        (second.GetComponent<Matchable>().Position, firstTouch.GetComponent<Matchable>().Position) = (firstTouch.GetComponent<Matchable>().Position, second.GetComponent<Matchable>().Position);
        SortArray();
    }
    
    //TODO: replace this trash with something better
    private IEnumerator FunctionOrder( GameObject secondMatchable) {
        SwapItems(secondMatchable);
        
        yield return new WaitUntil(() => new Vector2(firstTouch.transform.position.x, firstTouch.transform.position.y) == firstTouch.GetComponent<Matchable>().p_target);

        SortArray();
        otherMatchable = secondMatchable;
        CheckForMatch(true);
    }

    // Corrects the array and positions based on the position changes made in SwapItems
    public void SortArray() {
        for (int x = 0; x < gridX; x++) {
            for (int y = 0; y < gridY; y++) {
                int matchX = (int)matchables[x, y].GetComponent<Matchable>().Position.x;
                int matchY = (int)matchables[x, y].GetComponent<Matchable>().Position.y;
                if (x != matchX || y != matchY) {
                    GameObject matchableFirst = matchables[x, y].gameObject;
                    GameObject matchableSecond = matchables[matchX, matchY];

                    matchables[matchX, matchY] = matchableFirst;
                    matchables[x, y] = matchableSecond;
                }
            }
        }
    }

    // Checks for matches
    public void CheckForMatch(bool player) {
        bool matches = false;
        // Goes by the entire grid
        for (int x = 0; x < gridX; x++) {
            for (int y = 0; y < gridY; y++) {
                GameObject thisMatchable = matchables[x, y];
                int compType = thisMatchable.GetComponent<Matchable>().p_type;
                bool remove = compType == 0;
                if (thisMatchable != null && remove == false) {
                    // Checks left and right
                    List<GameObject> matchesMade = new List<GameObject>();
                    List<GameObject> potentionalMatchesX = CheckForMatchOnAxis(Vector2Int.right, new Vector2Int(x, y), gridX, compType);
                    List<GameObject> potentionalMatchesY = CheckForMatchOnAxis(Vector2Int.up, new Vector2Int(x, y), gridY, compType);
                    
                    if (potentionalMatchesX.Count > 2 ) {
                        matchesMade = potentionalMatchesX;
                    }
                    
                    if (potentionalMatchesY.Count > 2 && matchesMade.Count == 0) {
                        matchesMade = potentionalMatchesY;
                    }
                    
                    if (matchesMade.Count > 0) {
                        matches = true;

                        if (matchesMade.Count > 3) {
                             Matchable matchable = matchesMade.Find(e => e.GetComponent<Matchable>().p_IsPowerUp
                                 )?.GetComponent<Matchable>();
                        
                             if( matchable == null )
                                 matchable = matchesMade[0].GetComponent<Matchable>();
                             
                             if (!matchable.p_IsPowerUp && player) {
                                 matchable.BecomePowerup();
                                 matchesMade.RemoveAll( e => e == matchesMade[0] );
                             }
                        }

                        foreach (var t in matchesMade) {
                            Matchable matchable = t.GetComponent<Matchable>();
                            if (matchable.p_IsPowerUp == true) {
                                print("Delete All!");
                                for (int x2 = 0; x2 < gridX; x2++) {
                                    for (int y2 = 0; y2 < gridY; y2++) {
                                        if (matchables[x2, y2].GetComponent<Matchable>().p_type == compType ) {
                                            RemoveMatches(matchables[x2, y2]);
                                        }
                                    }
                                }
                                break;
                            }
                            
                            RemoveMatches(t);
                        }
                    }
                }
            }
        }
        // gives player control back once there are no matches
        if (matches == false && player == true) {
            SwapItems(otherMatchable);
            currentState = GameState.go;
        } else if (matches == false) {
            currentState = GameState.go;
        }
        
        GridBuilder(matches);
        currentState = GameState.go;
    }

    private List<GameObject> CheckForMatchOnAxis(Vector2Int axis ,Vector2Int start, int maxLength, int compType) {
        
        Stack<GameObject> matchesMade = new Stack<GameObject>();

        Vector2 selectedAxis = axis * start;
        
        float positionSelectedAxis = Mathf.Max(selectedAxis.x, selectedAxis.y);

        for (int i = (int)(positionSelectedAxis * -1f); i < maxLength - positionSelectedAxis; i++) {
            Vector2Int samplePosition = start + (axis * i); 
            
            if (matchables[samplePosition.x, samplePosition.y].GetComponent<Matchable>().p_type == compType) {
                matchesMade.Push(matchables[samplePosition.x, samplePosition.y]);
            }
            if (matchesMade.Count < 3 && matchables[samplePosition.x, samplePosition.y].GetComponent<Matchable>().p_type != compType) {
                matchesMade.Clear();
            } else if (matchesMade.Count > 2 && matchables[samplePosition.x, samplePosition.y].GetComponent<Matchable>().p_type != compType) {
                break;
            }
        }

        List<GameObject> matchesMadeList = new List<GameObject>(matchesMade);

        return matchesMadeList;
    }

    // Removes the presented matchable
    private void RemoveMatches(GameObject match) {
        Matchable matchable = match.GetComponent<Matchable>();

        if (matchable.p_type == goal1Comptype && playerStarted == true) {
            goal1CountDown -= 1;
            if (goal1CountDown < 0) {goal1CountDown = 0;}
            goal1Text.text = "" + goal1CountDown;
        } 
        if (matchable.p_type == goal2Comptype && playerStarted == true) {
            goal2CountDown -= 1;
            if (goal2CountDown < 0) {goal2CountDown = 0;}
            goal2Text.text = "" + goal2CountDown;
        }

        if( playerStarted && goal1CountDown == 0 && goal2CountDown == 0 )
        {
            enabled = false;
            OnGameCompleted.Invoke();
        }

        float totalScore = goal1CountDown + goal2CountDown;
        score.Value = (1 - (totalScore / goalCount * 2)) * 100;
        matchable.p_type = 0;
    }
}