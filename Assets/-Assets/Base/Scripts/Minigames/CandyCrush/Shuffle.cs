using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shuffle : MonoBehaviour {
    [SerializeField] private Board board;
    /// <summary>
    /// this function clones the matchables array and decides if a shuffle is need while staying within the array
    /// </summary>
    public void CheckForShuffle() {
        GameObject[,] matchablesClone = board.matchables;
        for (int x = 0; x < board.gridX; x++) {
            for (int y = 0; y < board.gridY; y++) {
                if (x < board.gridX - 1 && y < board.gridY - 1 && x - 1 > 0 && y - 1 > 0) {
                    
                    if (CompareType(matchablesClone, x, y)) {
                        return;
                    }
                } 
            }
        }
        DoShuffle();
    }
    
    /// <summary>
    /// Checks if there is a match possible withing each of 8 options if so returns true (example: 1141 will not count but 114 will count
    ///                                                                                                                      1)
    /// </summary>
    /// <param name="matchables"></param>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <returns></returns>
    private bool CompareType(GameObject[,] matchables, int x, int y) {
        int typeOriginal = matchables[x, y].GetComponent<Matchable>().p_type;
        if (matchables[x, y + 1].GetComponent<Matchable>().p_type == typeOriginal && matchables[x + 1, y - 1].GetComponent<Matchable>().p_type == typeOriginal ||
            matchables[x, y + 1].GetComponent<Matchable>().p_type == typeOriginal && matchables[x - 1, y - 1].GetComponent<Matchable>().p_type == typeOriginal ||
            
            matchables[x, y - 1].GetComponent<Matchable>().p_type == typeOriginal && matchables[x + 1, y + 1].GetComponent<Matchable>().p_type == typeOriginal ||
            matchables[x, y - 1].GetComponent<Matchable>().p_type == typeOriginal && matchables[x - 1, y + 1].GetComponent<Matchable>().p_type == typeOriginal ||
            
            matchables[x + 1, y].GetComponent<Matchable>().p_type == typeOriginal && matchables[x - 1, y + 1].GetComponent<Matchable>().p_type == typeOriginal ||
            matchables[x + 1, y].GetComponent<Matchable>().p_type == typeOriginal && matchables[x - 1, y - 1].GetComponent<Matchable>().p_type == typeOriginal ||
            
            matchables[x - 1, y].GetComponent<Matchable>().p_type == typeOriginal && matchables[x + 1, y + 1].GetComponent<Matchable>().p_type == typeOriginal ||
            matchables[x - 1, y].GetComponent<Matchable>().p_type == typeOriginal && matchables[x + 1, y - 1].GetComponent<Matchable>().p_type == typeOriginal) {
            return true;
        }

        return false;
    }
    
    /// <summary>
    /// Shuffle's the board until no longer needed
    /// </summary>
    private void DoShuffle() {
        for (int x = 0; x < board.gridX; x++) {
            for (int y = 0; y < board.gridY; y++) {
                int x2 = Random.Range(0, board.gridX); 
                int y2 = Random.Range(0, board.gridY);
                (board.matchables[x, y].GetComponent<Matchable>().Position, board.matchables[x2, y2].GetComponent<Matchable>().Position) = (board.matchables[x2, y2].GetComponent<Matchable>().Position, board.matchables[x, y].GetComponent<Matchable>().Position);
                board.SortArray();
            }
        }
        CheckForShuffle();
        board.CheckForMatch(false);
    }
}
