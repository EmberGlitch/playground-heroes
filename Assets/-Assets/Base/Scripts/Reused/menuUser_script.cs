﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
using DialogSystem;

public class menuUser_script : MonoBehaviour
{
    public Toggle male, female, nonBinary;
    public new TMP_InputField name;
    public GameObject fingers;
    public string gender, userName;

    [SerializeField]
    private Blackboard blackboard;

    [SerializeField] private GameObject spriteHolder;
    [SerializeField] private Sprite maleSprite, femaleSprite, nonBinarySprite;

    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    private void Start()
    {
        SetName();
    }

    public void swap()
    {
        if (male.isOn)
        {
            female.isOn = false;
            nonBinary.isOn = false;

            Image image = spriteHolder.GetComponent<Image>();
            image.sprite = maleSprite;
        }
        gender = "M";
    }

    public void swap1()
    {
        if (nonBinary.isOn)
        {
            male.isOn = false;
            female.isOn = false;

            Image image = spriteHolder.GetComponent<Image>();
            image.sprite = nonBinarySprite;
        }
        gender = "N";
    }

    public void swap2()
    {
        if (female.isOn)
        {
            male.isOn = false;
            nonBinary.isOn = false;

            Image image = spriteHolder.GetComponent<Image>();
            image.sprite = femaleSprite;
        }
        gender = "F";
    }

    private void SetName()
    {
        if (GameObject.Find("OldCharacterName"))
        {
            GameObject oldHolder = GameObject.Find("OldCharacterName");
            RememberName oldCharacterName = GameObject.Find("OldCharacterName").GetComponent<RememberName>();

            name.text = oldCharacterName.oldName;

            if (oldCharacterName.gender == "M")
            { male.isOn = true; swap(); }
            else if (oldCharacterName.gender == "F")
            { female.isOn = true; swap2(); }
            else
            { nonBinary.isOn = true; swap1(); }

            Destroy(oldHolder);
        }
        else
        { nonBinary.isOn = true; swap1(); }
    }

    public void getName()
    {
        userName = name.text;
        blackboard.Put( new BlackboardData() {name = "userName", value = name.text} );
    }

    public void gotoMenu()
    {
        if (userName != "")
            SceneManager.LoadScene("TitleScreen");
        else
            fingers.SetActive(true);
    }

    public void GoToMenu()
    {
        if (userName != "")
            SceneManager.LoadScene("SC_MainMenu");
        else
            fingers.SetActive(true);
    }
}