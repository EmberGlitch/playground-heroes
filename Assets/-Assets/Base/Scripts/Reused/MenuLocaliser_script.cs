﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuLocaliser_script : MonoBehaviour
{
    //Reference to the LanguageMenu_script
    public LanguageMenu_script LanguageMenu_script;

    //Start texts to be translated
    public GameObject textEN, textSL, textGR, textDU, textGE, textFR, soundYes, soundNo, EN, DU, GR, SL, GE, FR;

    //Reference that needs to be destroyed if we change language
    public GameObject DestroyThisReference;

    public GameObject Music;
    private bool toggleMusic = true;
    public GameObject gender;
    public score_script scoreRef;
    public resources_permanent resRef;
    public string sceneName;
    public string LastSceneName;

    private GameManager m_gameManager;

    private void Start()
    {
        m_gameManager = GameManager.FindObjectOfType<GameManager>();

        LanguageMenu_script = GameObject.Find("LanguageScriptHolder")?.GetComponent<LanguageMenu_script>();
        Scene currentScene = SceneManager.GetActiveScene();
        sceneName = currentScene.name;

        if (sceneName == "TitleScreen")
        {
            switch (LanguageMenu_script.selected_language)
            {
                case "EN":
                    textEN.SetActive(false);
                    textSL.SetActive(false);
                    textGR.SetActive(false);
                    textDU.SetActive(false);
                    textGE.SetActive(false);
                    textFR.SetActive(false);
                    EN.SetActive(false);
                    SL.SetActive(false);
                    GR.SetActive(false);
                    DU.SetActive(false);
                    GE.SetActive(false);
                    FR.SetActive(false);

                    textEN.SetActive(true);
                    EN.SetActive(true);
                    break;

                case "DU":
                    textEN.SetActive(false);
                    textSL.SetActive(false);
                    textGR.SetActive(false);
                    textDU.SetActive(false);
                    textGE.SetActive(false);
                    textFR.SetActive(false);
                    EN.SetActive(false);
                    SL.SetActive(false);
                    GR.SetActive(false);
                    DU.SetActive(false);
                    GE.SetActive(false);
                    FR.SetActive(false);

                    textDU.SetActive(true);
                    DU.SetActive(true);
                    break;

                case "GR":
                    textEN.SetActive(false);
                    textSL.SetActive(false);
                    textGR.SetActive(false);
                    textDU.SetActive(false);
                    textGE.SetActive(false);
                    textFR.SetActive(false);
                    EN.SetActive(false);
                    SL.SetActive(false);
                    GR.SetActive(false);
                    DU.SetActive(false);
                    GE.SetActive(false);
                    FR.SetActive(false);

                    textGR.SetActive(true);
                    GR.SetActive(true);
                    break;

                case "SL":
                    textEN.SetActive(false);
                    textSL.SetActive(false);
                    textGR.SetActive(false);
                    textDU.SetActive(false);
                    textGE.SetActive(false);
                    textFR.SetActive(false);
                    EN.SetActive(false);
                    SL.SetActive(false);
                    GR.SetActive(false);
                    DU.SetActive(false);
                    GE.SetActive(false);
                    FR.SetActive(false);

                    textSL.SetActive(true);
                    SL.SetActive(true);
                    break;

                case "GE":
                    textEN.SetActive(false);
                    textSL.SetActive(false);
                    textGR.SetActive(false);
                    textDU.SetActive(false);
                    textGE.SetActive(false);
                    textFR.SetActive(false);
                    EN.SetActive(false);
                    SL.SetActive(false);
                    GR.SetActive(false);
                    DU.SetActive(false);
                    GE.SetActive(false);
                    FR.SetActive(false);

                    textGE.SetActive(true);
                    GE.SetActive(true);
                    break;

                case "FR":
                    textEN.SetActive(false);
                    textSL.SetActive(false);
                    textGR.SetActive(false);
                    textDU.SetActive(false);
                    textGE.SetActive(false);
                    textFR.SetActive(false);
                    EN.SetActive(false);
                    SL.SetActive(false);
                    GR.SetActive(false);
                    DU.SetActive(false);
                    GE.SetActive(false);
                    FR.SetActive(false);

                    textFR.SetActive(true);
                    FR.SetActive(true);
                    break;

                default:
                    textEN.SetActive(true);
                    EN.SetActive(true);
                    break;
            }
        }
    }

    public void LanguageMenu()
    {
        //we destroy it otherwise it would be duplicated, making it impossible to change language
        DestroyThisReference = GameObject.Find("LanguageScriptHolder");
        Destroy(DestroyThisReference);
        SceneManager.LoadScene("LanguageSelection");
    }

    public void Scenario2()
    {
        SceneManager.LoadScene("Scenario2");
    }

    public void Scenario3()
    {
        SceneManager.LoadScene("Scenario3");
    }

    public void Scenario1()
    {
        SceneManager.LoadScene("Scenario1");
    }

    public void Scenario0()
    {
        SceneManager.LoadScene("Scenario0");
    }

    public void TitleScreen()
    {
        SceneManager.LoadScene("TitleScreen");
    }

    public void Tutorial()
    {
        SceneManager.LoadScene("Tutorial");
    }

    public void Scenario1_2()
    {
        SceneManager.LoadScene("Scenario1.2");
    }

    public void Scenario1_3()
    {
        SceneManager.LoadScene("Scenario1.3");
    }

    public void Gameplay1()
    {
        m_gameManager.SetBool();
        SceneManager.LoadScene("SC_Level_1");
    }

    public void Gameplay2()
    {
        m_gameManager.SetBool();
        SceneManager.LoadScene("SC_Level_1");
    }

    public void Gameplay3()
    {
        m_gameManager.SetBool();
        SceneManager.LoadScene("SC_Level_1");
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void DestroyGender()
    {
        gender = GameObject.FindGameObjectWithTag("gender");
        Destroy(gender);
    }

    public void MusicToggle()
    {
        toggleMusic = !toggleMusic;
        Music.SetActive(toggleMusic);
        soundYes.SetActive(toggleMusic);
        soundNo.SetActive(!toggleMusic);
    }

    public void add2()
    {
        scoreRef = GameObject.FindGameObjectWithTag("userScore").GetComponent<score_script>();
        resRef = GameObject.Find("resources").GetComponent<resources_permanent>();
        resRef.metalTotal = resRef.metalTotal + 8;
        resRef.brickTotal = resRef.brickTotal + 8;
        resRef.woodTotal = resRef.woodTotal + 8;
        scoreRef.userScore = scoreRef.userScore + 2;
    }

    public void add1()
    {
        scoreRef = GameObject.FindGameObjectWithTag("userScore").GetComponent<score_script>();
        resRef = GameObject.Find("resources").GetComponent<resources_permanent>();
        resRef.metalTotal = resRef.metalTotal + 4;
        resRef.brickTotal = resRef.brickTotal + 4;
        resRef.woodTotal = resRef.woodTotal + 4;
        scoreRef.userScore = scoreRef.userScore + 1;
    }
}