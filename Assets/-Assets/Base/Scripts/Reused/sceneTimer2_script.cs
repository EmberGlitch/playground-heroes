﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class sceneTimer2_script : MonoBehaviour {

    public sceneTimer_script scriptRef;
    public score_script scoreRef;
    private IEnumerator coroutine;
    public float timer;

    // Use this for initialization
    void Start () {
        scriptRef = GameObject.FindGameObjectWithTag("sceneTracker").GetComponent<sceneTimer_script>();
        coroutine = sceneTimer();
        StartCoroutine(coroutine);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public IEnumerator sceneTimer()
    {
        yield return new WaitForSeconds(timer);
        triggerScene();
    }

    public void triggerScene()
    {
        switch (scriptRef.LastSceneName)
        {
            case "Scenario1":
                SceneManager.LoadScene("Scenario1.2");
                break;
            case "Scenario1.2":
                SceneManager.LoadScene("Scenario1.3");
                break;
            case "Scenario1.3":
                scoreRef = GameObject.FindGameObjectWithTag("userScore").GetComponent<score_script>();
                if(scoreRef.userScore<=1)
                SceneManager.LoadScene("Scenario1END_BAD");
                else if(scoreRef.userScore >=2 && scoreRef.userScore <= 4)
                SceneManager.LoadScene("Scenario1END_AVG");
                else if (scoreRef.userScore >= 5)
                SceneManager.LoadScene("Scenario1END");
                break;
            case "Scenario2":
                SceneManager.LoadScene("Scenario2.2");
                break;
            case "Scenario2.2":
                SceneManager.LoadScene("Scenario2.3");
                break;
            case "Scenario2.3":
                SceneManager.LoadScene("Scenario2END");
                break;
            case "Scenario3":
                SceneManager.LoadScene("Scenario3.2");
                break;
            case "Scenario3.2":
                SceneManager.LoadScene("Scenario3.3");
                break;
            case "Scenario3.3":
                SceneManager.LoadScene("Scenario3END");
                break;
            default:
                break;
        }      
       

    }
}
