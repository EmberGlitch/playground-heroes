﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LanguageMenu_script : MonoBehaviour {

    public string selected_language;
 

    void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    public void MainMenu()
    {
        SceneManager.LoadScene("TitleScreen");
    }

    public void GenderScene()
    {
        SceneManager.LoadScene("NameGender");
    }


    public void English()
    {
        selected_language = "EN";
    }

    public void Slovak()
    {
        selected_language = "SL";
    }

    public void Greek()
    {
        selected_language = "GR";
    }

    public void Dutch()
    {
        selected_language = "DU";
    }
    public void German()
    {
        selected_language = "GE";
    }
    public void French()
    {
        selected_language = "FR";
    }
}
