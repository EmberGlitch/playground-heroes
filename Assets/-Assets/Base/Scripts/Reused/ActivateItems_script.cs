﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ActivateItems_script : MonoBehaviour {


    public GameObject buildables, playerM, playerF;
    public menuUser_script gender;

    // Update is called once per frame
    void Update () {
        Scene currentScene = SceneManager.GetActiveScene();
        string sceneName = currentScene.name;
        if (sceneName == ("Gameplay1")|| sceneName == ("Gameplay2")|| sceneName == ("Gameplay3"))
        {
            gender = GameObject.FindGameObjectWithTag("gender").GetComponent<menuUser_script>();
            if(gender.gender=="M")
            playerM.SetActive(true);
            else
            playerF.SetActive(true);

            buildables.SetActive(true);
        }
        else {
            playerM.SetActive(false);
            playerF.SetActive(false);
            buildables.SetActive(false);
        }
        if (sceneName == ("TitleScreen"))
        {
            Destroy(this.gameObject);
            //ScreenCapture.CaptureScreenshot("SomeLevel");
        }
    }
}
