﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class sceneTimer_script : MonoBehaviour {

    [Header("******Has played scenario?******")]
    //public bool scenario1_2, scenario1_3, scenario2_2, scenario2_3, scenario3_2, scenario3_3, scenario1, scenario2, scenario3;
    public string sceneName;
    public string LastSceneName;


    // Use this for initialization
    void Start () {
        DontDestroyOnLoad(this.gameObject);
    }
	
	// Update is called once per frame
	void Update () {

        Scene currentScene = SceneManager.GetActiveScene();
        sceneName = currentScene.name;

        if (sceneName != "Gameplay1" && sceneName != "Gameplay2" && sceneName != "Gameplay3" && sceneName != "SC_FullLevel_1")
            LastSceneName = sceneName;     

        if (sceneName == ("TitleScreen") || sceneName == ("SC_MainMenu"))
        {
            Destroy(this.gameObject);
        }
    }


}

