using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SyncWithMainCam : MonoBehaviour
{
    private GameObject mainCamera;

    void Start()
    {
        mainCamera = GameObject.Find("Main Camera");
        gameObject.transform.position = mainCamera.transform.position;
    }
}
