// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Hidden/testGradient"
{
	Properties 
    {
         _Slide ("Slide", Range(0, 1)) = 0.5
     }
 
     SubShader {
         Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" "PreviewType" = "Plane"}
         LOD 100
  
         Pass {
             CGPROGRAM
             #pragma vertex vert
             #pragma fragment frag
 
             #include "UnityCG.cginc"
 
             struct appdata_t {
                 float4 vertex : POSITION;
                 fixed4 color : COLOR;
                 float2 texcoord : TEXCOORD0;
             };
 
             struct v2f {
                 float4 vertex : SV_POSITION;
                 fixed4 color : COLOR;
                 half2 texcoord : TEXCOORD0;
             };
 
             v2f vert (appdata_t v)
             {
                 v2f o;
                 o.vertex = UnityObjectToClipPos(v.vertex);
                 o.texcoord = v.texcoord;
                 o.color = v.color;
                 return o;
             }

             float _Slide;
 
             fixed4 frag (v2f i) : SV_Target
             {
                 float t = length((i.texcoord - float2(0.5, 0.5)) * 2);
                 t = smoothstep(1, _Slide, t);
                 return lerp(0, i.color, t);
             }
             ENDCG
        }
    }
}
