using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioSourceWrapper : MonoBehaviour
{
    [SerializeField]
    private new AudioSource audio;
    [SerializeField, Range(0,0.3f)]
    private float pitchDiviation;
    [SerializeField, Range(0,1)]
    private float minInterval;

    private float timeLastPlayed;

    // Start is called before the first frame update
    void Start()
    {
        if( audio == null )
            audio = GetComponent<AudioSource>();
    }

    public void Play()
    {
        if( Time.time - timeLastPlayed > minInterval )
        {
            PlayOnes();
        }
    }

    private void PlayOnes()
    {
        audio.loop = false;
        audio.pitch = 1 + Random.Range( -pitchDiviation, pitchDiviation );
        audio.PlayOneShot(audio.clip);
        timeLastPlayed = Time.time;
    }

}
