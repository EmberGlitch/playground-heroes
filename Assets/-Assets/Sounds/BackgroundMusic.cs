using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundMusic : MonoBehaviour
{
    public static BackgroundMusic Instance {private set; get;}

    private new AudioSource audio;

    void Start()
    {
        DontDestroyOnLoad( this.gameObject );

        Instance = this;
        audio = GetComponent<AudioSource>();
        Play();
    }

    public void Play()
    {
        audio.loop = true;
        audio.Play();
    }

    public void Mute()
    {
        StartCoroutine( LerpVolume( 0 ));
    }

    public void UnMute()
    {
        StartCoroutine( LerpVolume( 1 ));
    }

    private IEnumerator LerpVolume(float target)
    {
        while( audio.volume != target )
        {
            audio.volume = Mathf.MoveTowards( audio.volume, target, Time.deltaTime * 3);
            yield return null;
        }
    }
}
