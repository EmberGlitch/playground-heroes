using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class Player : MonoBehaviour
{
    public FloatReference AFKTimer;

    [SerializeField] private GameObject target;
    [SerializeField] private Transform playerTransform;
    [SerializeField] private Vector3 playerScale;
    [SerializeField] private Sprite male, female, nonBinary;
    [SerializeField] private GameManager gameManager;
    [SerializeField] private SpriteRenderer playerSpriteRenderer;
    [SerializeField] private GameObject MalePlayer, FemalePlayer, NonbinaryPlayer;
    [SerializeField] private Animator Animator;

    private float DefaultTime = 15f;
    private GameObject m_player;
    private menuUser_script genderChecker;
    private Vector2 lastClick;
    public Vector3 startLoc, saveLoc;
    public AIPath sc_AIPatch;

    private void Awake()
    {
        startLoc = gameObject.transform.position;
        playerScale = new Vector3(0, 0, 1);
        playerTransform = transform;

        genderChecker = GameObject.Find("GenderScriptHolder").GetComponent<menuUser_script>();
        if (!genderChecker)
            return;

        if (genderChecker.gender == "F")
        {
            playerSpriteRenderer.sprite = female;
            m_player = Instantiate(FemalePlayer, gameObject.transform);
        }
        else if (genderChecker.gender == "M")
        {
            playerSpriteRenderer.sprite = male;
            m_player = Instantiate(MalePlayer, gameObject.transform);
        }
        else
        {
            playerSpriteRenderer.sprite = nonBinary;
            m_player = Instantiate(NonbinaryPlayer, gameObject.transform);
        }

        Animator = m_player.GetComponent<Animator>();
        m_player.transform.up = Vector3.up;
    }

    private void Update()
    {
        Clicked();

        if (DefaultTime <= 0)
        {
            AFKTimer.Value += Time.deltaTime;
        }

        //DevCheat
        if (Input.GetKeyDown(KeyCode.U) && Debug.isDebugBuild)
        {
            gameManager.objectOneBuild = true;
            gameManager.objectTwoBuild = true;
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
        if (playerTransform.position.y > -10f && playerTransform.position.x < 10f && playerTransform.position.x > -14.5f)
        {
            float realScalar = GetScaleInRange(-6.5f, 1.2f, 1.1f, 0.8f, playerTransform.position.y);
            playerScale.x = realScalar;
            playerScale.y = realScalar;
            m_player.transform.localScale = playerScale;
        }
        else
        {
            playerScale.x = playerScale.y = 1f;
            m_player.transform.localScale = playerScale;
        }
    }

    private void Clicked()
    {
        if (Input.GetMouseButtonDown(0))
        {
            //Raycast, lastclick doen als je op geen object klikt
            if (gameObject.GetComponent<AIPath>().enabled)
            {
                lastClick = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                target.transform.position = new Vector3(lastClick.x, lastClick.y, 0);
                StartCoroutine(Start());
            }
        }

        if (transform.hasChanged)
        {
            DefaultTime = 15f;

            Animator.SetBool("Walk", true);
            transform.hasChanged = false;
        }
        else
        {
            DefaultTime -= Time.deltaTime;

            Animator.SetBool("Walk", false);
        }
    }

    private IEnumerator Start()
    {
        sc_AIPatch.destination = target.transform.position;
        sc_AIPatch.SearchPath();

        Vector2 _velocity = target.transform.position - transform.position;

        if (_velocity.x > 0)
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }
        else
        {
            transform.localScale = new Vector3(1, 1, 1);
        }

        while (sc_AIPatch.pathPending || !sc_AIPatch.reachedEndOfPath)
        {
            yield return null;
        }
    }

    public IEnumerator ReloadStart()
    {
        yield return new WaitForSeconds(0.01f);
        target.transform.position = new Vector3(startLoc.x, startLoc.y, startLoc.z);
        transform.position = new Vector3(startLoc.x, startLoc.y, startLoc.z);

        sc_AIPatch.destination = target.transform.position;
        sc_AIPatch.SearchPath();
        while (sc_AIPatch.pathPending || !sc_AIPatch.reachedEndOfPath)
        {
            yield return null;
        }
    }

    public IEnumerator PrefentMovement()
    {
        yield return new WaitForSeconds(0.01f);
        saveLoc = gameObject.transform.position;
        target.transform.position = new Vector3(saveLoc.x, saveLoc.y, saveLoc.z);
        transform.position = new Vector3(saveLoc.x, saveLoc.y, saveLoc.z);

        sc_AIPatch.destination = target.transform.position;
        sc_AIPatch.SearchPath();
        while (sc_AIPatch.pathPending || !sc_AIPatch.reachedEndOfPath)
        {
            yield return null;
        }
    }

    public float GetScaleInRange(float minRangeA, float maxRangeA, float minRangeB, float maxRangeB, float scaleSourceXY)
    {
        float scaleValue = Mathf.Lerp(minRangeB, maxRangeB, Mathf.InverseLerp(minRangeA, maxRangeA, scaleSourceXY));
        return scaleValue;
    }
}