using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LocalizedString : MonoBehaviour
{
    [SerializeField]
    private LocalizationTable table;

    [SerializeField]
    private string stringName;

    [SerializeField]
    private UnityEvent<string> onStringChanged;

    void Start()
    {
        OnStringChanged();
    }

    void OnEnable()
    {
        LocaleSetting.SubToLocale( OnStringChanged ); 
    }

    void OnDisable()
    {
        LocaleSetting.UnsubToLocale( OnStringChanged );
    }

    private void OnStringChanged()
    {
        if( table == null )
            return;

        if( table.TryGet( stringName, out string text ) )
        {
            onStringChanged.Invoke( text );
        }
    }
}
