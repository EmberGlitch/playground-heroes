using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Ideafixxxer.CsvParser;
using UnityEngine;
using UnityEngine.Networking;

[System.Serializable]
public struct LocalizedText
{
    public string name;
    public string[] translations;
}

[CreateAssetMenu(menuName = "Localization")]
public class LocalizationTable : ScriptableObject
{
    private UnityWebRequest unityWebRequest;
    private AsyncOperation asyncOperation;

    private Dictionary<string, int> lookup;
    [Tooltip("Only for debugging, change localization.csv to edit")]
    private List<LocalizedText> table;

    void OnEnable()
    {
    	table = new List<LocalizedText>();
        lookup?.Clear();
        table?.Clear();

        string path = Path.Combine( Application.streamingAssetsPath, $"{name}.csv" );
        if( path.StartsWith( "http" ) == false )
            path = $"file://{path}";

        unityWebRequest = UnityWebRequest.Get( path );
        asyncOperation = unityWebRequest.SendWebRequest(); 
        asyncOperation.completed += ProcessFile;
    }

    public bool TryGet(string id, out string text)
    {
        text = null;
        bool succes = lookup.TryGetValue( id, out int index );
        
        if( !succes )
            return succes;

        string[] data = table[index].translations;
        text = data[(int)LocaleSetting.localeInfo.locale];
        return succes;
    }

    private void ProcessFile(AsyncOperation operation)
    {
        string[][] content = ReadFileContent( unityWebRequest.downloadHandler.text );
        lookup = new Dictionary<string, int>();

        // start from first data line
        // 0 is line with headers
        for( int i = 1; i < content.Length; ++i )
        {
            lookup.Add( content[i][0], table.Count );
            
            table.Add( new LocalizedText() { 
                name = content[i][0],  
                translations = content[i][1..]
            });
        }
    }

    /// <summary>
    /// parse raw file content csv to string[][]
    /// </summary>
    /// <param name="fileContent">text content of file</param>
    /// <returns>parsed csv array [][]</returns>
    private string[][] ReadFileContent(string fileContent)
    {
        CsvParser csvParser = new CsvParser();
        string[][] content = csvParser.Parse( fileContent );  
        return content;
    }
}
