using System.Linq;
using UnityEngine;
using UnityEngine.Events;


[System.Serializable]
public enum Locale
{
    NL,
    EN,
    DE,
    FR,
    ES,
}

[System.Serializable]
public struct LocaleData
{
    public Locale locale;
    public Sprite localImage;
    public string localeName;
}

[CreateAssetMenu(menuName = "LocaleSettings")]
public class LocaleSetting : ScriptableObject
{
    private static LocaleSetting instance;
    public static LocaleData localeInfo => instance.locales.First( x => x.locale == instance.locale );

    public Locale locale;
    public LocaleData[] locales;

    public UnityEvent onNewLocale;

    void OnEnable()
    {
        instance = this;
    }

    [ContextMenu("dutch")]
    void SetDutch()
    {
        UpdateLocale( Locale.NL );
    }

    [ContextMenu("english")]
    void SetEnglish()
    {
        UpdateLocale( Locale.EN );
    }

    public void SetLocale( int newLocale )
        => SetLocale( (Locale) newLocale );

    public void SetLocale( Locale newLocale )
    {
        if( locale == newLocale )
            return;

        locale = newLocale;
        onNewLocale.Invoke();
    }

    public static void UpdateLocale( Locale newLocale )
    {
        instance.SetLocale( newLocale );
    }

    public static void SubToLocale( UnityAction callback )
    {
        instance.onNewLocale.AddListener( callback );
    }

    public static void UnsubToLocale( UnityAction callback )
    {
        instance.onNewLocale.AddListener( callback );
    }
}
